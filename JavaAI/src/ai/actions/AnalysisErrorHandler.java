package ai.actions;

import java.util.LinkedList;
import java.util.List;

import ai.cfg.InterestingCodeFragment;
import ai.common.FatalAIError;
import ai.domain.AbstractSemanticsIntf;

public class AnalysisErrorHandler {
	public static class ErrorEntry {
		public final InterestingCodeFragment codeFragment;
		public final String error;
		public final AbstractSemanticsIntf<?> semantics;

		public ErrorEntry(InterestingCodeFragment codeFragment, String error, AbstractSemanticsIntf<?> semantics) {
			this.codeFragment = codeFragment;
			this.error = error;
			this.semantics = semantics;
		}
	}
	public List<ErrorEntry> errors = new LinkedList<ErrorEntry>();
	
	public boolean handleError(InterestingCodeFragment codeFragment, RuntimeException exc, AbstractSemanticsIntf<?> semantics) {
		try {
			throw exc;
		} catch (FatalAIError e) {
			errors.add(new ErrorEntry(codeFragment, e.getMessage(), semantics));
			System.out.println("FATAL ERROR:" + codeFragment.getUniqueName() + ": " + e.getMessage() + ":" + semantics);
			return true;
		} catch (RuntimeException e){
			return false;
		}
	}

	public void timedOut(InterestingCodeFragment codeFragment, AbstractSemanticsIntf<?> semantics, int seconds) {
		errors.add(new ErrorEntry(codeFragment, "TIME OUT (" + seconds + "s)", semantics));
		System.out.println("TIME OUT:" + codeFragment.getUniqueName() + ": " + semantics);
	}
	
//	public String[] getErrors(){
//		
//	}
}
