package ai.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import ai.Utils;
import ai.cfg.CFGVertice;
import ai.cfg.InterestingCodeFragment;
import ai.cfg.MethodCFGBuilder;
import ai.cfg.MethodControlFlowGraph;
import ai.cfg.MethodFinder;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;
import ai.domain.boolintv.BoolIntvSemantics;

/**
 * Our sample action implements workbench action delegate.
 * The action proxy will be created by the workbench and
 * shown in the UI. When the user tries to use the action,
 * this delegate will be created and execution will be 
 * delegated to it.
 * @see IWorkbenchWindowActionDelegate
 */
public class AnalyseBoolIntvAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	private ITreeSelection selection;
	/**
	 * The constructor.
	 */
	public AnalyseBoolIntvAction() {
	}

	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		if (selection == null){
			MessageDialog.openInformation(window.getShell(), "Information", "No input files selected");
			return;
		}
		int count = 0;
		BoolIntvSemantics semantics = new BoolIntvSemantics();
		AnalysisErrorHandler aeh = new AnalysisErrorHandler();
		try {
			List<ICompilationUnit> allTasks = Utils.getSelectedIcu(selection);
			for (ICompilationUnit icu : allTasks) {
				count += processIcu(icu, semantics, aeh);
			}			
		} catch (JavaModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("TOTAL: " + count);		
	}
	
	private static <DI extends DomainIntf<DI>> Map<CFGVertice, DI> findNonTrivials(Map<CFGVertice, DI> analysisResult){
		Map<CFGVertice, DI> result = new HashMap<CFGVertice, DI>();
		for(Map.Entry<CFGVertice, DI> entry: analysisResult.entrySet()){
			if (!entry.getValue().isTop())
				result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	private static <DI extends DomainIntf<DI>> int processIcu(ICompilationUnit icu,
			AbstractSemanticsIntf<DI> semantics, AnalysisErrorHandler aeh) {
//		BasicConfigurator.configure();
//		Logger logger = Logger.getRootLogger();
//		logger.setLevel(Level.DEBUG);
		
		CompilationUnit cu = Utils.parse(icu);
		int count = 0;
		for(InterestingCodeFragment codeFragment: MethodFinder.findMethods(cu)){
			if (Prefs.ONLY_METHOD != null && !codeFragment.getUniqueName().equals(Prefs.ONLY_METHOD))
				continue;
			//build graph
			MethodControlFlowGraph graph;
			try {
				 graph = MethodCFGBuilder.buildCFG(codeFragment);
			} catch (RuntimeException e) {
				System.err.println("Error building graph for:" + codeFragment.getUniqueName());
				e.printStackTrace(System.err);
				throw e;
			}
			if (graph == null)
				continue;
			System.err.println("Built graph for: " + codeFragment.getUniqueName());
			//analyse
			Map<CFGVertice, DI> res = ExecutionUtils.analyseOne(graph, semantics, aeh, codeFragment, 60);
			if (res == null)
				continue;
			Map<CFGVertice, DI> nonTrivialPoints = findNonTrivials(res);
			System.err.println("Analysed: " + codeFragment.getUniqueName());
			System.err.println("Found "+nonTrivialPoints.size()+ " non-trivial points");
			count += nonTrivialPoints.size();
		}
		return count;
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof ITreeSelection) {
			this.selection = (ITreeSelection) selection;
		}
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}