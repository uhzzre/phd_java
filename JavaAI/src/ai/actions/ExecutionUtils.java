package ai.actions;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ai.cfg.CFGVertice;
import ai.cfg.InterestingCodeFragment;
import ai.cfg.MethodControlFlowGraph;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;
import ai.interpreter.Interpreter;
import ai.interpreter.StopCondition;

public class ExecutionUtils {
	public static final int TIMEOUT_SECONDS = 60; 

	static class AnalysisCallable<DI extends DomainIntf<DI>> implements Callable<Map<CFGVertice, DI>> {
		private final MethodControlFlowGraph graph;
		private final AbstractSemanticsIntf<DI> semantics;
		public AnalysisCallable(MethodControlFlowGraph graph, AbstractSemanticsIntf<DI> semantics) {
			this.graph = graph;
			this.semantics = semantics;
			
		}
		@Override
		public Map<CFGVertice, DI> call() throws Exception {
			return Interpreter.analyse(graph, semantics, new StopCondition.NoStopCondition());
		}

	}

	@Deprecated
	public static <DI extends DomainIntf<DI>> Map<CFGVertice, DI> analyseOneOld(MethodControlFlowGraph graph,
			AbstractSemanticsIntf<DI> semantics, AnalysisErrorHandler aeh, InterestingCodeFragment codeFragment) {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<Map<CFGVertice, DI>> future = executor.submit(new AnalysisCallable(graph, semantics));
		Map<CFGVertice, DI> result = null;
		try {
			result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			aeh.timedOut(codeFragment, semantics, TIMEOUT_SECONDS);
		} catch (InterruptedException e) {
			System.out.println("Terminated");
			e.printStackTrace();
		} catch (ExecutionException e) {
			RuntimeException cause = (RuntimeException) e.getCause();
			if (!aeh.handleError(codeFragment, cause, semantics)) {
				System.out.println("Terminated: " + codeFragment.getUniqueName());
				e.printStackTrace();
				throw cause;
			}
		} finally {
			executor.shutdownNow();
		}
		return result;
	}
	
	public static <DI extends DomainIntf<DI>> Map<CFGVertice, DI> analyseOne(MethodControlFlowGraph graph,
			AbstractSemanticsIntf<DI> semantics, AnalysisErrorHandler aeh, InterestingCodeFragment codeFragment,
			int timeoutSeconds) {
		try {
			Map<CFGVertice, DI> result = Interpreter.analyse(graph, semantics, new StopCondition.TimeoutStopCondition(timeoutSeconds*1000));
			if (result == null)
				aeh.timedOut(codeFragment, semantics, timeoutSeconds);
			return result;
		} catch (RuntimeException e) {
			if (aeh.handleError(codeFragment, e, semantics))
				return null;
			System.out.println("Terminated: " + codeFragment.getUniqueName());
			e.printStackTrace();
			throw e;
		}
	}
}

