package ai.interpreter;

import java.util.Timer;
import java.util.TimerTask;

public interface StopCondition {
	public boolean shouldStop();
	public void stop();
	
	public class NoStopCondition implements StopCondition {
		@Override
		public boolean shouldStop() {
			return false;
		}

		@Override
		public void stop() {
		}
	}

	public class TimeoutStopCondition implements StopCondition {
		private Object sync = new Object();
		private Timer timer;
		private volatile boolean shouldStop = false;
		public TimeoutStopCondition(int timeoutMiliseconds) {
			this.timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					synchronized (sync) {
						shouldStop = true;
						timer.cancel();
						timer = null;
					}
				}
			}, timeoutMiliseconds);
		}
		@Override
		public boolean shouldStop() {
			return shouldStop;
		}
		@Override
		public void stop() {
			synchronized (sync) {
				if (timer!=null)
					timer.cancel();
			}
		}
	}
}
