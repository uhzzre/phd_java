package ai.interpreter;

public class InterpreterException extends RuntimeException{

	private static final long serialVersionUID = -1555649504137322865L;

	public InterpreterException(String format, Object... args) {
		super(String.format(format, args));
	}

}
