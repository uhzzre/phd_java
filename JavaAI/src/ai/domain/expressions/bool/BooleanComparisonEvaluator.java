package ai.domain.expressions.bool;

import org.eclipse.jdt.core.dom.InfixExpression;

import ai.domain.DomainIntf;
import ai.domain.intervals.eval.EvaluationUtils;

public class BooleanComparisonEvaluator<DI extends DomainIntf<DI>> implements ComparisonEvaluatorIntf<DI> {
//	public static class BooleanComparisonEvaluatorException extends RuntimeException {
//		private static final long serialVersionUID = -1430050335667587818L;
//
//		public BooleanComparisonEvaluatorException(String format, Object... args) {
//			super(String.format(format, args));
//		}
//	}
//
//	private final BooleanEvaluator<DI> eval;
//
//	private Operator negate(Operator op) {
//		if (op == Operator.EQUALS)
//			return Operator.NOT_EQUALS;
//		if (op == Operator.NOT_EQUALS)
//			return Operator.EQUALS;
//		throw new BooleanComparisonEvaluatorException("Unknown boolean comparison operator '%s'", op); 
//	}
//	
//	private DI process(DI input, Operator op, Expression left, Expression right) {
//		
//	}
//
//	private DI evaluateComparison(DI input, InfixExpression comparison, boolean negated) {
//		if (input.isBottom())
//			return input;
//		Operator op = comparison.getOperator();
//		if (negated)
//			op = negate(op);
//		DI result = process(input, op, comparison.getLeftOperand(), comparison.getRightOperand());
//		if (result == null)
//			throw new BooleanComparisonEvaluatorException("Something is wrong!!");
//		return result;
//	}
	@Override
	public BooleanEvaluationState<DI> evaluateComparison(DI input, InfixExpression comparison) {
		if (!EvaluationUtils.isBooleanType(comparison.getLeftOperand())
				|| !EvaluationUtils.isBooleanType(comparison.getRightOperand()))
			return null;

		// TODO Auto-generated method stub
		return null;
	}
//	
//	private BooleanComparisonEvaluator(BooleanEvaluator<DI> eval){
//		this.eval = eval;
//	}
//
//	public static <DI extends DomainIntf<DI>> BooleanComparisonEvaluator<DI> create(
//			BooleanEvaluator<DI> eval) {
//		return new BooleanComparisonEvaluator<DI>(eval);
//	}
//	
}
