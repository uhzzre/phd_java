package ai.domain.expressions.bool;

import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;

import ai.domain.DomainIntf;
import ai.domain.Variable;
import ai.domain.bool.Bool;

public interface BooleanExpressionSemantics<DI extends DomainIntf<DI>> {
	
	public BooleanEvaluationState<DI> processBooleanNode(DI input, ArrayAccess node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, FieldAccess node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, InstanceofExpression node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, MethodInvocation node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, SimpleName node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, QualifiedName node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, NullLiteral node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, SuperFieldAccess node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, SuperMethodInvocation node);

	public BooleanEvaluationState<DI> processBooleanNode(DI input, ClassInstanceCreation node);
	
	public BooleanEvaluationState<DI> processBooleanNode(DI input, CastExpression node);
	
	public BooleanEvaluationState<DI> processComparison(DI input, InfixExpression node);

	/**
	 * 
	 * @param input not bottom!!
	 * @param toWhat
	 * @param value
	 * @return
	 */
	public DI processBooleanAssignment(DI input, Expression toWhat, Bool value);
	
	public DI newBooleanVariable(DI input, Variable var, Bool initialValueOrNull, boolean isArgument);
	
	//boxing??
	public BooleanEvaluationState<DI> processBooleanBoxing(DI input, Assignment node);
}
