package ai.domain.expressions.bool;

import ai.domain.DomainIntf;

public class BooleanEvaluationState<DI> {
	public final DI conditionMet;
	public final DI conditionNotMet;

	public BooleanEvaluationState(DI conditionMet, DI conditionNotMet) {
		this.conditionMet = conditionMet;
		this.conditionNotMet = conditionNotMet;
	}
	
	public static <DI extends DomainIntf<DI>> BooleanEvaluationState<DI> create(DI conditionMet, DI conditionNotMet) {
		return new BooleanEvaluationState<DI>(conditionMet, conditionNotMet);
	}
	
	public String toString(){
		return "Eval[met="+conditionMet+"; notMet:"+conditionNotMet+"]";
	}
}

