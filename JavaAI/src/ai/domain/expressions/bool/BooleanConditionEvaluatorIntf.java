package ai.domain.expressions.bool;

import org.eclipse.jdt.core.dom.Expression;

public interface BooleanConditionEvaluatorIntf<DI> {
	public BooleanEvaluationState<DI> evaluateCondition(DI input, Expression conditionOrNull);
}
