package ai.domain.expressions.bool;

import org.eclipse.jdt.core.dom.InfixExpression;

import ai.domain.DomainIntf;

public interface ComparisonEvaluatorIntf<DI extends DomainIntf<DI>> {
	public BooleanEvaluationState<DI> evaluateComparison(DI input, InfixExpression comparison);

}
