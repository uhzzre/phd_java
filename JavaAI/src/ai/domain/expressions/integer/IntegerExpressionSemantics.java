package ai.domain.expressions.integer;

import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;

import ai.domain.Variable;

/**
 * 
 * @author uhzzre
 *
 * @param <PS> expression processing state class, e.g Pair<Interval, NonRelationalDomain<Interval>>
 */
public interface IntegerExpressionSemantics<PS> {// extends AbstractNumericExpressionSemantics<T>{
	
	public PS newIntegerVariable(PS input, Variable variable, boolean isArgument);

//	public PS processIntegerAssignment(PS input, Expression toWhat);

	public PS processIntegerNode(PS input, ArrayAccess node);
	
	public PS processIntegerNode(PS input, FieldAccess node);

	public PS processIntegerNode(PS input, MethodInvocation node);

	public PS processIntegerNode(PS input, SimpleName node);

	public PS processIntegerNode(PS input, QualifiedName node);

	public PS processIntegerNode(PS input, SuperFieldAccess node);

	public PS processIntegerNode(PS input, SuperMethodInvocation node);
	
	public PS processIntegerNode(PS input, ConditionalExpression node);
	
	public PS processIntegerNode(PS input, NumberLiteral node, long value);

	public PS processIntegerNode(PS input, CharacterLiteral node, long value);
	
	public PS processIntegerValueChange(PS input, Expression ofWhat, int delta, boolean postfixOrPrefix);

	// operations
	public PS processIntegerUnaryPlus(PS input);
	public PS processIntegerUnaryMinus(PS input);
	public PS processIntegerUnaryComplement(PS input);
	
	/**
	 * NOTE: non integer arguments case??
	 * @param input
	 * @param node
	 * @return
	 */
	public PS processIntegerNode(PS input, ClassInstanceCreation node);
	public PS processIntegerNode(PS input, CastExpression node);
	public PS processIntegerNode(PS input, NullLiteral node);
//
//	public T getConstantValue(long value);
//	
//	public T getArgumentValue();	
	
	public PS processFloatNode(PS input, Expression node);
}
