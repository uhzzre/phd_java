package ai.domain.expressions.integer;

import org.eclipse.jdt.core.dom.SimpleName;

import ai.domain.DomainIntf;

public interface BackwardExpressionSemantics<DI extends DomainIntf<DI>, T> {

	public DI processIntegerNode(DI input, SimpleName node, T value);

	public T getConstantValue(long value);

}
