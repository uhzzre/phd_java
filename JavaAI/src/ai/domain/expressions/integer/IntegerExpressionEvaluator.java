package ai.domain.expressions.integer;

import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;

import ai.domain.DomainIntf;
import ai.domain.Variable;
import ai.domain.expressions.ExpressionEvaluatorBase;
import ai.domain.intervals.eval.EvaluationUtils;

/**
 * 
 * @author kjk@mimuw.edu.pl
 *
 * @param <DI>
 * @param <PS>
 */
public abstract class IntegerExpressionEvaluator<DI extends DomainIntf<DI>, PS> extends ExpressionEvaluatorBase<DI, PS> {
	public static class IntegerExpressionEvaluatorException extends RuntimeException {

		private static final long serialVersionUID = -3475613060812813036L;

		public IntegerExpressionEvaluatorException(String format, Object... args) {
			super(String.format(format, args));
		}
	}

	protected abstract class IntegerExpressionVisitor extends ExpressionEvaluatorBase<DI, PS>.ExpressionVisitorBase {

		protected IntegerExpressionVisitor(PS input) {
			super(input);
		}

		@Override
		public boolean visit(ArrayAccess node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(ArrayCreation node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(ArrayInitializer node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(Assignment node) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean visit(BooleanLiteral node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(CastExpression node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(CharacterLiteral node) {
			long value = EvaluationUtils.getIntegerValue(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node, value));
			return false;
		}

		@Override
		public boolean visit(ClassInstanceCreation node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(ConditionalExpression node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(FieldAccess node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(InfixExpression node) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean visit(InstanceofExpression node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(MethodInvocation node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(SimpleName node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(QualifiedName node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(NullLiteral node) {
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(NumberLiteral node) {
			long value = EvaluationUtils.getIntegerValue(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node, value));
			return false;
		}

		@Override
		public boolean visit(ParenthesizedExpression node) {
			acceptChild(node.getExpression());
			setNewResult(node, result);
			return false;
		}

		@Override
		public boolean visit(PostfixExpression node) {
			ensureCorrectTypeOrBoxing(node);
			acceptChild(node.getOperand());
			int delta = (node.getOperator() == PostfixExpression.Operator.INCREMENT) ? 1 : -1;
			setNewResult(node, expressionSemantics.processIntegerValueChange(result, node.getOperand(), delta, true));
			return false;
		}

		@Override
		public boolean visit(PrefixExpression node) {
			ensureCorrectTypeOrBoxing(node);
			acceptChild(node.getOperand());
			PS newResult;
			PrefixExpression.Operator op = node.getOperator();
			Expression operand = node.getOperand();
			if (op == PrefixExpression.Operator.INCREMENT)
				newResult = expressionSemantics.processIntegerValueChange(result, operand, 1, false);
			else if (op == PrefixExpression.Operator.DECREMENT)
				newResult = expressionSemantics.processIntegerValueChange(result, operand, -1, false);
			else if (op == PrefixExpression.Operator.PLUS)
				newResult = expressionSemantics.processIntegerUnaryPlus(result);
			else if (op == PrefixExpression.Operator.MINUS)
				newResult = expressionSemantics.processIntegerUnaryMinus(result);
			else if (op == PrefixExpression.Operator.COMPLEMENT)
				newResult = expressionSemantics.processIntegerUnaryComplement(result);
			else
				throw getException("Invalid integer operator '%s'", op);
			setNewResult(node, newResult);
			return false;
		}

		@Override
		public boolean visit(StringLiteral node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(SuperFieldAccess node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(SuperMethodInvocation node) {
			ensureCorrectTypeOrBoxing(node);
			setNewResult(node, expressionSemantics.processIntegerNode(result, node));
			return false;
		}

		@Override
		public boolean visit(ThisExpression node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(TypeLiteral node) {
			throw unhandledNodeException(node);
		}

		@Override
		public boolean visit(VariableDeclarationExpression node) {
			throw unhandledNodeException(node);
		}
	}


	protected final IntegerExpressionSemantics<PS> expressionSemantics;

	public IntegerExpressionEvaluator(IntegerExpressionSemantics<PS> expressionSemantics) {
		this.expressionSemantics = expressionSemantics;
	}	
	@Override
	protected boolean isInterestingType(Expression expr) {
		return EvaluationUtils.isIntegerType(expr);
	}
	

	@Override
	protected boolean isInterestingType(VariableDeclaration variableDeclaration) {
		return EvaluationUtils.isIntegerType(variableDeclaration);
	}
	

	@Override
	protected PS processNewVariable(PS input, Variable var, boolean isArgument) {
		return expressionSemantics.newIntegerVariable(input, var, isArgument);
	}
	

	@Override
	protected RuntimeException getException(String message, Object... args) {
		return new IntegerExpressionEvaluatorException(message, args);
	}

//	@Override
//	protected PS constructEvaluationStateFromDomain(DI input) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected DI constructDomainFromEvaluationState(PS input) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected void verifyResult(PS result) {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	public static <DI extends DomainIntf<DI>, PS> IntegerExpressionEvaluator<DI, PS> create(IntegerExpressionSemantics<PS> semantics) {
//		return new IntegerExpressionEvaluator<DI, PS>(semantics);
//	}
	
	
//	private final IntegerExpressionSemantics<DI, T> expressionSemantics;
//
//	public IntegerExpressionEvaluator(IntegerExpressionSemantics<DI, T> expressionSemantics) {
//		this.expressionSemantics = expressionSemantics;
//	}
//
//	@Override
//	protected boolean isInterestingType(VariableDeclarationFragment variableDeclaration) {
//		return EvaluationUtils.isIntegerType(variableDeclaration);
//	}
//
//	@Override
//	protected DI processNewVariable(DI input, Variable var, T initialValueOrNull, boolean isArgument) {
//		return expressionSemantics.newIntegerVariable(input, var, initialValueOrNull, isArgument);
//	}
//
//	@Override
//	protected RuntimeException getException(String message, Object... args) {
//		return new IntegerExpressionEvaluatorException(message, args);
//	}
//
//	@Override
//	protected ExpressionVisitorBase getEvaluator(DI input) {
//		return new IntegerExpressionVisitor(input, false);
//	}
//	
//	public Pair<ExpressionEvaluationMap<T>, DI> evaluateExpressionsMap(DI input, Expression... expressions) {
//		IntegerExpressionVisitor eval = new IntegerExpressionVisitor(input, true);
//		for (Expression expr : expressions) {
//			if (!isInterestingType(expr))
//				throw getException("Invalid usage");
//			expr.accept(eval);
//			if (eval.result == null)
//				throw getException("NO RESULT!!");
//			if (eval.result.left == null)
//				throw getException("NO RESULT!!");
//			if (eval.result.right == null)
//				throw getException("NO RESULT!!");
//		}
//		return Pair.create(eval.nodeValues, eval.result.right);
//	}
//	
//	public static <DI extends DomainIntf<DI>, T> IntegerExpressionEvaluator<DI, T> create(IntegerExpressionSemantics<DI, T> semantics) {
//		return new IntegerExpressionEvaluator<DI, T>(semantics);
//	}
//	
}
