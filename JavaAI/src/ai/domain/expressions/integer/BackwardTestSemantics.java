package ai.domain.expressions.integer;

import ai.common.Pair;

public interface BackwardTestSemantics<T> {
	public Pair<T, T> equality(T left, T right);

	public Pair<T, T> leq(T left, T right);

	public Pair<T, T> less(T left, T right);

	public Pair<T, T> neq(T left, T right);
}
