package ai.domain.expressions.integer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;

public class ExpressionEvaluationMap<T> {
	public static class ExpressionEvaluationMapException extends RuntimeException {
		private static final long serialVersionUID = 2076676503416758463L;

		public ExpressionEvaluationMapException(String format, Object... args) {
			super(String.format(format, args));
		}
	}
	
	private Map<InfixExpression, LinkedList<T>> infixMap = new HashMap<InfixExpression, LinkedList<T>>();
	private Map<Expression, T> regularMap = new HashMap<Expression, T>();

	/**
	 * values represent sum of prefix of increasing length:
	 *   values[0] -> arguments
	 *   values[1] -> values[1] + another argument etc
	 * @param node
	 * @param values
	 */
	public void put(InfixExpression node, LinkedList<T> values){
		//length should be equal to number of argumenst -1
		int valuesListLength = 1 + (node.hasExtendedOperands() ? node.extendedOperands().size() : 0);
		if (values.size() != valuesListLength)
			throw new ExpressionEvaluationMapException("Invalid values list");
		infixMap.put(node, values);
	}
	
	public void put(Expression node, T value) {
		if (node instanceof InfixExpression)
			throw new ExpressionEvaluationMapException("Invalid usage, should receive list instead!!");
		regularMap.put(node, value);
	}
	
	public LinkedList<T> get(InfixExpression node) {
		return infixMap.get((InfixExpression)node);
	}

	public T get(Expression node) {
		if (node instanceof InfixExpression)
			return infixMap.get((InfixExpression)node).getLast();
		return regularMap.get(node);
	}
	
	public String toString() {
		return "ExpressionEvaluationMap(\ninfix: "+infixMap + "\n"+"regular: "+regularMap + "\n)";
	}
}
