package ai.domain.expressions;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;

import ai.domain.DomainIntf;

public interface ExpressionEvaluatorIntf<DI extends DomainIntf<DI>> {

	/**
	 * Handle expression and return result.
	 * 
	 * @param expr Expression to handle
	 * @param input Input
	 * @return != null if handled otherwise null
	 */
	public DI evaluate(Expression expr, DI input);
	
	public DI evaluate(SimpleName name, Expression initializerOrNull, DI input,
			boolean asAssignment);
	
	public DI evaluateNewArgument(SimpleName argument, DI input);
}
