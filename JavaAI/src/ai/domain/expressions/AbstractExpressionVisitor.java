package ai.domain.expressions;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;

public abstract class AbstractExpressionVisitor extends ASTVisitor {
	// possible ast nodes are listed here:
	// http://help.eclipse.org/indigo/index.jsp?topic=%2Forg.eclipse.jdt.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fjdt%2Fcore%2Fdom%2FExpression.html

	public abstract boolean visit(ArrayAccess node);

	public abstract boolean visit(ArrayCreation node);

	public abstract boolean visit(ArrayInitializer node);

	public abstract boolean visit(Assignment node);

	public abstract boolean visit(BooleanLiteral node);

	public abstract boolean visit(CastExpression node);

	public abstract boolean visit(CharacterLiteral node);

	public abstract boolean visit(ClassInstanceCreation node);

	public abstract boolean visit(ConditionalExpression node);

	public abstract boolean visit(FieldAccess node);

	public abstract boolean visit(InfixExpression node);

	public abstract boolean visit(InstanceofExpression node);

	public abstract boolean visit(MethodInvocation node);

	public abstract boolean visit(SimpleName node);

	public abstract boolean visit(QualifiedName node);

	public abstract boolean visit(NullLiteral node);

	public abstract boolean visit(NumberLiteral node);

	public abstract boolean visit(ParenthesizedExpression node);

	public abstract boolean visit(PostfixExpression node);

	public abstract boolean visit(PrefixExpression node);

	public abstract boolean visit(StringLiteral node);

	public abstract boolean visit(SuperFieldAccess node);

	public abstract boolean visit(SuperMethodInvocation node);

	public abstract boolean visit(ThisExpression node);

	public abstract boolean visit(TypeLiteral node);

	public abstract boolean visit(VariableDeclarationExpression node);
}
