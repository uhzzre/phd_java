package ai.domain.interval;

import ai.domain.DomainException;
import ai.domain.Variable;
import ai.domain.generic.NonRelationalDomain;

public class Utils {
	public static NonRelationalDomain<Interval> updateVariableBy(NonRelationalDomain<Interval> input, Variable var,
			int delta) {
		if (input.isBottom())
			return input;
		if (!input.containsValue(var))
			throw new DomainException("Missing variable: '%s'", var);
		Interval oldValue = input.getValueFor(var);
		Interval newValue = new Interval(oldValue.getValue().add(delta));
		if (newValue.equals(oldValue))
			return input;
		return input.updateVariable(var, newValue);
	}

}
