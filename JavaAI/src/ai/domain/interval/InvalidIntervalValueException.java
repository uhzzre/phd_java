package ai.domain.interval;

public class InvalidIntervalValueException extends RuntimeException{

	public InvalidIntervalValueException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 5038045950776984578L;

	public static InvalidIntervalValueException invalidIntervalValue(Double left, Double right) {
		return new InvalidIntervalValueException(String.format("Invalid interval value [%,f:%,f]", left, right));		
	}

}
