package ai.domain.interval;

import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;

import ai.domain.expressions.integer.IntegerExpressionSemantics;

/**
 * 
 * @author uhzzre
 *
 * @param <PS> expression processing state class, e.g Pair<Interval, NonRelationalDomain<Interval>>
 */
public interface IntervalsExpressionSemantics<PS> extends IntegerExpressionSemantics<PS>{// extends AbstractNumericExpressionSemantics<T>{
	
	public PS processIntegerAssignment(PS input, Expression toWhat);
	
	public PS processIntegerBoxing(PS input, Assignment node);
}
