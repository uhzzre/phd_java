package ai.domain.interval;

import ai.common.Pair;
import ai.domain.expressions.integer.BackwardTestSemantics;

public class IntervalBackwardTestSemantics implements BackwardTestSemantics<Interval> {
	// public static final Pair<Interval, Interval> BOTTOM = new Pair<Interval,
	// Interval>(Interval.BOTTOM,
	// Interval.BOTTOM);
	//
	private Pair<Interval, Interval> result(Interval left, Interval right) {
		if (left.isBottom())
			return null;
		if (right.isBottom())
			return null;
		return Pair.create(left, right);
	}

	public Pair<Interval, Interval> equality(Interval left, Interval right) {
		if (left.isBottom() || right.isBottom())
			return null;
		Interval meet = left.meet(right);
		return result(meet, meet);
	}

	public Pair<Interval, Interval> leq(Interval left, Interval right) {
		if (left.isBottom() || right.isBottom())
			return null;
		Interval newLeft = left.meet(new Interval(Double.NEGATIVE_INFINITY, right.getRight()));
		Interval newRight = right.meet(new Interval(left.getLeft(), Double.POSITIVE_INFINITY));
		return result(newLeft, newRight);
	}
	
	public Pair<Interval, Interval> lessHelper(Interval left, Interval right) {
		if (left.isBottom() || right.isBottom())
			return Pair.create(Interval.BOTTOM, Interval.BOTTOM);
		// we are in Z
		Interval newLeft = left.meet(new Interval(Double.NEGATIVE_INFINITY, right.getRight() - 1));
		Interval newRight = right.meet(new Interval(left.getLeft() + 1, Double.POSITIVE_INFINITY));
		return Pair.create(newLeft, newRight);
	}

	public Pair<Interval, Interval> less(Interval left, Interval right) {
		Pair<Interval, Interval> x = lessHelper(left, right);
		return result(x.left, x.right);
	}

	public Pair<Interval, Interval> neq(Interval left, Interval right) {
		if (left.isBottom() || right.isBottom())
			return null;
		Pair<Interval, Interval> first = lessHelper(left, right);
		Pair<Interval, Interval> second = lessHelper(right, left).swap();
		return result(first.left.join(second.left), first.right.join(second.right));
	}
}
