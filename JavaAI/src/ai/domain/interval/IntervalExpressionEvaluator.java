package ai.domain.interval;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;

import ai.common.Pair;
import ai.domain.DomainIntf;
import ai.domain.expressions.integer.ExpressionEvaluationMap;
import ai.domain.expressions.integer.IntegerExpressionEvaluator;
import ai.domain.intervals.eval.EvaluationUtils;

public class IntervalExpressionEvaluator<DI extends DomainIntf<DI>> extends IntegerExpressionEvaluator<DI, Pair<Interval, DI>>{
	private class MyExpressionVisitor extends IntegerExpressionVisitor {
		private final ExpressionEvaluationMap<Interval> nodeValues;

		protected MyExpressionVisitor(Pair<Interval, DI> input, boolean withMap) {
			super(input);
			if (withMap)
				this.nodeValues = new ExpressionEvaluationMap<Interval>();
			else
				this.nodeValues = null;
		}

		@Override
		protected void beforeVisitChild() {
			result = result.setLeft(null);
		}

		protected void setNewResult(Expression expr, Pair<Interval, DI> newResult) {
			super.setNewResult(expr, newResult);
			if (nodeValues != null)
				nodeValues.put(expr, newResult.left);
		}
		
		private LinkedList<Interval> processBinaryOperation(InfixExpression.Operator operator, Expression leftOperand, Expression... rightOperands) {
			acceptChild(leftOperand);
			Pair<Interval, DI> afterLeft = result;
			// right
			List<Interval> operandValues = new LinkedList<Interval>();
			for (Expression operand : rightOperands) {
				acceptChild(operand);
				Pair<Interval, DI> afterOp = result;
				operandValues.add(afterOp.left);
			}
			LinkedList<Interval> result = new LinkedList<Interval>();

			Interval resultValue = afterLeft.left;
			for(Interval operandValue: operandValues) {
				if (operator == InfixExpression.Operator.TIMES)
					resultValue = sem.times(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.DIVIDE)
					resultValue = sem.divide(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.REMAINDER)
					resultValue = sem.remainder(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.PLUS)
					resultValue = sem.plus(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.MINUS)
					resultValue = sem.minus(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.LEFT_SHIFT)
					resultValue = sem.leftShift(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.RIGHT_SHIFT_SIGNED)
					resultValue = sem.rightShiftSigned(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.RIGHT_SHIFT_UNSIGNED)
					resultValue = sem.rightShiftUnsigned(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.AND)
					resultValue = sem.and(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.OR)
					resultValue = sem.or(resultValue, operandValue);
				else if (operator == InfixExpression.Operator.XOR)
					resultValue = sem.xor(resultValue, operandValue);
				else
					throw getException("Unhandled infix operator: '%s'", operator);
				result.add(resultValue);
			}
			return result;
		}
		
		@Override
		public boolean visit(Assignment node) {
			if (EvaluationUtils.isIntegerType(node)) {
				Assignment.Operator op = node.getOperator();
				Interval resultVal;
				if (op == Assignment.Operator.ASSIGN){
					acceptChildNoTypeCheck(node.getRightHandSide());
					resultVal = result.left;
				} else if (op == Assignment.Operator.PLUS_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.PLUS, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.MINUS_ASSIGN) 
					resultVal = processBinaryOperation(InfixExpression.Operator.MINUS, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.TIMES_ASSIGN) 
					resultVal = processBinaryOperation(InfixExpression.Operator.TIMES, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.DIVIDE_ASSIGN) 
					resultVal = processBinaryOperation(InfixExpression.Operator.DIVIDE, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.BIT_AND_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.AND, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.BIT_OR_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.OR, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.BIT_XOR_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.XOR, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.REMAINDER_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.REMAINDER, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.LEFT_SHIFT_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.LEFT_SHIFT, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.RIGHT_SHIFT_SIGNED_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.RIGHT_SHIFT_SIGNED, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else if (op == Assignment.Operator.RIGHT_SHIFT_UNSIGNED_ASSIGN)
					resultVal = processBinaryOperation(InfixExpression.Operator.RIGHT_SHIFT_UNSIGNED, node.getLeftHandSide(), node.getRightHandSide()).getLast();
				else 
					throw getException("Unhandled operator: '%s'", op);
				Pair<Interval, DI> modifiedState = intervalExpressionSemantics.processIntegerAssignment(result.setLeft(resultVal), node.getLeftHandSide());
				setNewResult(node, modifiedState);
			} else if (EvaluationUtils.isIntegerTypeBoxing(node)){
				setNewResult(node, intervalExpressionSemantics.processIntegerBoxing(result, node));
			} else 
				throw getException("I shouldn't be here? '%s'", node);
			return false;		
		}

		@Override
		public boolean visit(InfixExpression node) {
			ensureIntegerType(node);
			//prepare arguments
			Expression[] operands = new Expression[1 + (node.hasExtendedOperands() ? node.extendedOperands().size() : 0)];
			operands[0] = node.getRightOperand();
			if (node.hasExtendedOperands()) {
				int i=1;
				for (Object child : node.extendedOperands())
					operands[i++] = (Expression) child;
			}			
			LinkedList<Interval> results = processBinaryOperation(node.getOperator(), node.getLeftOperand(), operands);			
			super.setNewResult(node, result.setLeft(results.getLast()));
			if (nodeValues != null)
				nodeValues.put(node, results);
			return false;
		}

		@Override
		protected void processFloatExpression(Expression expr) {
			result = result.setLeft(null);
			setNewResult(expr, expressionSemantics.processFloatNode(result, expr));
		}
		
	}

	private final IntervalExpressionSemantics sem;
	private final IntervalsExpressionSemantics<Pair<Interval, DI>> intervalExpressionSemantics;

	public IntervalExpressionEvaluator(IntervalsExpressionSemantics<Pair<Interval, DI>> expressionSemantics,
			IntervalExpressionSemantics sem) {
		super(expressionSemantics);
		this.intervalExpressionSemantics = expressionSemantics;
		this.sem = sem;
	}

	@Override
	protected final ExpressionVisitorBase getEvaluator(Pair<Interval, DI> input) {
		return new MyExpressionVisitor(input, false);
	}

	@Override
	protected final void verifyResult(Pair<Interval, DI> result, Expression expr) {
		if (result == null)
			throw getException("NO RESULT!! '%s'", expr);
		if (result.left == null)
			throw getException("NO RESULT!! '%s'", expr);
		if (result.right == null)
			throw getException("NO RESULT!! '%s'", expr);
	}

	@Override
	protected Pair<Interval, DI> constructEvaluationStateFromDomain(DI input) {
		return Pair.create(null, input);
	}

	@Override
	public DI constructDomainFromEvaluationState(Pair<Interval, DI> input) {
		return input.right;
	}

	public Pair<ExpressionEvaluationMap<Interval>, DI> evaluateExpressionsMap(DI input, Expression... expressions) {
		MyExpressionVisitor eval = new MyExpressionVisitor(constructEvaluationStateFromDomain(input), true);
		for (Expression expr : expressions) {
			if (!isInterestingType(expr))
				throw getException("Invalid usage");
			expr.accept(eval);
			if (eval.result == null)
				throw getException("NO RESULT!!");
			if (eval.result.left == null)
				throw getException("NO RESULT!!");
			if (eval.result.right == null)
				throw getException("NO RESULT!!");
		}
		return Pair.create(eval.nodeValues, eval.result.right);
	}
	
	public static <DI extends DomainIntf<DI>> IntervalExpressionEvaluator<DI> create(
			IntervalsExpressionSemantics<Pair<Interval, DI>> expressionSemantics,
			IntervalExpressionSemantics sem) {
		return new IntervalExpressionEvaluator<DI>(expressionSemantics, sem);
	}

	@Override
	public Pair<Interval, DI> processVariableAssignment(Pair<Interval, DI> input, Expression toWhat) {
		return intervalExpressionSemantics.processIntegerAssignment(input, toWhat);
	}
}
