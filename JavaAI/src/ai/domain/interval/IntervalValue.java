package ai.domain.interval;


public class IntervalValue {
	public final double left;
	public final double right;

	public IntervalValue(double left, double right) {
		if (left > right)
			throw new InvalidIntervalValueException(String.format("Invalid interval value [%,f:%,f]", left, right));
		if (right == Double.NEGATIVE_INFINITY)
			throw new InvalidIntervalValueException("Invalid end of the interval (-inf)");
		if (left == Double.POSITIVE_INFINITY)
			throw new InvalidIntervalValueException("Invalid begin of the interval (+inf)");
		if (Double.isNaN(left))
			throw new InvalidIntervalValueException("Invalid begin of the interval (NaN)");
		if (Double.isNaN(right))
			throw new InvalidIntervalValueException("Invalid end of the interval (NaN)");
		
		this.left = left;
		this.right = right;
	}

	public IntervalValue(double constantInitialiser) {
		this(constantInitialiser, constantInitialiser);
	}

	public IntervalValue(long constantInitialiser) {
		this(constantInitialiser, constantInitialiser);
	}
	
	public String toString(){
		return String.format("[%,f:,%,f]", left, right);
	}
	
	public boolean equals(Object other) {
		if (other == null) return false;
		if (!this.getClass().equals(other.getClass())) return false;
		IntervalValue otherValue = (IntervalValue) other;
		return left == otherValue.left && right == otherValue.right;
	}
	
	public IntervalValue le(IntervalValue other) {
		if (right <= other.right)
			return this;
		if (other.right < left)
			return null;
		return new IntervalValue(left, other.right-1);
	}
	
	public IntervalValue leq(IntervalValue other) {
		if (right <= other.right)
			return this;
		if (other.right < left)
			return null;
		return new IntervalValue(left, other.right);
	}

	public IntervalValue add(int i) {
		if (left == Double.NEGATIVE_INFINITY && right == Double.POSITIVE_INFINITY)
			return this;
		double newLeft = left == Double.NEGATIVE_INFINITY ? left : left + i;
		double newRight = right == Double.POSITIVE_INFINITY ? right : right + i;
		return new IntervalValue(newLeft, newRight);
	}
}
