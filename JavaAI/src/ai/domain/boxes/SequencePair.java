package ai.domain.boxes;

import java.util.ArrayList;

public class SequencePair {
	public final Double key;
	public final ArrayList<SequencePair> value;

	public SequencePair(Double key, ArrayList<SequencePair> value) {
		//IMPORTANT: we have to normalize -0.0 -> 0.0
		this.key = (key == 0.0) ? 0.0 : key;
		this.value = value;
	}
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("<");
		builder.append(key);
		builder.append(", ");
		if (value == null)
			builder.append("null");
		else {
			builder.append(" [");
			for(SequencePair item: value)
				builder.append(item.toString());
			builder.append("]");
		}
		builder.append(">");
		return builder.toString();
	}
}
