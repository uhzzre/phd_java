package ai.domain.boxes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import ai.common.Pair;
import ai.domain.Variable;

public class BoxesWideningOperator {
	public static interface WideningStepThresholds {
		public Set<Double> getThresholds(Variable var);
	}

	private final ArrayList<ArrayList<Double>> thresholds;
	private final int variableCount;
	
	private BoxesWideningOperator(int variablesCount, ArrayList<ArrayList<Double>> thresholds) {
		this.variableCount = variablesCount;
		//compute thresholds
		this.thresholds = thresholds;
	}

	private Pair<Pair<ArrayList<Double>, ArrayList<Double>>, Set<Double>> prepareKeys(int variable, ArrayList<SequencePair> left,
			ArrayList<SequencePair> right) {
		Set<Double> argKeys = new HashSet<Double>();
		Set<Double> allKeySet = new HashSet<Double>();
		allKeySet.add(Double.NEGATIVE_INFINITY);
		argKeys.add(Double.NEGATIVE_INFINITY);
		if (thresholds.get(variable) != null) 
			allKeySet.addAll(thresholds.get(variable));
		Set<Double> thresOrLeft = new HashSet<Double>(allKeySet);
		for(int i=0; i< right.size(); i++) {
			Double key = right.get(i).key;
			argKeys.add(key);
			allKeySet.add(key);
		}
		Set<Double> keysNotInLeft = new HashSet<Double>(allKeySet);
		for(int i=0; i< left.size(); i++){
			Double key = left.get(i).key;
			argKeys.add(key);
			thresOrLeft.add(key);
			if (!keysNotInLeft.remove(key))
				allKeySet.add(key);
		}
		ArrayList<Double> sortedKeys = new ArrayList<Double>(allKeySet);
		Collections.sort(sortedKeys);
		ArrayList<Double> sortedArgKeys = new ArrayList<Double>(argKeys);
		Collections.sort(sortedArgKeys);
 		return Pair.create(Pair.create(sortedKeys, sortedArgKeys), thresOrLeft);
	}
	
	private ArrayList<SequencePair> apply(int variable, 
			ArrayList<SequencePair> left, ArrayList<SequencePair> right) {
		if (left == null || variable == variableCount) //same as join
			return (left == null) ? right : left;
		ArrayList<SequencePair> result = new ArrayList<SequencePair>();
		
		Pair<Pair<ArrayList<Double>, ArrayList<Double>>, Set<Double>> keys = prepareKeys(variable, left, right);
		ArrayList<Double> all = keys.left.left;
		ArrayList<Double> sortedArgKeys = keys.left.right;
		Set<Double> thresOrLeft = keys.right;
		
		ArrayList<SequencePair> lastValue = null;
		ArrayList<SequencePair> currentLeftValue = null;
		ArrayList<SequencePair> currentRightValue = null;
		int currentLeftIdx = -1;
		int currentRightIdx = -1;
		int lastArgKey = -1;
		
//		int caseUsed = -1;
		for(int i =0; i < all.size(); i++){
//		try {
			Double key = all.get(i);
			//update current values
			if (currentLeftIdx < left.size() -1 && left.get(currentLeftIdx + 1).key.equals(key)) {
				currentLeftIdx++;
				currentLeftValue = left.get(currentLeftIdx).value;
			}
			if (currentRightIdx < right.size() -1 && right.get(currentRightIdx + 1).key.equals(key)) {
				currentRightIdx++;
				currentRightValue = right.get(currentRightIdx).value;
			}
			if (lastArgKey < sortedArgKeys.size() - 1 && sortedArgKeys.get(lastArgKey + 1).equals(key))
				lastArgKey++;
			//compute result
			ArrayList<SequencePair> newValue; 
			if (!IntegerBoxesHelper.equals(currentLeftValue, currentRightValue)) { //case 1
//				caseUsed = 1;
				newValue = apply(variable+1, currentLeftValue, currentRightValue);
			} else if (thresOrLeft.contains(key) && 
					(i == all.size() - 1 || thresOrLeft.contains(all.get(i+1)))) {//case 2
//				caseUsed = 2;
				newValue = currentLeftValue;
			} else if (lastArgKey < sortedArgKeys.size()-1 &&
					!thresOrLeft.contains(sortedArgKeys.get(lastArgKey + 1))) {
				//case 3, k = currentRightIdx + 1
//				caseUsed = 3;
				newValue = apply(variable+1, currentLeftValue, right.get(currentRightIdx+1).value);
			} else {
				//case 4
//				caseUsed = 4;
				newValue = apply(variable+1, currentLeftValue, right.get(currentRightIdx-1).value);
			}
			if (!IntegerBoxesHelper.equals(newValue, lastValue)) {
				lastValue = newValue;
				result.add(new SequencePair(key, newValue));
			}
//		} catch (RuntimeException e) {
//			System.err.println("ERROR DETAILS:");
//			System.err.println("THRESHOLDS:" + thresholds);
//			System.err.println("i:" + i);
//			System.err.println("CASE:" + caseUsed);
//			System.err.println("VAR:" + variable);
//			System.err.println("LEFT:" + left);
//			System.err.println("RIGHT:" + right);
//			System.err.println("SORTED ARG KEYS:" + sortedArgKeys);
//			System.err.println("CURRENT LEFT IDX:" + currentLeftIdx);
////			System.err.println("CURRENT LEFT VALUE:" + currentLeftValue);
//			System.err.println("CURRENT RIGHT IDX:" + currentRightIdx);
////			System.err.println("CURRENT RIGHT VALUE:" + currentRightValue);
//			System.err.println("LAST ARG KEY:" + lastArgKey);
//			throw e;
//		}
		}
		return result;
	}
	
	public static IntegerBoxes computeWidening(IntegerBoxes left, IntegerBoxes right,
			WideningStepThresholds stepThresholds) {
		if (left.isBottom())
			return right;
		if (!left.variables.equals(right.variables))
			throw new IntegerBoxes.IntegerBoxesException("Not the same variables");
		IntegerBoxes newRight = left.join(right); //it verifies variables are correct!!
		if (!left.leq(newRight))
			throw new RuntimeException("shit");
		if (!right.leq(newRight))
			throw new RuntimeException("shit 2");
		if (left.equals(newRight))
			return left;
		ArrayList<Variable> variables = left.variables;
		ArrayList<ArrayList<Double>> thresholds = new ArrayList<ArrayList<Double>>(variables.size());
		for(int i=0; i < variables.size(); i++) {
			ArrayList<Double> vt;
			if (stepThresholds != null)
				vt = new ArrayList<Double>(stepThresholds.getThresholds(variables.get(i)));
			else 
				vt = new ArrayList<Double>();
			Collections.sort(vt);
			thresholds.add(vt);
		}
		
		BoxesWideningOperator operator = new BoxesWideningOperator(left.variables.size(), thresholds);
//		try {
		ArrayList<SequencePair> data = operator.apply(0, left.data, newRight.data);
		return new IntegerBoxes(left.variables, data);
//		} catch (RuntimeException e) {
//			System.err.println("-------LEFT:");
//			System.err.println(IntegerBoxesHelper.formatData(left));
//			System.err.println("-------RIGHT:");
//			System.err.println(IntegerBoxesHelper.formatData(right));
//			System.err.println("-------JOIN:");
//			System.err.println(IntegerBoxesHelper.formatData(newRight));
//			throw e;
//			// TODO: handle exception
//		}
	}

	public static IntegerBoxes computeWidening(IntegerBoxes left, IntegerBoxes right,
			Collection<Double> globalThresholds) {
		final Set<Double> thresholdsSet = new HashSet<Double>(globalThresholds);
		return computeWidening(left, right, new WideningStepThresholds() {
			@Override
			public Set<Double> getThresholds(Variable var) {
				return thresholdsSet;
			}
		});
	}

	public static IntegerBoxes computeWidening(IntegerBoxes left, IntegerBoxes right) {
//		try {
		return computeWidening(left, right, new LinkedList<Double>());
//		} catch (RuntimeException e) {
//			System.err.println(IntegerBoxesHelper.split(left));
//			System.err.println("ORIGINAL LEFT:" + left);
//			System.err.println("ORIGINAL RIGHT:" + right);
//			e.printStackTrace(System.err);
//			throw e;
//		}
	}
}
