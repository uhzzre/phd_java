package ai.domain.bool;


/**
 * Implementation of INTEGER interval domain semantics 
 * 
 * 
 * @author kjk@mimuw.edu.pl
 *
 */
public class BoolExpressionSemantics{
	public Bool not(Bool arg) {
		if (arg==Bool.BOTTOM || arg==Bool.TOP)
			return arg;
		return arg==Bool.TRUE ? Bool.FALSE : Bool.TRUE;
	}
	
	public Bool or(Bool leftArg, Bool rightArg) {
		if (leftArg.isBottom() || rightArg.isBottom())
			return Bool.BOTTOM;
		if (leftArg == Bool.TRUE || rightArg == Bool.TRUE)
			return Bool.TRUE;		
		if (leftArg == Bool.TOP || rightArg == Bool.TOP)
			return Bool.TOP;
		return Bool.FALSE;
	}

	public Bool and(Bool leftArg, Bool rightArg) {
		if (leftArg.isBottom() || rightArg.isBottom())
			return Bool.BOTTOM;
		if (leftArg == Bool.TRUE && rightArg == Bool.TRUE)
			return Bool.TRUE;
		if (leftArg == Bool.FALSE || rightArg == Bool.FALSE)
			return Bool.FALSE;
		return Bool.TOP;
	}
	
	public Bool xor(Bool leftArg, Bool rightArg) {
		if (leftArg.isBottom() || rightArg.isBottom())
			return Bool.BOTTOM;
		if (leftArg == Bool.TRUE && rightArg == Bool.FALSE || leftArg == Bool.FALSE && rightArg == Bool.TRUE)
			return Bool.TRUE;
		if (leftArg == Bool.TRUE && rightArg == Bool.TRUE || leftArg == Bool.FALSE && rightArg == Bool.FALSE)
			return Bool.FALSE;
		return Bool.TOP;
	}
}
