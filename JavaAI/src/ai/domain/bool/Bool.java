package ai.domain.bool;

import ai.domain.DomainException;
import ai.domain.DomainIntf;

public enum Bool implements DomainIntf<Bool> {
	BOTTOM(0), FALSE(1), TRUE(2), TOP(3);
	
	private static Bool[] GET_VALUE = {BOTTOM, FALSE, TRUE, TOP};
	
	final int value;
	
	private Bool(int value){
		this.value = value;
	}
	
	public static Bool get(boolean value) {
		return value ? TRUE : FALSE;
	}
	
	@Override
	public boolean isBottom() {
		return value == 0; 
	}

	@Override
	public Bool join(Bool other) {
		return GET_VALUE[value | other.value];
	}

	@Override
	public Bool meet(Bool other) {
		return GET_VALUE[value & other.value];
	}

	@Override
	public Bool widen(Bool other) {
		return meet(other);
	}

	@Override
	public boolean leq(Bool other) {
		return this==BOTTOM || other==TOP || this == other;
	}

	@Override
	public boolean equals(Bool other) {
		return value == other.value;
	}
	
	public String toString(){
		switch (value) {
		case 0:
			return "BOT";
		case 1:
			return "FALSE";
		case 2:
			return "TRUE";
		case 3:
			return "TOP";
		default:
			throw new DomainException("Invalid value??");
		}
	}

	@Override
	public Bool getBottom() {
		return BOTTOM;
	}

	@Override
	public boolean isTop() {
		return value==3;
	}

}
