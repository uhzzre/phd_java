package ai.domain;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;

import ai.common.Pair;
import ai.domain.widening.WideningOperator;

public interface AbstractSemanticsIntf<DI extends DomainIntf<DI>> {
	public DI processArgument(DI input, SimpleName argument);
	
	public DI processEmptyEdge(DI input, List<SimpleName> variablesToRemove);

	public DI processExpression(DI input, Expression expression);

	public DI processNewVariable(DI input, SimpleName name, Expression initializerOrNull, boolean asAssignment);
	
	public DI getInitialValue();
	
	public DI processFinallyOrException(DI input, SingleVariableDeclaration excOrNull);

	public DI processConstructorInvocation(DI input, ConstructorInvocation constructorInvocation);

	public DI processSuperConstructorInvocation(DI input, SuperConstructorInvocation superConstructorInvocation);

	/**
	 * 
	 * @param input
	 * @param conditionOrNull
	 * @return pair (positive, negative)
	 */
	public Pair<DI, DI> processCondition(DI input, Expression conditionOrNull);

	
	/**
	 * 
	 * @param input
	 * @param expr
	 * @param switchCases list of switch cases (without default)
	 * @return pair (default, list of values)
	 */
	public Pair<DI, ArrayList<DI>> processSwitchCases(DI input, Expression expr, Expression[] switchCases);
	
	public WideningOperator<DI> getWideningOperator();
}
