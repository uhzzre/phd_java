package ai.domain;

public class DomainException extends RuntimeException{

	private static final long serialVersionUID = -4902924245516075242L;

	public DomainException(String format, Object... args) {
		super(String.format(format, args));
	}

}
