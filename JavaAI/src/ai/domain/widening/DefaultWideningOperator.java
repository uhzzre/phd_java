package ai.domain.widening;

import ai.domain.DomainIntf;

public class DefaultWideningOperator<DI extends DomainIntf<DI>> implements WideningOperator<DI>{

	@Override
	public DI widen(DI left, DI right) {
		return left.widen(right);
	}

	public static <DI extends DomainIntf<DI>> WideningOperator<DI> create(){
		return new DefaultWideningOperator<DI>();
	}
}
