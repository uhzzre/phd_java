package ai.domain.widening;

import ai.domain.DomainIntf;

public interface WideningOperator<DI extends DomainIntf<DI>> {
	public DI widen(DI left, DI right);
}
