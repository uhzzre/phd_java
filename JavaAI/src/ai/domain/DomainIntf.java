package ai.domain;

/**
 * Interface of the abstract domain
 * 
 * @author kjk@mimuw.edu.pl
 *
 * @param <T>
 */
public interface DomainIntf <T extends DomainIntf<T>>{
	public boolean isBottom();
	public boolean isTop();
	public T join(T other);
	public T meet(T other);
	public T widen(T other);
	public boolean leq(T other);
	public boolean equals(T other);
	public T getBottom();
}
