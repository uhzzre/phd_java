package ai.domain.generic;

/**
 * Class is an abstract expression semantics for non-relational numerical
 * domains.
 * 
 * Implementation of backward semantics is based on Phd Thesis of Antoine MINE,
 * Chapter 2.4 - Discovering Properties of Numerical Variables, page 31
 * 
 * @author kjk@mimuw.edu.pl
 * 
 * @param <DI>
 *            abstract domain
 */
public interface AbstractNumericExpressionSemantics<T> {
	public T getZero(); // representative of 0 element

	public T getMinus1to1(); // representative of interval [-1, 1]

	public T minus(T arg);

	public T plus(T leftArg, T rightArg);

	public T minus(T leftArg, T rightArg);

	public T times(T a, T b);

	public T divide(T leftArg, T rightArg);

	public T leftShift(T leftArg, T rightArg);

	public T remainder(T leftArg, T rightArg);

	public T rightShiftSigned(T leftArg, T rightArg);

	public T rightShiftUnsigned(T leftArg, T rightArg);

	public T and(T leftArg, T rightArg);

	public T or(T leftArg, T rightArg);

	public T xor(T leftArg, T rightArg);
	
	public T plus(T arg);
	
	public T complement(T arg);

	public T delta(T arg, int value);

}
