package ai.domain.generic;

import ai.common.Pair;
import ai.domain.DomainIntf;

public class GenericReverseExpressionSemantics<DI extends DomainIntf<DI>> {
	private final AbstractNumericExpressionSemantics<DI> sem;

	public GenericReverseExpressionSemantics(AbstractNumericExpressionSemantics<DI> semantics) {
		this.sem = semantics;
	}
	
	public DI backwardMinus(DI arg, DI result) {
		return arg.meet(sem.minus(result));
	}
	
	public Pair<DI, DI> backwardPlus(DI leftArg, DI rightArg, DI result) {
		return new Pair<DI, DI>(leftArg.meet(sem.minus(result, rightArg)), rightArg.meet(sem.minus(result, leftArg)));
	}

	public Pair<DI, DI> backwardMinus(DI leftArg, DI rightArg, DI result) {
		return new Pair<DI, DI>(leftArg.meet(sem.plus(rightArg, result)), rightArg.meet(sem.minus(leftArg, result)));
	}

	public Pair<DI, DI> backwardTimes(DI leftArg, DI rightArg, DI result) {
		return new Pair<DI, DI>(leftArg.meet(sem.divide(result, rightArg)), rightArg.meet(sem.divide(result, leftArg)));
	}

	private DI adj(DI arg) {
		return sem.plus(arg, sem.getMinus1to1());
	}

	public Pair<DI, DI> backwardDivide(DI leftArg, DI rightArg, DI result) {
		return new Pair<DI, DI>(leftArg.meet(sem.times(rightArg, adj(result))), rightArg.meet(sem.divide(leftArg,
				adj(result)).join(sem.getZero())));
	}


}
