package ai.domain.intervals.eval;

import org.eclipse.jdt.core.dom.ASTNode;

public class EvaluatorException extends RuntimeException{
	private static final long serialVersionUID = 7902334546007120143L;

	public EvaluatorException(String format, Object... args) {
		super(String.format(format, args));
	}
	
	public static EvaluatorException unhandledNode(ASTNode node){
		throw new EvaluatorException("Unhandled node '%s(%s)'", node.getClass(), node);
	}
}