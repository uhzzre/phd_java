package ai.domain;

import org.eclipse.jdt.core.dom.IVariableBinding;

public class Variable implements Comparable<Variable>{
	private final IVariableBinding variableBinding;

	public Variable(IVariableBinding variableBinding) {
		this.variableBinding = variableBinding;
	}
	
	public String toString() {
		return "Var(" + variableBinding.getVariableId() + "#" + variableBinding.getName() + ")";
//		return "Var(" + variableBinding.getName() + ", " + variableBinding.getKey() + ")";
	}
	
	public boolean equals(Object other) {
		if (!(other instanceof Variable))
			return false;
		Variable otherVar = (Variable) other;
		return variableBinding.isEqualTo(otherVar.variableBinding);
	}
	
	@Override
	public int hashCode() {
		return variableBinding.hashCode();
	}
	
	protected int variableOrderId() {
		return variableBinding.getVariableId();
	}
	
	@Override
	public int compareTo(Variable o) {
		return variableOrderId() - o.variableOrderId();
	}
}
