package ai.domain.boolintv;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;

import ai.common.Pair;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.Variable;
import ai.domain.bool.Bool;
import ai.domain.expressions.MainExpressionEvaluator;
import ai.domain.expressions.bool.BooleanEvaluationState;
import ai.domain.expressions.bool.BooleanEvaluator;
import ai.domain.expressions.bool.BooleanExpressionSemantics;
import ai.domain.expressions.integer.BackwardExpressionSemantics;
import ai.domain.generic.GenericReverseExpressionSemantics;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.generic.ProductDomain;
import ai.domain.interval.Interval;
import ai.domain.interval.IntervalBackwardTestSemantics;
import ai.domain.interval.IntervalComparisonEvaluator;
import ai.domain.interval.IntervalExpressionEvaluator;
import ai.domain.interval.IntervalExpressionSemantics;
import ai.domain.interval.IntervalValue;
import ai.domain.interval.IntervalsExpressionSemantics;
import ai.domain.intervals.eval.EvaluationUtils;
import ai.domain.widening.DefaultWideningOperator;
import ai.domain.widening.WideningOperator;

public class BoolIntvSemantics implements
		AbstractSemanticsIntf<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> {
	private class MyBooleanSemantics implements
			BooleanExpressionSemantics<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> {

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, ArrayAccess node) {
			return BooleanEvaluator.createOutput(input, Bool.TOP);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, FieldAccess node) {
			Boolean aValue = EvaluationUtils.tryGetBooleanValue(node);
			Bool value = (aValue != null) ? Bool.get(aValue) : Bool.TOP;
			return BooleanEvaluator.createOutput(input, value);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, InstanceofExpression node) {
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, MethodInvocation node) {
			for (Object arg : node.arguments())
				input = defaultEval.evaluate((Expression) arg, input);
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, SimpleName node) {
			Bool value;
			if (EvaluationUtils.isBooleanType(node)) {
				Boolean aValue = EvaluationUtils.tryGetBooleanValue(node);
				if (aValue != null) { // a constant??
					value = Bool.get(aValue);
				} else {// variable??
					Variable var = EvaluationUtils.tryGetVariable(node);
					if (var == null) { // not a variable, we know nothing :(
						value = Bool.TOP;
					} else
						value = EvaluationUtils.getValueForVariable(input.getLeft(), var);
				}
			} else //unboxing
				value = Bool.TOP;
			return BooleanEvaluator.createOutput(input, value);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, QualifiedName node) {
			Boolean aValue = EvaluationUtils.tryGetBooleanValue(node);
			Bool value = (aValue != null) ? Bool.get(aValue) : Bool.TOP;
			return BooleanEvaluator.createOutput(input, value);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, NullLiteral node) {
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, SuperFieldAccess node) {
			Boolean aValue = EvaluationUtils.tryGetBooleanValue(node);
			Bool value = (aValue != null) ? Bool.get(aValue) : Bool.TOP;
			return BooleanEvaluator.createOutput(input, value);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
				SuperMethodInvocation node) {
			for (Object arg : node.arguments())
				input = defaultEval.evaluate((Expression) arg, input);
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
				ClassInstanceCreation node) {
			for (Object arg : node.arguments())
				input = defaultEval.evaluate((Expression) arg, input);
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, CastExpression node) {
			input = defaultEval.evaluate(node.getExpression(), input);
			return BooleanEvaluationState.create(input, input);
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processComparison(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, InfixExpression node) {
			return defaultEval.evaluateComparison(input, node);
		}

		@Override
		public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processBooleanAssignment(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Expression toWhat,
				Bool value) {
			Variable var = EvaluationUtils.tryGetVariable(toWhat);
			if (var != null)
				return input.setLeft(input.getLeft().updateVariable(var, value));
			else
				// FIXME: ???
				return input;
		}

		@Override
		public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> newBooleanVariable(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Variable var,
				Bool initialValueOrNull, boolean isArgument) {
			if (initialValueOrNull == null)
				initialValueOrNull = Bool.TOP;
			return input.setLeft(input.getLeft().addNewVariable(var, initialValueOrNull, isArgument));
		}

		@Override
		public BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processBooleanBoxing(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Assignment node) {
			input = defaultEval.evaluate(node.getLeftHandSide(), input);
			input = defaultEval.evaluate(node.getRightHandSide(), input);
			return BooleanEvaluator.createOutput(input, Bool.TOP);			
		}

	}

	private class MyIntergerBackwardSemantics
			implements
			BackwardExpressionSemantics<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>, Interval> {

		@Override
		public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processIntegerNode(
				ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, SimpleName node,
				Interval value) {
			if (input.isBottom())
				return input;
			if (value.isBottom())
				return input.getBottom();

			Variable var = EvaluationUtils.tryGetVariable(node);
			if (var == null) // no change in state
				return input;
			else
				return input.setRight(EvaluationUtils.updateValueOfVariable(input.getRight(), var, value));
		}

		@Override
		public Interval getConstantValue(long value) {
			return new Interval(value, value);
		}
	}

	private class MyIntegerSemantics
			implements
			IntervalsExpressionSemantics<Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>>> {

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> newIntegerVariable(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				Variable variable, boolean isArgument) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = input.right;
			Interval value = input.left;
			if (value == null)
				value = Interval.TOP;
			return Pair.create(value, output.setRight(output.getRight().addNewVariable(variable, value, isArgument)));
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				ArrayAccess node) {
			return Pair.create(Interval.TOP, defaultEval.evaluate(node.getIndex(), input.right));
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				FieldAccess node) {
			IntervalValue value = EvaluationUtils.tryGetIntervalValue(node);
			return Pair.create(value != null ? new Interval(value) : Interval.TOP, input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				MethodInvocation node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = input.right;
			for (Object arg : node.arguments())
				output = defaultEval.evaluate((Expression) arg, output);
			return Pair.create(Interval.TOP, output);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				SimpleName node) {
			Interval value;
			if (EvaluationUtils.isIntegerType(node)) {
				IntervalValue aValue = EvaluationUtils.tryGetIntervalValue(node);
				if (aValue != null) { // a constant??
					value = new Interval(aValue);
				} else {// variable??
					Variable var = EvaluationUtils.tryGetVariable(node);
					if (var == null) { // not a variable, we know nothing :(
						value = Interval.TOP;
					} else
						value = EvaluationUtils.getValueForVariable(input.right.getRight(), var);
				}
			} else // unboxing
				value = Interval.TOP;
			return Pair.create(value, input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				QualifiedName node) {
			IntervalValue aValue = EvaluationUtils.tryGetIntervalValue(node);
			return Pair.create((aValue != null) ? new Interval(aValue) : Interval.TOP, input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				SuperFieldAccess node) {
			IntervalValue value = EvaluationUtils.tryGetIntervalValue(node);
			return Pair.create(value != null ? new Interval(value) : Interval.TOP, input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				SuperMethodInvocation node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = input.right;
			for (Object arg : node.arguments())
				output = defaultEval.evaluate((Expression) arg, output);
			return Pair.create(Interval.TOP, output);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				ConditionalExpression node) {
			return EvaluationUtils.evaluateConditionalExpression(node, beval, input, ieval);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				NumberLiteral node, long value) {
			return Pair.create(new Interval(value, value), input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				CharacterLiteral node, long value) {
			return Pair.create(new Interval(value, value), input.right);
		}
		
		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerValueChange(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				Expression ofWhat, int delta, boolean postfixOrPrefix) {
			Interval valueBeforeChange = input.left;
			Interval valueAfterChange = sem.delta(valueBeforeChange, delta);
			Variable var = EvaluationUtils.tryGetVariable(ofWhat);
			NonRelationalDomain<Interval> output = input.right.getRight();
			if (var != null && EvaluationUtils.isIntegerType(ofWhat))
				output = output.updateVariable(var, valueAfterChange);
			return Pair.create(postfixOrPrefix ? valueBeforeChange : valueAfterChange, input.right.setRight(output));
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerUnaryPlus(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input) {
			return input;
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerUnaryMinus(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input) {
			return input.setLeft(sem.minus(input.left));
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerUnaryComplement(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input) {
			return input.setLeft(sem.complement(input.left));
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				ClassInstanceCreation node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = input.right;
			for (Object arg : node.arguments())
				output = defaultEval.evaluate((Expression) arg, output);
			return Pair.create(Interval.TOP, output);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				CastExpression node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = defaultEval.evaluate(
					node.getExpression(), input.right);
			return Pair.create(Interval.TOP, output);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				NullLiteral node) {
			return Pair.create(Interval.TOP, input.right);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerAssignment(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				Expression toWhat) {
			Variable var = EvaluationUtils.tryGetVariable(toWhat);
			if (var == null)
				return input;

			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> output = input.right;
			output = output.setRight(output.getRight().updateVariable(var, input.left));
			return Pair.create(input.left, output);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processIntegerBoxing(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				Assignment node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> res = defaultEval.evaluate(node.getLeftHandSide(), input.right);
			res = defaultEval.evaluate(node.getRightHandSide(), res);
			return Pair.create(Interval.TOP, res);
		}

		@Override
		public Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processFloatNode(
				Pair<Interval, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> input,
				Expression node) {
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> res = defaultEval.evaluate(node, input.right);
			return Pair.create(Interval.TOP, res);
		}

	}

	private final MainExpressionEvaluator<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> defaultEval;
	private final BooleanEvaluator<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> beval;
	private final IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
	private final IntervalComparisonEvaluator<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> iCondEval;
	private final IntervalExpressionEvaluator<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> ieval;

	public BoolIntvSemantics() {
		MyIntegerSemantics eSemantics = new MyIntegerSemantics();
		MyBooleanSemantics boolSemantics = new MyBooleanSemantics();
		this.beval = BooleanEvaluator.create(boolSemantics);
		this.defaultEval = MainExpressionEvaluator.create(this.beval);
		defaultEval.addEvaluator(beval);
		this.ieval = IntervalExpressionEvaluator.create(eSemantics, sem);
		defaultEval.addEvaluator(ieval);
		MyIntergerBackwardSemantics backSemantics = new MyIntergerBackwardSemantics();
		GenericReverseExpressionSemantics<Interval> bes = new GenericReverseExpressionSemantics<Interval>(sem);
		iCondEval = IntervalComparisonEvaluator.create(ieval, new IntervalBackwardTestSemantics(), bes, backSemantics);
		defaultEval.addComparisonEvaluator(iCondEval);
	}

	@Override
	public Pair<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> processCondition(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Expression conditionOrNull) {
		if (conditionOrNull == null)
			return Pair.create(input, input);
		BooleanEvaluationState<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> x = beval
				.evaluateCondition(input, conditionOrNull);
		return Pair.create(x.conditionMet, x.conditionNotMet);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processEmptyEdge(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, List<SimpleName> variablesToRemove) {
		if (input.isBottom())
			return input;
		NonRelationalDomain<Bool> bools = input.getLeft();
		NonRelationalDomain<Interval> intv = input.getRight();
		for(SimpleName name: variablesToRemove){
			Variable var = EvaluationUtils.tryGetVariable(name);
			if (var == null)
				continue;
			if (bools.containsValue(var))
				bools = bools.removeVariable(var);
			else if (intv.containsValue(var))
				intv = intv.removeVariable(var);
		}
		return ProductDomain.create(bools, intv);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processExpression(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Expression expression) {
		return defaultEval.evaluate(expression, input);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processNewVariable(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
			SimpleName name, Expression initializerOrNull, boolean asAssignment) {
		return defaultEval.evaluate(name, initializerOrNull, input, asAssignment);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> getInitialValue() {
		NonRelationalDomain<Bool> initialBools = NonRelationalDomain.getInitialValue();
		NonRelationalDomain<Interval> initialIntervals = NonRelationalDomain.getInitialValue();

		return ProductDomain.create(initialBools, initialIntervals);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processArgument(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, SimpleName argument) {
		return defaultEval.evaluateNewArgument(argument, input);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processFinallyOrException(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
			SingleVariableDeclaration excOrNull) {
		if (input.isBottom())
			return input;
		// process booleans
		NonRelationalDomain<Bool> bools = input.getLeft();
		for (Variable var : bools.getVariables())
			bools = bools.updateVariable(var, Bool.TOP);
		// process intervals
		NonRelationalDomain<Interval> intv = input.getRight();
		for (Variable var : intv.getVariables())
			intv = intv.updateVariable(var, new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY));
		return ProductDomain.create(bools, intv);
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processConstructorInvocation(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
			ConstructorInvocation constructorInvocation) {
		if (constructorInvocation.arguments() == null)
			return input;
		for(Object arg: constructorInvocation.arguments())
			input = defaultEval.evaluate((Expression)arg, input);
		return input;
	}

	@Override
	public ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> processSuperConstructorInvocation(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input,
			SuperConstructorInvocation superConstructorInvocation) {
		if (superConstructorInvocation.arguments() == null)
			return input;
		for(Object arg: superConstructorInvocation.arguments())
			input = defaultEval.evaluate((Expression) arg, input);
		return input;
	}

	@Override
	public Pair<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>, ArrayList<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>>> processSwitchCases(
			ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> input, Expression expr,
			Expression[] switchCases) {
		if (!EvaluationUtils.isIntegerType(expr))
			return EvaluationUtils.defaultSwitchProcess(defaultEval, input, expr, switchCases);
		return EvaluationUtils.evaluateSwitchInteger(input, expr, switchCases, iCondEval);
	}

	@Override
	public WideningOperator<ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> getWideningOperator() {
		return DefaultWideningOperator.create();
	}
}
