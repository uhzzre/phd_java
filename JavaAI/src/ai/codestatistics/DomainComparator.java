package ai.codestatistics;

import java.util.HashSet;
import java.util.Set;

import ai.domain.DomainIntf;
import ai.domain.Variable;
import ai.domain.bool.Bool;
import ai.domain.boxes.IntegerBoxes;
import ai.domain.boxes.IntegerBoxesHelper;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.generic.ProductDomain;
import ai.domain.interval.Interval;
import ai.test.DummyVariable;

public class DomainComparator {
	public static enum ComparisonResult {EQUAL, LEFT, RIGHT, UNCOMPARABLE};
	private static ComparisonResult compareProductDomains(ProductDomain left, ProductDomain right) {
		ComparisonResult leftResult = compare(left.getLeft(), right.getLeft());
		ComparisonResult rightResult = compare(left.getRight(), right.getRight());
		
		if (leftResult == ComparisonResult.UNCOMPARABLE || rightResult == ComparisonResult.UNCOMPARABLE)
			return ComparisonResult.UNCOMPARABLE;
		if (leftResult == ComparisonResult.LEFT && rightResult == ComparisonResult.RIGHT ||
				rightResult == ComparisonResult.LEFT && leftResult == ComparisonResult.RIGHT)
			return ComparisonResult.UNCOMPARABLE;
		if (leftResult == ComparisonResult.EQUAL)
			return rightResult;
		return rightResult;
	}

	private static ComparisonResult getResult(boolean leftLeqRight, boolean rightLeqLeft) {
		if (leftLeqRight && rightLeqLeft)
			return ComparisonResult.EQUAL;
		if (leftLeqRight)
			return ComparisonResult.LEFT;
		if (rightLeqLeft)
			return ComparisonResult.RIGHT;
		return ComparisonResult.UNCOMPARABLE;
	}
	
	private static ComparisonResult compareNonRelationals(NonRelationalDomain left, NonRelationalDomain right){
		return getResult(left.leq(right), right.leq(left));
	}
	
	private static ComparisonResult compareBoxes(IntegerBoxes left, IntegerBoxes right) {
		return getResult(left.leq(right), right.leq(left));
	}
	
	private static ComparisonResult withIntegerBoxes(IntegerBoxes left, NonRelationalDomain right) {
		Set<Variable> leftVars = new HashSet<Variable>(left.getVariables());
		if (!leftVars.equals(right.getVariables()))
			return ComparisonResult.UNCOMPARABLE;
		return compareBoxes(left, IntegerBoxesHelper.fromIntervalBox(right));
	}
	
	private static ComparisonResult reverse(ComparisonResult result) {
		if (result == ComparisonResult.LEFT)
			return ComparisonResult.RIGHT;
		if (result == ComparisonResult.RIGHT)
			return ComparisonResult.LEFT;
		return result;
	}
	
	public static ComparisonResult compare(DomainIntf<?> left, DomainIntf<?> right) {
		if (left.isBottom()) {
			if (right.isBottom())
				return ComparisonResult.EQUAL;
			else
				return ComparisonResult.RIGHT;
		}
		if (right.isBottom())
			return ComparisonResult.LEFT;
		if (left instanceof ProductDomain) {
			if (right instanceof ProductDomain)
				return compareProductDomains((ProductDomain) left, (ProductDomain)right);
			return ComparisonResult.UNCOMPARABLE;
		}
		if (left instanceof NonRelationalDomain && right instanceof NonRelationalDomain)
			return compareNonRelationals((NonRelationalDomain) left, (NonRelationalDomain)right);
		if (left instanceof IntegerBoxes && right instanceof IntegerBoxes)
			return compareBoxes((IntegerBoxes)left, (IntegerBoxes) right);
		if (left instanceof IntegerBoxes || right instanceof NonRelationalDomain)
			return withIntegerBoxes((IntegerBoxes)left, (NonRelationalDomain) right);
		if (right instanceof IntegerBoxes || left instanceof NonRelationalDomain)
			return reverse(withIntegerBoxes((IntegerBoxes)right, (NonRelationalDomain) left));
		return ComparisonResult.UNCOMPARABLE;
	}
	
	public static void main(String[] args) {
		Variable var = new DummyVariable("aaa", 0);
		NonRelationalDomain<Bool> bools = NonRelationalDomain.getInitialValue();
		bools = bools.addNewVariable(var, Bool.TRUE, false);
		NonRelationalDomain<Interval> intv = NonRelationalDomain.getInitialValue();
		ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> x = ProductDomain.create(bools, intv);
		compare(x, x);
	}
}
