package ai.codestatistics.preferences;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import ai.JavaAIPlugin;

/**
 * Class used to initialize default preference values.
 */
public class AnalysisPreferences extends AbstractPreferenceInitializer {

	public static final String DOMAINS_PREFERENCE = "cs:domains";
	public static final String ANALYSIS_TIMEOUT = "cs:analysis_timeout";
	
	public static final String DOMAINS_DEFAULT = "intv=intv;boxes_empty=boxes";
	public static final int TIMEOUT_DEFAULT = 60;
	
	private static final String PREFERENCE_DELIMITER = ";";
	private static final String NAME_VALUE_DELIMITER = "=";
	
	public static boolean verifyDomainName(String name) {
		return !name.contains(PREFERENCE_DELIMITER) && !name.contains(NAME_VALUE_DELIMITER); 
	}
	
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = JavaAIPlugin.getDefault().getPreferenceStore();
		store.setDefault(DOMAINS_PREFERENCE, DOMAINS_DEFAULT);
		store.setDefault(ANALYSIS_TIMEOUT, TIMEOUT_DEFAULT);
	}
	
	private static String[] convert(String multiValue) {
		StringTokenizer tokenizer = new StringTokenizer(multiValue, PREFERENCE_DELIMITER);
		int tokenCount = tokenizer.countTokens();
		String[] elements = new String[tokenCount];
		for(int i=0; i< tokenCount; i++)
			elements[i] = tokenizer.nextToken();
		return elements;
	}
	
	public static List<DomainEntry> loadDomains(String desc) {
		List<DomainEntry> result = new LinkedList<DomainEntry>();
		for(String domainDesc: convert(desc)){
			String[] keyValue = domainDesc.split(NAME_VALUE_DELIMITER);
			DomainEntry entry = new DomainEntry(keyValue[0], keyValue[1]);
			result.add(entry);
		}
		return result;
	}

	public static void saveDomains(IPreferenceStore preferenceStore, List<DomainEntry> entries) {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for(DomainEntry entry: entries){
			if (first)
				first = false;
			else
				result.append(PREFERENCE_DELIMITER);
			result.append(entry.name);
			result.append(NAME_VALUE_DELIMITER);
			result.append(entry.repr);
		}
		preferenceStore.setValue(DOMAINS_PREFERENCE, result.toString());
	}

}
