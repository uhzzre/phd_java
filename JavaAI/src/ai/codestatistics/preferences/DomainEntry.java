package ai.codestatistics.preferences;

public class DomainEntry {
	public final String repr;
	public final String name;

	public DomainEntry(String name, String repr) {
		this.repr = repr;
		this.name = name;
	}
	
	public String getDescription() {
		if (repr.equals(INTV))
			return name + ": Bool x Intv";
		if (repr.equals(BOXES))
			return name + ": Bool x Boxes, empty thresholds";
		String[] items = repr.split(SEPARATOR);
		String count = items[2];
		if (items[1].equals(BOXES_THRESHOLD_BOX))
			return name + ": Bool x Boxes, thresholds from surrounding box, first "+ count + " arguments";
		if (items[1].equals(BOXES_THRESHOLD_ARG))
			return name + ": Bool x Boxes, thresholds from local special points, first "+ count + " arguments";
		throw new RuntimeException("Unknown description");
	}
	
	public static final String INTV = "intv";
	public static final String BOXES = "boxes";
	public static final String BOXES_THRESHOLD_BOX = "box";
	public static final String BOXES_THRESHOLD_ARG = "arg";
	public static final String SEPARATOR = ":";
	
	public static String reprIntv(){
		return INTV;
	}
	
	public static String reprBoxesEmpty(){
		return BOXES;
	}

	public static String reprBoxesBox(int count){
		return BOXES + SEPARATOR + BOXES_THRESHOLD_BOX + SEPARATOR + count;
	}

	public static String reprBoxesArg(int count){
		return BOXES + SEPARATOR + BOXES_THRESHOLD_ARG + SEPARATOR + count;
	}
}
