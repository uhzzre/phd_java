package ai.codestatistics.preferences;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ai.JavaAIPlugin;

public class AnalysisPluginPreferencesPage extends PreferencePage
	implements IWorkbenchPreferencePage {

    // list of chosen domains
    private List domainList;

    private Button newButton;
    private Button removeButton;
    
    private java.util.List<DomainEntry> entries = new LinkedList<DomainEntry>();

	private Text timeoutText;
    
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(JavaAIPlugin.getDefault().getPreferenceStore());
	}
	
	private Set<String> getExistingNames(){
		Set<String> result = new HashSet<String>();
		for(DomainEntry entry: entries)
			result.add(entry.name);
		return result;
	}

    private void openEditorDialog() {
        DomainConfigDialog dialog = new DomainConfigDialog(getShell(), getExistingNames());
        int code = dialog.open();
        if (code == DomainConfigDialog.OK) {
        	DomainEntry entry = dialog.getSelectedDomainSettings();
        	entries.add(entry);
        	domainList.add(entry.getDescription());
        }
    }	
	private void addDomainsList(Composite contents) {
        Composite leftPart = new Composite(contents, SWT.NULL);
        leftPart.setLayout(new GridLayout());
        leftPart.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        Label label = new Label(leftPart, SWT.LEFT);
        label.setText("Computed domains:");
        label.setLayoutData(new GridData());
        domainList = new List(leftPart, SWT.SINGLE | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
//        domainList.setItems(getBuilderItems());
        domainList.setLayoutData(new GridData(GridData.FILL_BOTH));
        domainList.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
                int index = domainList.getSelectionIndex();
                removeButton.setEnabled(index >= 0);
            }});

        Composite rightPart = new Composite(contents, SWT.NULL);
        rightPart.setLayout(new GridLayout());
        rightPart.setLayoutData(new GridData(GridData.FILL_VERTICAL));
        
        Label empty = new Label(rightPart, SWT.LEFT);
        empty.setLayoutData(new GridData());
        
        newButton = new Button(rightPart, SWT.PUSH);
        newButton.setEnabled(true);
        newButton.setText("New...");
        newButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING | GridData.FILL_HORIZONTAL));
        newButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
            	openEditorDialog();
            }});
        
        removeButton = new Button(rightPart, SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.setText("Remove");
        removeButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING | GridData.FILL_HORIZONTAL));
        removeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
            	removeButton.setEnabled(false);
            	int selectedIdx = domainList.getSelectionIndex();
            	if (selectedIdx < 0)
            		return;
            	entries.remove(selectedIdx);
            	domainList.remove(selectedIdx);
            }});

        Label filler = new Label(rightPart, SWT.LEFT);
        filler.setLayoutData(new GridData(GridData.FILL_VERTICAL));
	}
	
	private void addTimeoutSettings(Composite contents) {
        Composite tc = new Composite(contents, SWT.NULL);
        tc.setLayout(new RowLayout());
        GridData gl = new GridData(GridData.FILL_HORIZONTAL);
        gl.horizontalSpan = 2;
        tc.setLayoutData(gl);
        
        Label label = new Label(tc, SWT.LEFT | SWT.CENTER);
        label.setText("Timeout:");
        
		timeoutText = new Text(tc, SWT.BORDER | SWT.NO_SCROLL | SWT.CENTER);
		timeoutText.addVerifyListener(Shared.NUMBER_VERIFIER);

        Label secondsLabel = new Label(tc, SWT.LEFT | SWT.CENTER);
        secondsLabel.setText("seconds");
	}

	@Override
	protected Control createContents(Composite parent) {
	    Composite contents = new Composite(parent, SWT.NULL);
        GridLayout gl = new GridLayout();
        gl.numColumns = 2;
        contents.setLayout(gl);
        
        addTimeoutSettings(contents);
	    addDomainsList(contents);
        
		loadDomains(getPreferenceStore().getString(AnalysisPreferences.DOMAINS_PREFERENCE));
		timeoutText.setText(""+getPreferenceStore().getInt(AnalysisPreferences.ANALYSIS_TIMEOUT));

	    return contents;
	}
	
	@Override
    public boolean performOk() {
        boolean ok = super.performOk();
        AnalysisPreferences.saveDomains(getPreferenceStore(), entries);
        getPreferenceStore().setValue(AnalysisPreferences.ANALYSIS_TIMEOUT, Integer.parseInt(timeoutText.getText()));
        return ok;
    }
	
	private void loadDomains(String desc) {
		domainList.removeAll();
		entries = AnalysisPreferences.loadDomains(desc);
		for(DomainEntry entry: entries)
			domainList.add(entry.getDescription());
	}

    /**
     * Called when defaults-button is pressed.
     * Sets all field values to defaults.
     * Currently does nothing to builder paths.
     */
	@Override
    public void performDefaults() {
        super.performDefaults();
        loadDomains(AnalysisPreferences.DOMAINS_DEFAULT);
        timeoutText.setText(""+getPreferenceStore().getInt(AnalysisPreferences.ANALYSIS_TIMEOUT));
    }	

}