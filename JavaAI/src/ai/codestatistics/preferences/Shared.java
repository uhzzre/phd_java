package ai.codestatistics.preferences;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Text;

public class Shared {
	static VerifyListener NUMBER_VERIFIER = new VerifyListener() {
		@Override
		public void verifyText(VerifyEvent e) {
			String oldText = ((Text)e.widget).getText();
			String newText = oldText.substring(0, e.start) + e.text + oldText.substring(e.end);

			try {
				int value = Integer.parseInt(newText);
				e.doit = value >= 1;
			} catch (NumberFormatException exc) {
				e.doit = false;
			}
		}
	};
}
