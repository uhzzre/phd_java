package ai.plugin.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_TIMEOUT = "timeout";
	public static final String P_BOXES_THRESHOLDS = "boxesThresholds";
}
