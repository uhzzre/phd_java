package ai.plugin.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import ai.JavaAIPlugin;

/**
 * Class used to initialize default preference values.
 */
public class JavaAIPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = JavaAIPlugin.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_TIMEOUT, 60);
		store.setDefault(PreferenceConstants.P_BOXES_THRESHOLDS, "");
	}

}
