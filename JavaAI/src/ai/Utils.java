package ai;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ITreeSelection;

public class Utils {
	public static List<ICompilationUnit> getSelectedIcu(ITreeSelection selection)
			throws JavaModelException {
		List<ICompilationUnit> result = new LinkedList<ICompilationUnit>();
		Iterator<?> iter = selection.iterator();
		while (iter.hasNext()) {
			Object obj = iter.next();
			if (obj instanceof ICompilationUnit) {
				result.add((ICompilationUnit) obj);
			} else if (obj instanceof IPackageFragment) {
				IPackageFragment pf = (IPackageFragment) obj;
				for (ICompilationUnit icu : pf.getCompilationUnits())
					result.add(icu);
			} else if (obj instanceof IJavaProject) {
				IJavaProject jp = (IJavaProject) obj;
				for (IPackageFragmentRoot packageFragment : jp
						.getAllPackageFragmentRoots()) {
					if (packageFragment.getKind() != IPackageFragmentRoot.K_SOURCE)
						continue;
					for (IJavaElement element : packageFragment.getChildren()) {
						if (element.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
							for (ICompilationUnit icu : ((IPackageFragment) element)
									.getCompilationUnits())
								result.add(icu);
						} else
							throw new RuntimeException(
									"AAAAAAAA: Nie package fragment!!!");
					}
				}

			}
		}
		return result;
	}
	
	public static CompilationUnit parse(ICompilationUnit unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit); // set source
		parser.setResolveBindings(true); // we need bindings later on
		return (CompilationUnit) parser.createAST(null /* IProgressMonitor */); // parse
	}	
}
