package ai.common;

import org.eclipse.jdt.core.dom.ASTNode;

public class ASTUtils {
	public static ASTNode findAncestorOfType(ASTNode node, int... types) {
		node = node.getParent();
		while (node != null) {
			for(int type: types){
				if (node.getNodeType() == type)
					return node;
			}
			node = node.getParent();
		};
		return null;
	}
}
