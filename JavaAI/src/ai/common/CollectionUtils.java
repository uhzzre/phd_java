package ai.common;

import java.util.ArrayList;

public class CollectionUtils {
	public static interface Predicate<T> {
		boolean meets(T obj);
	}

	/**
	 * Leaves items that meet the predicate
	 * 
	 * @param input
	 * @param filter
	 * @return
	 */
	public static <T> ArrayList<T> filter(ArrayList<T> input, Predicate<T> filter) {
		ArrayList<T> result = new ArrayList<T>();
		for(T item: input)
			if (filter.meets(item))
				result.add(item);
		return result;
	}
}
