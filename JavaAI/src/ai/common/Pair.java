package ai.common;

public final class Pair<K1, K2> {
	public final K1 left;
	public final K2 right;

	public Pair(K1 left, K2 right) {
		this.left = left;
		this.right = right;
	}
	
	public Pair<K1, K2> setRight(K2 newValue) {
		return new Pair<K1, K2>(left, newValue);
	}

	public Pair<K1, K2> setLeft(K1 newValue) {
		return new Pair<K1, K2>(newValue, right);
	}

	public Pair<K2, K1> swap(){
		return new Pair<K2, K1>(right, left);
	}
	
	public boolean equals(Pair<K1, K2> other) {
		return left.equals(other.left) && right.equals(other.right);
	}
	
	public String toString() {
		return String.format("Pair<%s,%s>", left, right);
	}
	
	/**
	 * Helper function to shorten writing generics
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static <K1, K2> Pair<K1, K2> create(K1 left, K2 right) {
		return new Pair<K1, K2>(left, right);
	}
}
