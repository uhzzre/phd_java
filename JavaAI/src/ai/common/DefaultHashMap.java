package ai.common;

import java.util.HashMap;

public class DefaultHashMap<K, V> extends HashMap<K, V> implements DefaultMap<K, V>{
	private static final long serialVersionUID = -1948113289847887450L;

	public V setDefault(K key, V defaultValue) {
		if (containsKey(key)) {
			return get(key);
		}
		put(key, defaultValue);
		return defaultValue;
	}

	@Override
	public V getDefault(K key, V defaultValue) {
		if (containsKey(key)) {
			return get(key);
		}
		return defaultValue;
	}
}
