package ai.common;

public class MutableInt {
	private int value;
	
	public MutableInt(int initialValue) {
		value = initialValue;
	}
	
	public int inc(){ 
		return value++;
	}
	
	public int get(){
		return value;
	}
}

