package ai.common;


public class FatalAIError extends RuntimeException{
	private static final long serialVersionUID = -6962156055491767872L;

	public FatalAIError(String format, Object... args) {
		super(String.format(format, args));
	}

}
