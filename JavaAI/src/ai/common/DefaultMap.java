package ai.common;

import java.util.Map;

public interface DefaultMap<K, V>  extends Map<K, V>{

	public V getDefault(K key, V defaultValue);
	public V setDefault(K key, V defaultValue);
}
