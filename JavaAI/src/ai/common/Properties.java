package ai.common;

import java.util.HashMap;
import java.util.Map;

public class Properties {
	private static volatile Properties instance = null;
	
	public static Properties getInstance() {
		if (instance == null)
			synchronized (Properties.class) {
				if (instance == null)
					instance = new Properties();
			}
		return instance;
	}

	private final Map<String, String> data;

	private Properties(){
		this.data = new HashMap<String, String>();
	}
	
	public void put(String key, String value) {
		this.data.put(key, value);
	}
	
	public String get(String key, String defaultValue) {
		String result = data.get(key);
		return (result == null) ? defaultValue : result;
	}
}
