package ai.cfg;

public class GraphContext {
	private final CFGVertice previousVertice;
	private final CFGVertice nextVertice;
	public GraphContext(CFGVertice previousVertice, CFGVertice nextVertice) {
		this.previousVertice = previousVertice;
		this.nextVertice = nextVertice;
	}
	
	public CFGVertice getPreviousVertice(){
		return previousVertice;
	}
	
	public CFGVertice getNextVertice() {
		return nextVertice;
	}
}
