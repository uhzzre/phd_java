package ai.cfg;

public class CFGException extends RuntimeException {

	public CFGException(String format, Object... args) {
		super(String.format(format, args));
	}

	private static final long serialVersionUID = -3449396903214732260L;

}
