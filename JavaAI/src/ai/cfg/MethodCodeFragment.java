package ai.cfg;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

public class MethodCodeFragment implements InterestingCodeFragment {
	private final MethodDeclaration md;

	public MethodCodeFragment(MethodDeclaration md) {
		this.md = md;
	}

	@Override
	public Block getBody() {
		return md.getBody();
	}

	@Override
	public ASTNode getNode() {
		return md;
	}

	@Override
	public List<SingleVariableDeclaration> parameters() {
		List<SingleVariableDeclaration> result = new LinkedList<SingleVariableDeclaration>();
		for(Object param: md.parameters())
			result.add((SingleVariableDeclaration) param);
		return result;
	}
	
	public String toString(){
		return "MethodCodeFragment: " + md.getName()+ ":"+md.parameters();
	}

	private static int methodIdxGen = 0;
	private static Map<MethodDeclaration, String> generatedNames = new HashMap<MethodDeclaration, String>();
	
	@Override
	public String getUniqueName() {
		StringBuilder parameterTypes = new StringBuilder("(");
		boolean first = true;
		for(Object obj: md.parameters()){
			if (first)
				first = false;
			else
				parameterTypes.append(", ");		
			parameterTypes.append(((SingleVariableDeclaration)obj).getType().toString());
		}
		parameterTypes.append(")");

		IMethodBinding binding = md.resolveBinding();
		if (binding == null) {
//			log.error("Binding not resolved, probably project path is invalid: " + node);
			if (generatedNames.containsKey(md))
				return generatedNames.get(md);
			String name = "Dummy_method_location_" + methodIdxGen++ + ":" + md.getName() + parameterTypes;
			generatedNames.put(md, name);
			return name;
		}
		return binding.getDeclaringClass().getBinaryName() + ":" + md.getName()+ parameterTypes;
	}

	@Override
	public Javadoc getJavadoc() {
		return md.getJavadoc();
	}
}