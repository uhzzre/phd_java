package ai.cfg;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;


/**
 * Either MethodDeclaration or Initializer
 * 
 * @author kjk@mimuw.edu.pl
 *
 */
public interface InterestingCodeFragment {
	public Block getBody();
	public ASTNode getNode();
	public List<SingleVariableDeclaration> parameters();
	public String getUniqueName();
	public Javadoc getJavadoc();
}
