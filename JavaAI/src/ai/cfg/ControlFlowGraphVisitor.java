package ai.cfg;

import ai.cfg.edges.CFGMultiTargetEdge.CFGSingleEdge;


public interface ControlFlowGraphVisitor {
	
	public boolean visitVertice(CFGVertice vertice);
	
	public boolean visitEdge(CFGSingleEdge edge);
}
