package ai.cfg;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class MethodFinder extends ASTVisitor {
	private final Collection<InterestingCodeFragment> methods;
	private MethodFinder() {
		super();
		methods = new LinkedList<InterestingCodeFragment>();
	}
	
	public boolean visit(MethodDeclaration node) {
		methods.add(new MethodCodeFragment(node));
		return true;
	}

	public boolean visit(Initializer node) {
		methods.add(new InitializerCodeFragment(node));
		return true;
	}
	
	public static Collection<InterestingCodeFragment> findMethods(CompilationUnit cu) {
		MethodFinder visitor = new MethodFinder();
		cu.accept(visitor);
		return Collections.unmodifiableCollection(visitor.methods);
	}
}
