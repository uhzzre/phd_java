package ai.cfg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

import ai.cfg.edges.CFGMultiTargetEdge;
import ai.cfg.edges.CFGMultiTargetEdge.CFGSingleEdge;

public class CFGVertice {
	private final ASTNode node;
	private String description;
	private final ArrayList<CFGMultiTargetEdge> multiEdges = new ArrayList<CFGMultiTargetEdge>();
	private final ArrayList<CFGSingleEdge> singleEdges = new ArrayList<CFGSingleEdge>();

	public CFGVertice(ASTNode node){
		this.node = node;
	}
	
	public ASTNode getNode(){
		return node;
	}
	
	void addEdge(CFGMultiTargetEdge out) {
		assert out.source == this;
		multiEdges.add(out);
		for(CFGSingleEdge edge: out.singleEdges)
			singleEdges.add(edge);
	}
	
	void addDescription(String description) {
		String prefix = this.description == null ? "" : this.description + "/";
		this.description = prefix + description;
	}
	
	public String getDescription() {
		return this.description;
	}

	public List<CFGSingleEdge> getOutgoingEdges() {
		return Collections.unmodifiableList(singleEdges);
		
	}

	public List<CFGMultiTargetEdge> getOutgoingMultiEdges() {
		return Collections.unmodifiableList(multiEdges);
	}
	
	public String toString() {
		return description != null ? description : super.toString();
	}
}
