package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.ConstructorInvocation;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class ConstructorInvocationEdge extends CFGMultiTargetEdge{
	public final ConstructorInvocation stmt;

	public ConstructorInvocationEdge(CFGVertice source, CFGVertice target, ConstructorInvocation stmt) {
		super(source, target);
		this.stmt = stmt;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processConstructorInvocation(input, stmt));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		return "CFG:This("+stmt.toString()+")";
	}
}
