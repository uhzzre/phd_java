package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class ExpressionStatementEdge extends CFGMultiTargetEdge{
	public final Expression expression;

	public ExpressionStatementEdge(CFGVertice source, CFGVertice target, Expression expression) {
		super(source, target);
		this.expression = expression;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processExpression(input, expression));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		return "CFG:ExpressionStatementEdge("+expression.toString()+")";
	}
}
