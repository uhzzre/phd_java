package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;

import ai.cfg.CFGVertice;
import ai.common.Pair;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class ConditionalEdge extends CFGMultiTargetEdge {
	public final Expression conditionOrNull;
	
	public ConditionalEdge(CFGVertice source, CFGVertice positiveTarget, 
			CFGVertice negativeTarget, Expression conditionOrNull) {
		super(source, positiveTarget, negativeTarget);
		this.conditionOrNull = conditionOrNull;
	}
	
	public String conditionToString() {
		return (conditionOrNull == null) ? "?" : conditionOrNull.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		Pair<DI, DI> res = semantics.processCondition(input, conditionOrNull);
		return Arrays.asList(res.left, res.right);
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		boolean positiveBranch = targetIdx == 0;
		return "CFG:ConditionalEdge("+positiveBranch+","+conditionToString()+")";
	}
}
