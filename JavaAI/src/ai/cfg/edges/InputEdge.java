package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import ai.cfg.CFGVertice;
import ai.cfg.InterestingCodeFragment;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;


public class InputEdge extends CFGMultiTargetEdge{
	private final InterestingCodeFragment codeFragment;

	public InputEdge(CFGVertice target, InterestingCodeFragment codeFragment) {
		super(null, target);
		this.codeFragment = codeFragment;
	}
	
//	public String toString() {
//		return "CFG:IN("+method.getName()+ ":"+method.parameters()+")";
//	}
//	
	@SuppressWarnings("unchecked")
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input){
		for(SingleVariableDeclaration parameter: codeFragment.parameters())
			input = semantics.processArgument(input, parameter.getName());
		return Arrays.asList(input);
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		return "CFG:IN("+codeFragment.toString()+")";
	}
}
