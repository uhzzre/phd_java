package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.SuperConstructorInvocation;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class SuperConstructorInvocationEdge extends CFGMultiTargetEdge{
	public final SuperConstructorInvocation stmt;

	public SuperConstructorInvocationEdge(CFGVertice source, CFGVertice target, SuperConstructorInvocation stmt) {
		super(source, target);
		this.stmt = stmt;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processSuperConstructorInvocation(input, stmt));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		return "CFG:This("+stmt.toString()+")";
	}
}
