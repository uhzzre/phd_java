package ai.cfg.edges;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;

import ai.cfg.CFGVertice;
import ai.common.Pair;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;


public class SwitchEdge extends CFGMultiTargetEdge{
	
	public final Expression expr;
	public final Expression[] cases;

	public SwitchEdge(CFGVertice source, Expression expr, CFGVertice defaultTarget, 
			List<Pair<CFGVertice, Expression>> cases) {
		super(source, func(defaultTarget, cases));
		this.cases = new Expression[cases.size()];
		int i=0;
		for(Pair<CFGVertice, Expression> aCase: cases)
			this.cases[i++] = aCase.right;
		this.expr = expr;
	}

	private static CFGVertice[] func(CFGVertice defaultTarget, List<Pair<CFGVertice, Expression>> cases) {
		CFGVertice[] targets = new CFGVertice[cases.size() + 1];
		targets[0] = defaultTarget;
		int i=1;
		for(Pair<CFGVertice, Expression> aCase: cases) {
			if (aCase.right == null)
				throw new RuntimeException("Invalid usage");
			targets[i++] = aCase.left;
		}
		return targets;
	}

	@Override
	public <DI extends DomainIntf<DI>> ArrayList<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		ArrayList<DI> result = new ArrayList<DI>(cases.length+1);
		Pair<DI, ArrayList<DI>> res = semantics.processSwitchCases(input, expr, cases);
		result.add(res.left);
		result.addAll(res.right);
		return result;
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		String message = (targetIdx == 0) ? "default" : cases[targetIdx-1].toString();
		return "CFG:Switch(" + message + ")";
	}
}
