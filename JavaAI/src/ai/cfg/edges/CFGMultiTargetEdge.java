package ai.cfg.edges;

import java.util.ArrayList;
import java.util.List;

import ai.cfg.CFGVertice;
import ai.common.Pair;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public abstract class CFGMultiTargetEdge {
	public static final class CFGSingleEdge {
		public final CFGMultiTargetEdge edge;
		public final int edgeIdx;
		public final CFGVertice target;
		public final CFGVertice source;

		public CFGSingleEdge(CFGVertice source, CFGVertice target, CFGMultiTargetEdge edge, int edgeIdx) {
			this.source = source;
			this.target = target;
			this.edge = edge;
			this.edgeIdx = edgeIdx;
		}

		public final String toString() {
			return edge.toString(edgeIdx);
		}
	}

	public final CFGVertice source;
	public final CFGSingleEdge[] singleEdges;

	protected CFGMultiTargetEdge(CFGVertice source, CFGVertice... targets) {
		this.source = source;
		this.singleEdges = new CFGSingleEdge[targets.length];
		for (int i = 0; i < targets.length; i++)
			singleEdges[i] = new CFGSingleEdge(source, targets[i], this, i);
	}

	public final String toString() {
		return "CFG:" + getClass().getName();
	}

	public final String toString(int edgeIdx) {
		if (edgeIdx < 0 || edgeIdx >= singleEdges.length)
			throw new RuntimeException("Invalid usage");
		return toStringVerified(edgeIdx);
	}

	protected abstract String toStringVerified(int edgeIdx);

	public <DI extends DomainIntf<DI>> List<Pair<CFGSingleEdge, DI>> acceptVerified(
			AbstractSemanticsIntf<DI> semantics, DI input) {
		List<DI> analysisResult;
		if (input.isBottom()) {
			analysisResult = new ArrayList<DI>(singleEdges.length);
			for (int i = 0; i < singleEdges.length; i++)
				analysisResult.add(input);
		} else {
			try {
				analysisResult = this.accept(semantics, input);
			} catch (RuntimeException e) {
				System.err.println("Error evaluating edge:" + this);
				throw e;
			}
			if (analysisResult.size() != singleEdges.length)
				throw new RuntimeException("Invalid usage");
		}
		List<Pair<CFGSingleEdge, DI>> result = new ArrayList<Pair<CFGSingleEdge, DI>>(singleEdges.length);
		int i = 0;
		for (DI singleResult : analysisResult)
			result.add(Pair.create(singleEdges[i++], singleResult));
		return result;
	}

	protected abstract <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input);
}
