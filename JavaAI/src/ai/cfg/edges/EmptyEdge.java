package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.SimpleName;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class EmptyEdge extends CFGMultiTargetEdge{
	public final List<SimpleName> variablesToRemove;

	public EmptyEdge(CFGVertice source, CFGVertice target, List<SimpleName> variablesToRemove) {
		super(source, target);
		this.variablesToRemove = variablesToRemove;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processEmptyEdge(input, variablesToRemove));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		return "CFG:EmptyEdge()";
	}
}
