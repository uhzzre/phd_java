package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;


public class NewVariableEdge extends CFGMultiTargetEdge {
	public final SimpleName name;
	public final Expression initializerOrNull;
	public final boolean asAssignment;

	public NewVariableEdge(CFGVertice source, CFGVertice target, SimpleName name,
			Expression initializerOrNull, boolean asAssignment) {
		super(source, target);
		this.name = name;
		this.initializerOrNull = initializerOrNull;
		this.asAssignment = asAssignment;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processNewVariable(input, name, initializerOrNull, asAssignment));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		String value = (initializerOrNull == null) ? "" : "=" + initializerOrNull;
		return "CFG:NewVariableEdge("+name.toString() + value + ")";
	}
}
