package ai.cfg.edges;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import ai.cfg.CFGVertice;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;

public class FinallyOrCatchEdge extends CFGMultiTargetEdge{

	public final SingleVariableDeclaration exceptionOrNull;

	public FinallyOrCatchEdge(CFGVertice source, CFGVertice target, SingleVariableDeclaration exceptionOrNull) {
		super(source, target);
		this.exceptionOrNull = exceptionOrNull;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <DI extends DomainIntf<DI>> List<DI> accept(AbstractSemanticsIntf<DI> semantics, DI input) {
		return Arrays.asList(semantics.processFinallyOrException(input, exceptionOrNull));
	}

	@Override
	protected String toStringVerified(int targetIdx) {
		StringBuilder result = new StringBuilder("CFG:FinallyOrCatchEdge(");
		if (exceptionOrNull != null)
			result.append(exceptionOrNull.toString());
		result.append(")");
		return result.toString();
	}
}
