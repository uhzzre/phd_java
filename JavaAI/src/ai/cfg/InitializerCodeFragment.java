package ai.cfg;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class InitializerCodeFragment implements InterestingCodeFragment {
	private static final List<SingleVariableDeclaration> EMPTY = new LinkedList<SingleVariableDeclaration>();
	private final Initializer init;

	public InitializerCodeFragment(Initializer init) {
		this.init = init;
	}

	@Override
	public Block getBody() {
		return init.getBody();
	}

	@Override
	public ASTNode getNode() {
		return init;
	}

	@Override
	public List<SingleVariableDeclaration> parameters() {
		return EMPTY;
	}
	
	public String toString() {
		return "InitializerCodeFragment(" + init.getParent() +")";
	}

	@Override
	public String getUniqueName() {
		TypeDeclaration td = (TypeDeclaration) init.getParent();
		return "Initializer:"+td.getName().getFullyQualifiedName();
	}

	@Override
	public Javadoc getJavadoc() {
		return null;
	}
}
