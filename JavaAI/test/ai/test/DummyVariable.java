package ai.test;

import ai.domain.Variable;

/**
 * Variable implementation for tests only (when we create domain elements manually
 * without JDT)
 * 
 * @author kjk@mimuw.edu.pl
 *
 */
public class DummyVariable extends Variable{
	private final String name;
	private final int orderId;

	public DummyVariable(String name, int orderId) {
		super(null);
		this.name = name;
		this.orderId = orderId;
	}

	public String toString() {
		return "DummyVar(" + name + ")";
	}

	public boolean equals(Object other) {
		if (!(other instanceof DummyVariable))
			return false;
		DummyVariable otherVar = (DummyVariable) other;
		return name.equals(otherVar.name);
	}	

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	protected int variableOrderId() {
		return orderId;
	}
}
