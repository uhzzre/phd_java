package ai.test;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;

import ai.cfg.CFGVertice;
import ai.cfg.MethodControlFlowGraph;
import ai.domain.AbstractSemanticsIntf;
import ai.domain.DomainIntf;
import ai.domain.bool.Bool;
import ai.domain.bool.BoolsSemantics;
import ai.domain.boolintv.BoolIntvSemantics;
import ai.domain.boxes.IntegerBoxesSemantics;
import ai.domain.boxes.IntegerBoxes;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.generic.ProductDomain;
import ai.domain.interval.Interval;
import ai.domain.interval.IntervalsSemantics;
import ai.interpreter.Interpreter;
import ai.interpreter.StopCondition;
import ai.test.TestProject.ProjectFile;
import ai.test.comment.TestCommentData;

public abstract class TestsBase {
	protected final static Logger log = Logger.getLogger(TestsBase.class.getName());	

	protected TestProject project;
	
	protected Level getLogLevel(){
		return Level.WARN;
	}

	@Before
	public void setUp() throws Exception {
		BasicConfigurator.configure();
		Logger logger = Logger.getRootLogger();
		logger.setLevel(getLogLevel());		
		project = TestUtils.initialiseAndGetProject("CFG Tests");
	}

	protected abstract void performTest(MethodControlFlowGraph cfg);
	
	protected void testAllMethods(String packageName, String filename) {
		ProjectFile pf = project.openFile(packageName, filename);
		for(MethodControlFlowGraph cfg: pf.getAllMethodGraphs()){
			try {
				performTest(cfg);
			} catch (AssertionError e) {
				e.printStackTrace(System.err);
				throw new AssertionError("Method '" + cfg.getCodeFragment().getUniqueName() + "':'" + e.getMessage());
			} catch (RuntimeException e) {
				e.printStackTrace(System.err);
				throw new RuntimeException("Method '" + cfg.getCodeFragment().getUniqueName() + "':'" + e.toString());
			}
		}
	}
	

	protected static <D1 extends DomainIntf<D1>, D2 extends DomainIntf<D2>> Map<CFGVertice, D1> makeMap1(
			Map<CFGVertice, ProductDomain<D1, D2>> data, D1 BOTTOM) {
		Map<CFGVertice, D1> result = new HashMap<CFGVertice, D1>();
		for (Map.Entry<CFGVertice, ProductDomain<D1, D2>> entry : data.entrySet()) {
			result.put(entry.getKey(), entry.getValue().isBottom() ? BOTTOM : entry.getValue().getLeft());
		}
		return result;
	}

	protected static <D1 extends DomainIntf<D1>, D2 extends DomainIntf<D2>> Map<CFGVertice, D2> makeMap2(
			Map<CFGVertice, ProductDomain<D1, D2>> data, D2 BOTTOM) {
		Map<CFGVertice, D2> result = new HashMap<CFGVertice, D2>();
		for (Map.Entry<CFGVertice, ProductDomain<D1, D2>> entry : data.entrySet()) {
			result.put(entry.getKey(), entry.getValue().isBottom() ? BOTTOM : entry.getValue().getRight());
		}
		return result;
	}
	
	protected void testBooleansAndIntervals(MethodControlFlowGraph cfg) {
		TestCommentData commentData = TestCommentData.parseComments(cfg);

		BoolIntvSemantics as = new BoolIntvSemantics();
		NonRelationalDomain<Bool> initialBools = commentData.getInitialBools();
		NonRelationalDomain<Interval> initialIntervals = commentData.getInitialIntervals();
		ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>> initialValue = as.getInitialValue();
		if (initialBools!=null)
			initialValue = initialValue.setLeft(initialBools);
		if (initialIntervals!=null)
			initialValue = initialValue.setRight(initialIntervals);
		Map<CFGVertice, ProductDomain<NonRelationalDomain<Bool>, NonRelationalDomain<Interval>>> result = Interpreter
				.analyse(cfg, as, initialValue, new StopCondition.NoStopCondition());

		NonRelationalDomain<Bool> boolsBottom = NonRelationalDomain.getStaticBottom();
		NonRelationalDomain<Interval> intvBottom = NonRelationalDomain.getStaticBottom();
		commentData.matchBools(cfg, makeMap1(result, boolsBottom));
		commentData.matchIntervals(cfg, makeMap2(result, intvBottom));
	}
	
	protected void testBooleansAndBoxes(MethodControlFlowGraph cfg) {
		TestCommentData commentData = TestCommentData.parseComments(cfg);
		IntegerBoxesSemantics as = new IntegerBoxesSemantics("");

		NonRelationalDomain<Bool> initialBools = commentData.getInitialBools();
		IntegerBoxes initialBoxes = commentData.getInitialBoxes();

		ProductDomain<NonRelationalDomain<Bool>, IntegerBoxes> initialValue = as.getInitialValue();

		if (initialBools != null)
			initialValue = initialValue.setLeft(initialBools);
		if (initialBoxes != null)
			initialValue = initialValue.setRight(initialBoxes);

		Map<CFGVertice, ProductDomain<NonRelationalDomain<Bool>, IntegerBoxes>> result = Interpreter
				.analyse(cfg, as, initialValue, new StopCondition.NoStopCondition());

		NonRelationalDomain<Bool> boolsBottom = NonRelationalDomain.getStaticBottom();
		IntegerBoxes boxesBottom = IntegerBoxes.BOTTOM;
		commentData.matchBools(cfg, makeMap1(result, boolsBottom));
		commentData.matchBoxes(cfg, makeMap2(result, boxesBottom));
	}
	
	protected void testBooleans(MethodControlFlowGraph cfg) {
		TestCommentData commentData = TestCommentData.parseComments(cfg);
//		Pair<BiMap<CFGVertice, String>, Map<String, Variable>> matchResult = commentData.matchMethodGraph(cfg);
		
		AbstractSemanticsIntf<NonRelationalDomain<Bool>> as = new BoolsSemantics();
		NonRelationalDomain<Bool> initialValue = as.getInitialValue();
		NonRelationalDomain<Bool> initialBools = commentData.getInitialBools();		
		if (initialBools!=null)
			initialValue = initialBools;
		
		Map<CFGVertice, NonRelationalDomain<Bool>> result = Interpreter.analyse(cfg, as, initialValue, new StopCondition.NoStopCondition());
		
		commentData.matchBools(cfg, result);
	}

	protected void testIntervals(MethodControlFlowGraph cfg) {
		TestCommentData commentData = TestCommentData.parseComments(cfg);
//		Pair<BiMap<CFGVertice, String>, Map<String, Variable>> matchResult = commentData.matchMethodGraph(cfg);
		
		AbstractSemanticsIntf<NonRelationalDomain<Interval>> as = new IntervalsSemantics();
		NonRelationalDomain<Interval> initialValue = as.getInitialValue();
		NonRelationalDomain<Interval> initialBools = commentData.getInitialIntervals();		
		if (initialBools!=null)
			initialValue = initialBools;
		
		Map<CFGVertice, NonRelationalDomain<Interval>> result = Interpreter.analyse(cfg, as, initialValue, new StopCondition.NoStopCondition());
		
		commentData.matchIntervals(cfg, result);
	}
}
