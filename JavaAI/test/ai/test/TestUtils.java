package ai.test;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

public class TestUtils {
	public static TestProject initialiseAndGetProject(String name) throws Exception {
		// Initialize the test fixture for each test
		// that is run.
		waitForJobs();
		return new TestProject(name);
//		delay(3000);
//		waitForJobs();

		// testView = (FavoritesView)
		// PlatformUI
		// .getWorkbench()
		// .getActiveWorkbenchWindow()
		// .getActivePage()
		// .showView(VIEW_ID);
		//
		// // Delay for 3 seconds so that
		// // the Favorites view can be seen.
		// waitForJobs();
		// delay(3000);
		// // Add additional setup code here.
	}

	/**
	 * Wait until all background tasks are complete.
	 */
	private static void waitForJobs() {
		while (!Job.getJobManager().isIdle())
			delay(1000);
	}

	private static void delay(long waitTimeMillis) {
		Display display = Display.getCurrent();

		// If this is the UI thread,
		// then process input.

		if (display != null) {
			long endTimeMillis = System.currentTimeMillis() + waitTimeMillis;
			while (System.currentTimeMillis() < endTimeMillis) {
				if (!display.readAndDispatch())
					display.sleep();
			}
			display.update();
		}
		// Otherwise, perform a simple sleep.
		else {
			try {
				Thread.sleep(waitTimeMillis);
			} catch (InterruptedException e) {
				// Ignored.
			}
		}
	}

}
