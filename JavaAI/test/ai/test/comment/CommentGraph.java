package ai.test.comment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;

import ai.cfg.edges.CFGMultiTargetEdge;
import ai.cfg.edges.CFGMultiTargetEdge.CFGSingleEdge;
import ai.cfg.edges.ConstructorInvocationEdge;
import ai.cfg.edges.EmptyEdge;
import ai.cfg.edges.ExpressionStatementEdge;
import ai.cfg.edges.FinallyOrCatchEdge;
import ai.cfg.edges.SuperConstructorInvocationEdge;
import ai.cfg.edges.SwitchEdge;
import ai.domain.Variable;
import ai.domain.intervals.eval.EvaluationUtils;

import com.google.common.collect.BiMap;

public class CommentGraph {
	public static abstract class EdgeBase {
		final String source;
		final String target;

		public EdgeBase(String source, String target) {
			this.target = target;
			this.source = source;
		}

		protected String toString(String message) {
			return this.getClass().getName() + "[" + source + "->" + target + "](" + message + ")";
		}

		public abstract void match(CFGSingleEdge edge, BiMap<String, Variable> variables);

		protected void ensureCorrectType(CFGSingleEdge cfgEdge, Class<? extends CFGMultiTargetEdge> expectedType){
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			org.junit.Assert.assertTrue(message, expectedType.isInstance(cfgEdge.edge));
		}
	}

	static class EmptyCommentEdge extends EdgeBase {

		private final String[] variablesToRemove;

		public EmptyCommentEdge(String source, String target) {
			super(source, target);
			this.variablesToRemove = new String[0];
		}

		public EmptyCommentEdge(String source, String target, String[] variablesToRemove) {
			super(source, target);
			this.variablesToRemove = variablesToRemove;
		}
		
		public String toString() {
			return toString(variablesToRemove.toString());
		}

		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, ai.cfg.edges.EmptyEdge.class);
			ai.cfg.edges.EmptyEdge e = (EmptyEdge) cfgEdge.edge;
			org.junit.Assert.assertEquals("EDGE: "+this+":Different variables to remove: expected: " + variablesToRemove + ", was:"+e.variablesToRemove, 
					variablesToRemove.length, e.variablesToRemove.size());
			int i=0;
			for(SimpleName var: e.variablesToRemove){
				Variable result = EvaluationUtils.tryGetVariable(var);
				if (result == null)
					throw new RuntimeException("Variable??");
				String varId = variables.inverse().get(result);
				org.junit.Assert.assertEquals(variablesToRemove[i].trim(), varId);
				i++;
			}
		}
	};

	static class ConditionalEdge extends EdgeBase {
		final String condition;
		final boolean positive;

		ConditionalEdge(String source, String target, String condition, boolean positive) {
			super(source, target);
			this.condition = condition;
			this.positive = positive;
		}

		public String toString() {
			return toString(positive + "," + condition);
		}

		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, ai.cfg.edges.ConditionalEdge.class);
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			ai.cfg.edges.ConditionalEdge conditionalCFGEdge = (ai.cfg.edges.ConditionalEdge) cfgEdge.edge;
			org.junit.Assert.assertEquals(message, condition, conditionalCFGEdge.conditionToString());
			org.junit.Assert.assertEquals(message, positive, cfgEdge.edgeIdx == 0);
		}
	};

	static class ExpressionEdge extends EdgeBase {
		final String expression;

		ExpressionEdge(String source, String target, String expression) {
			super(source, target);
			this.expression = expression;
		}

		public String toString() {
			return toString(expression);
		}

		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, ExpressionStatementEdge.class);
			ExpressionStatementEdge expr = (ExpressionStatementEdge) cfgEdge.edge;
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			org.junit.Assert.assertEquals(message, expression, expr.expression.toString().trim());
		}
	}

	static class NewVariableEdge extends EdgeBase {
		final String variable;
		final String initialiser;
		private String id;

		NewVariableEdge(String source, String target, String variable, String id, String initialiser) {
			super(source, target);
			this.variable = variable;
			this.id = id;
			this.initialiser = initialiser.trim();
		}

		public String toString() {
			return toString(variable + "," + initialiser);
		}

		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, ai.cfg.edges.NewVariableEdge.class);
			String message = "Edge " + toString();
			ai.cfg.edges.NewVariableEdge var = (ai.cfg.edges.NewVariableEdge) cfgEdge.edge;
			org.junit.Assert.assertEquals(message, variable, var.name.toString());
			Expression initialiserExpr = var.initializerOrNull;
			org.junit.Assert.assertEquals(message, initialiser, initialiserExpr != null ? initialiserExpr.toString()
					: "");
			if (!variable.equals(var.name.toString()))
				throw new RuntimeException("Not a proper variable!!: '" + var + "'" + " should be "+variable);
			Variable result = EvaluationUtils.tryGetVariable(var.name);
			if (result == null)
				throw new RuntimeException("Not a proper variable!!: '" + var + "'");
			
			variables.put(id, result);
		}
	}

	static class InputEdge extends EdgeBase {

		public InputEdge(String target) {
			super(null, target);
		}

		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
		}
		
	}

	static class FinOrCatchEdge extends EdgeBase {
		private final String variable;
		private final String id;
		private final String excType;
		
		public FinOrCatchEdge(String source, String target) {
			super(source, target);
			this.variable = null;
			this.id = null;
			this.excType = null;
		}

		public FinOrCatchEdge(String source, String target, String variable, String id, String excType) {
			super(source, target);
			this.variable = variable;
			this.id = id;
			this.excType = excType;
		}
		
		public String toString() {
			return toString(excType + ", "+variable+":"+id);
		}
		
		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, FinallyOrCatchEdge.class);
			FinallyOrCatchEdge excEdge = (FinallyOrCatchEdge) cfgEdge.edge;
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			if (excType.length() == 0)
				org.junit.Assert.assertTrue(message, excEdge.exceptionOrNull == null);
			else {
				org.junit.Assert.assertTrue(message, excEdge.exceptionOrNull != null);
				org.junit.Assert.assertEquals(message, excEdge.exceptionOrNull.getType().toString(), excType);
				if  (variable==null) {
					org.junit.Assert.assertTrue(message, excEdge.exceptionOrNull.getName() == null);
				} else {
					org.junit.Assert.assertEquals(message, excEdge.exceptionOrNull.getName().toString(), variable);
					Variable result = EvaluationUtils.tryGetVariable(excEdge.exceptionOrNull.getName());
					if (result == null)
						throw new RuntimeException("Not a proper variable!!: '" + excEdge.exceptionOrNull + "'");
					variables.put(id == null ? variable : id, result);
				}
			}
		}		
	}
	
	private static String listToString(List<?> objects) {
		StringBuilder result = new StringBuilder();
		if (objects != null) {
			boolean first = true;
			for(Object node: objects){
				if (!first)
					result.append(", ");
				else 
					first = false;
				result.append(node.toString());
			}
		}
		return result.toString();
	}

	static class ThisEdge extends EdgeBase {
		private final String args;

		public ThisEdge(String source, String target, String args) {
			super(source, target);
			this.args = args;
		}
		
		public String toString() {
			return toString(args);
		}
		
		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, ConstructorInvocationEdge.class);
			ConstructorInvocationEdge thisEdge = (ConstructorInvocationEdge) cfgEdge.edge;
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			org.junit.Assert.assertEquals(message, args, listToString(thisEdge.stmt.arguments()));
		}		
	}

	static class SuperEdge extends EdgeBase {
		private final String args;

		public SuperEdge(String source, String target, String args) {
			super(source, target);
			this.args = args;
		}
		
		public String toString() {
			return toString(args);
		}
		
		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, SuperConstructorInvocationEdge.class);
			SuperConstructorInvocationEdge superEdge = (SuperConstructorInvocationEdge) cfgEdge.edge;
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			Expression optionalExpresison = superEdge.stmt.getExpression();
			String optExpressionStr = optionalExpresison == null ? "" : optionalExpresison.toString();
			org.junit.Assert.assertEquals(message, args, optExpressionStr +","+listToString(superEdge.stmt.arguments()));
		}		
	}

	static class SwEdge extends EdgeBase {
		private final String details;

		public SwEdge(String source, String target, String details) {
			super(source, target);
			this.details = details;
		}
		
		public String toString() {
			return toString(details);
		}
		
		@Override
		public void match(CFGSingleEdge cfgEdge, BiMap<String, Variable> variables) {
			ensureCorrectType(cfgEdge, SwitchEdge.class);
			SwitchEdge switchEdge = (SwitchEdge) cfgEdge.edge;
			String message = "Expected '" + this.toString() + "', have '" + cfgEdge.toString() + "' instead.";
			if (cfgEdge.edgeIdx == 0) // default
				org.junit.Assert.assertEquals(message, details, "default, "+switchEdge.expr);
			else
				org.junit.Assert.assertEquals(message, details, switchEdge.cases[cfgEdge.edgeIdx-1]+", "+switchEdge.expr);
		}		
	}
	
	private static final Pattern CONDITIONAL_EDGE_PATTERN = Pattern.compile("conditional\\((true|false),\\s*(.*)\\)$");
	private static final Pattern EXPRESSION_EDGE_PATTERN = Pattern.compile("expression\\((.*)\\)$");
	private static final Pattern NEW_VARIABLE_EDGE_PATTERN = Pattern.compile("var\\(([^,]*),(.*)\\)");
	private static final Pattern NEW_VARIABLE_WITH_ID_EDGE_PATTERN = Pattern.compile("var\\(\\s*(" + Constants.NODE +")\\s*:\\s*("+ Constants.NODE+")\\s*,(.*)\\)");
	private static final Pattern FINALLY_OR_CATCH_EDGE_PATTERN = Pattern.compile("finallyOrCatch\\(\\s*([^\\s]*)\\s*((([^:]*):)?(" + Constants.NODE + "))?\\s*\\)$");
	private static final Pattern THIS_EDGE_PATTERN = Pattern.compile("this\\((.*)\\)$");
	private static final Pattern SUPER_EDGE_PATTERN = Pattern.compile("super\\((.*)\\)$");
	private static final Pattern SWITCH_EDGE_PATTERN = Pattern.compile("switch\\((.*)\\)$");
	private static final Pattern EMPTY_EDGE_PATTERN = Pattern.compile("empty\\((.*)\\)$");

	// ------------------------------------ CLASS --------------------------------------
	
	private void addVariable(String variableId) {
//		if (variableNames.contains(variableId))
//			throw new CommentParserException("Duplicate variable id: '%s'", variableId);
		variableNames.add(variableId);
	}
	
	private EdgeBase parseEdgeDetails(String source, String target, String details) {
		// empty
		if (details.length() == 0) {
			return new EmptyCommentEdge(source, target);
		}
		
		Matcher matcher = EMPTY_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			String[] variablesToRemove = matcher.group(1).split(",");
			return new EmptyCommentEdge(source, target, variablesToRemove);
		}
		
		// conditional
		matcher = CONDITIONAL_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			boolean positive = Boolean.parseBoolean(matcher.group(1));
			String condition = matcher.group(2);
			return new ConditionalEdge(source, target, condition, positive);
		}
		// expression
		matcher = EXPRESSION_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			String expression = matcher.group(1);
			return new ExpressionEdge(source, target, expression);
		}
		// new variable
		matcher = NEW_VARIABLE_WITH_ID_EDGE_PATTERN.matcher(details);
		if (matcher.matches()){
			addVariable(matcher.group(2));
			return new NewVariableEdge(source, target, matcher.group(1), matcher.group(2), matcher.group(3));
		}
		matcher = NEW_VARIABLE_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			addVariable(matcher.group(1));
			return new NewVariableEdge(source, target, matcher.group(1), matcher.group(1), matcher.group(2));
		}
		
		matcher = FINALLY_OR_CATCH_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			String exc = matcher.group(1).trim();
			String var = matcher.group(5);
			String id = matcher.group(4);
			return new FinOrCatchEdge(source, target, var, id, exc);
		}

		matcher = THIS_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			return new ThisEdge(source, target, matcher.group(1).trim());
		}

		matcher = SUPER_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			return new SuperEdge(source, target, matcher.group(1).trim());
		}
		
		matcher = SWITCH_EDGE_PATTERN.matcher(details);
		if (matcher.matches()) {
			return new SwEdge(source, target, matcher.group(1).trim());
		}
		
		throw new RuntimeException("Unrecognised edge details: '" + details + "'");
	}

	private Map<String, ArrayList<EdgeBase>> graph = new HashMap<String, ArrayList<EdgeBase>>();  //vertice id -> outputs edges
	private Set<String> variableNames = new HashSet<String>(); 

	public void newNode(String nodeId) {
		if (graph.containsKey(nodeId))
			throw new CommentParserException("Duplicate node: '%s'", nodeId);
		graph.put(nodeId, new ArrayList<EdgeBase>());
	}
	
	public void newEdge(String sourceId, String targetId, String details) {
		if (!graph.containsKey(sourceId))
			throw new CommentParserException("Invalid source name: '%s'", sourceId);
		if (!graph.containsKey(targetId))
			throw new CommentParserException("Invalid target name: '%s'", targetId);
		ArrayList<EdgeBase> targets = graph.get(sourceId);
		targets.add(parseEdgeDetails(sourceId, targetId, details));
	}

	public boolean containsVertice(String verticeId) {
		return graph.containsKey(verticeId);
	}
	
	public Set<String> verticeNames(){
		return graph.keySet();
	}

	public Set<String> variableNames(){
		return variableNames;
	}
	

	public EdgeBase getInput() {
		return new InputEdge("IN");
	}

	public ArrayList<EdgeBase> getOutputs(String commentVertice) {
		ArrayList<EdgeBase> result = graph.get(commentVertice);
		if (result == null)
			return new ArrayList<CommentGraph.EdgeBase>();
		return result;
	}
}
