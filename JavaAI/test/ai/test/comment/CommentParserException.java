package ai.test.comment;

public class CommentParserException extends RuntimeException {

	public CommentParserException(String format, Object... args) {
		super(String.format(format, args));
	}

	private static final long serialVersionUID = -874413954560621440L;

}
