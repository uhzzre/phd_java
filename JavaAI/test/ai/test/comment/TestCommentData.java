package ai.test.comment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import ai.cfg.CFGVertice;
import ai.cfg.ControlFlowGraphVisitor;
import ai.cfg.InterestingCodeFragment;
import ai.cfg.MethodControlFlowGraph;
import ai.cfg.Utils;
import ai.cfg.edges.CFGMultiTargetEdge.CFGSingleEdge;
import ai.domain.Variable;
import ai.domain.bool.Bool;
import ai.domain.boxes.IntegerBoxes;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.interval.Interval;
import ai.domain.intervals.eval.EvaluationUtils;
import ai.test.comment.CommentGraph.EdgeBase;
import ai.test.comment.domains.CommentBooleans;
import ai.test.comment.domains.CommentBoxes;
import ai.test.comment.domains.CommentDomainFactory;
import ai.test.comment.domains.CommentDomainIntf;
import ai.test.comment.domains.CommentIntv;
import ai.test.comment.domains.MatchResult;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class TestCommentData {
	class GraphComparator {

		private final MethodControlFlowGraph cfg;
		private final TestCommentData commentCfg;
		private final BiMap<CFGVertice, String> verticesMap = HashBiMap.create();
		private final BiMap<String, Variable> variablesMap = HashBiMap.create();

		public GraphComparator(MethodControlFlowGraph graph, TestCommentData cfgCommentGraph) {
			this.cfg = graph;
			this.commentCfg = cfgCommentGraph;
		}
		
		private MatchResult compare(EdgeBase inputCommentEdge, CFGSingleEdge inputCfgEdge) {
			String commentVertice = inputCommentEdge.target;
			CFGVertice vertice = inputCfgEdge.target;
			// CFGVertice existing = verticesMap.get(commentVertice);
			if (verticesMap.containsKey(vertice)) {
				// already visited, verify if the same mapping
				org.junit.Assert.assertEquals("Edge '" + inputCommentEdge.toString() + "'/'" + inputCfgEdge.toString()
						+ "'", commentVertice, verticesMap.get(vertice));
			} else {
				org.junit.Assert.assertFalse("Comment target '" + commentVertice + "' alreadyVisited (edge "+inputCommentEdge+") but vertice " +
						vertice + " not", verticesMap.containsValue(commentVertice));
				verticesMap.put(vertice, commentVertice);
				ArrayList<EdgeBase> commentEdges = commentCfg.graph.getOutputs(commentVertice);
				List<CFGSingleEdge> cfgEdges = vertice.getOutgoingEdges();
				try {
					org.junit.Assert.assertEquals("Vertice '" + commentVertice
							+ "' has different number of output edges: expected '" + commentEdges + "', received: '"
							+ cfgEdges + "'.", commentEdges.size(), cfgEdges.size());
				} catch (NullPointerException exc) {
					throw exc;
				}
				for (int i = 0; i < commentEdges.size(); i++) {
					EdgeBase commentEdge = commentEdges.get(i);
					CFGSingleEdge cfgEdge = cfgEdges.get(i);
					commentEdge.match(cfgEdge, variablesMap);
//					if (var != null)
//						variablesMap.put(var.left, var.right);
					compare(commentEdge, cfgEdge);
				}
			}
			return new MatchResult(verticesMap, variablesMap);
		}

		public MatchResult compare() {
			//map argument variables
			for(Object param: cfg.getCodeFragment().parameters()){
				SingleVariableDeclaration var = (SingleVariableDeclaration) param;
				SimpleName name = var.getName();
				Variable result = EvaluationUtils.tryGetVariable(name);
				if (result == null)
					throw new RuntimeException("Not a proper variable!!: '" + var + "'");
				variablesMap.put(name.toString(), result);
			}
			MatchResult result = compare(commentCfg.graph.getInput(), cfg.getInput().singleEdges[0]);
			org.junit.Assert.assertTrue("Invalid number of vertices", result.getVerticesCount() <= cfg.size());
			return result;
		}
	}

	static final int STATE_WAIT_FOR_NODES = 0;
	static final int STATE_PARSING_NODES = 1;
	static final int STATE_PARSING_EDGES = 2;
	static final int STATE_PARSING_LOOPS = 3;
	static final int STATE_PARSING_DOMAIN = 4;
	static final int STATE_PARSING_INITIAL = 5;

	// groups
	static final String VERTICES_PREFIX = "vertices:";
	static final String EDGES_PREFIX = "edges:";
	static final String LOOPS_PREFIX = "loops:";

	static final Pattern edgeFinderRegex = Pattern.compile("\\s*(" + Constants.NODE + ")\\s*->\\s*(" + Constants.NODE
			+ ")\\s*:(.*)");
	static final Pattern loopLine = Pattern.compile("\\s*(" + Constants.NODE + ")\\s*:(.*)$");

	static final Pattern DOMAIN_NAME_REGEX = Pattern.compile("domain:\\s*(" + Constants.NODE
			+ ")\\s*(\\((.*)\\))?\\s*$");

	private int state;

	private final CommentGraph graph;
	private Map<String, Set<String>> loops = new HashMap<String, Set<String>>();
	private Map<String, CommentDomainIntf<?>> domains = new HashMap<String, CommentDomainIntf<?>>();
	private CommentDomainIntf<?> currentDomain;
	private final MethodControlFlowGraph cfg;
	private MatchResult matchResult;
	private boolean matched;

	private TestCommentData(MethodControlFlowGraph cfg) {
		this.state = STATE_WAIT_FOR_NODES;
		this.graph = new CommentGraph();
		this.cfg = cfg;
		this.matched = false;
	}

	private void build() {
		InterestingCodeFragment md = cfg.getCodeFragment();
		Javadoc doc = md.getJavadoc();
		if (doc == null)
			throw new CommentParserException("No javadoc defined for method: '%s'", md.getUniqueName());
		List<?> a = doc.tags();
		assert a.size() == 1;
		String text = a.get(0).toString();
		for (String line : text.split("\n"))
			try {
				parseLine(line);
			} catch (CommentParserException e) {
				System.err.println("Error parsing line '"+line+"'");
				e.printStackTrace(System.err);
				throw e;
			}
		// parse text
		if (!matched)
			matchGraph();
	}

	private List<String> parseNames(String nodes) {
		List<String> result = new LinkedList<String>();
		for (String node : nodes.split("\\s")) {
			if (!node.matches(Constants.NODE))
				throw new CommentParserException("Invalid node name: '%s'", node);
			result.add(node);
		}
		return result;
	}

	private void parseNodes(String nodes) {
		for (String node : parseNames(nodes))
			graph.newNode(node);
	}

	private void parseEdge(String line) {
		Matcher matcher = edgeFinderRegex.matcher(line);
		if (!matcher.matches())
			throw new CommentParserException("Invalid edge line '%s'", line);
		String source = matcher.group(1);
		String target = matcher.group(2);
		String edgeDetails = matcher.group(3).trim();
		graph.newEdge(source, target, edgeDetails);
	}

	private void parseLoop(String line) {
		Matcher matcher = loopLine.matcher(line);
		if (!matcher.matches())
			throw new CommentParserException("Invalid loop: '%s'", line);
		String loopVertice = matcher.group(1);
		if (!graph.containsVertice(loopVertice))
			throw new CommentParserException("Invalid vertice: '%s'", loopVertice);
		if (loops.containsKey(loopVertice))
			throw new CommentParserException("Already defined loop: '%s'", loopVertice);
		List<String> sources = parseNames(matcher.group(2).trim());
		Set<String> inputs = new HashSet<String>();
		loops.put(loopVertice, inputs);
		for (String source : sources) {
			if (!graph.containsVertice(source))
				throw new CommentParserException("Invalid vertice: '%s'", source);
			if (inputs.contains(source))
				throw new CommentParserException("Duplicate loop edge: '%s'", source);
			inputs.add(source);
		}
	}

	private boolean parseDomainStart(String line) {
		Matcher matcher = DOMAIN_NAME_REGEX.matcher(line);
		if (matcher.matches()) {
			String domainName = matcher.group(1);
			if (domains.containsKey(domainName))
				throw new CommentParserException("Duplicate domain description '%s'", domainName);
			CommentDomainIntf<?> domain = CommentDomainFactory.getDomainParser(domainName, matchResult); 
			domains.put(domainName, domain);
			currentDomain = domain;
			// initial value?
			String initialValueStr = matcher.group(3);
			if (initialValueStr != null)
				domain.parseInitialValue(initialValueStr);
			return true;
		} else {
			return false;
		}
	}

	private void matchGraph() {
		matched = true;
		GraphComparator comparator = new GraphComparator(cfg, this);
		try {
			matchResult = comparator.compare();
		} catch (Error e) {
			printGraph(cfg, e, null);
			throw e;
		}
	}
	
	private void parseLine(String line) {
		// remove * from the beginning
		int index = line.indexOf('*');
		if (index >= 0)
			line = line.substring(index + 1).trim();

		// state machine
		if (line.length() == 0)
			return;
		switch (state) {
		case STATE_WAIT_FOR_NODES:
			if (line.startsWith(VERTICES_PREFIX)) {
				state = STATE_PARSING_NODES;
				parseNodes(line.substring(VERTICES_PREFIX.length() + 1));
			} else
				throw new CommentParserException("Invalid vertices definition line: '%s'", line);
			break;
		case STATE_PARSING_NODES:
			if (line.equals(EDGES_PREFIX)) {
				state = STATE_PARSING_EDGES;
			} else {
				parseNodes(line);
			}
			break;
		case STATE_PARSING_EDGES:
			if (line.equals(LOOPS_PREFIX)) {
				matchGraph();
				state = STATE_PARSING_LOOPS;
			} else {
				parseEdge(line);
			}
			break;
		case STATE_PARSING_LOOPS:
			if (parseDomainStart(line)) {
				state = STATE_PARSING_DOMAIN;
				Map<CFGVertice, Collection<CFGSingleEdge>> loopVertices = Utils.findLoopVertices(cfg);
				try {
					matchResult.compareLoops(loopVertices, loops);
				} catch (Error e) {
					printGraph(cfg, e, loopVertices);
					throw e;
				}
			} else
				parseLoop(line);
			break;
		case STATE_PARSING_DOMAIN:
			if (parseDomainStart(line))
				break;
			currentDomain.parseLine(line);
		default:
			break;
		}
	}
	
	private static void printGraph(MethodControlFlowGraph cfg, Error e, Object message) {
		System.out.println("-------------- CFG ---------------");
		if (message != null)
			System.out.println(message);
		System.out.println("ERROR:" + e);
		Utils.printCFG(cfg);
		System.out.println("----------------------------------");
	}

	public static TestCommentData parseComments(MethodControlFlowGraph cfg) {
		TestCommentData graph = new TestCommentData(cfg);
		graph.build();
		return graph;
	}

	public void matchIntervals(MethodControlFlowGraph graph, final Map<CFGVertice, NonRelationalDomain<Interval>> result) {
		if (!domains.containsKey("intervals"))
			throw new CommentParserException("No intervals domain to verify");
		final CommentIntv domain = (CommentIntv) domains.get("intervals");
		Utils.walkGraph(graph, new ControlFlowGraphVisitor() {
			@Override
			public boolean visitVertice(CFGVertice vertice) {
				// verify values
//				String commentVertice = verticeMap.get(vertice);
				NonRelationalDomain<Interval> analysisResult = result.get(vertice);
				domain.compareResult(vertice, analysisResult);
				return true;
			}

			@Override
			public boolean visitEdge(CFGSingleEdge edge) {
				return true;
			}
		});
	}

	public void matchBools(MethodControlFlowGraph graph, final Map<CFGVertice, NonRelationalDomain<Bool>> result) {
		if (!domains.containsKey("booleans"))
			throw new CommentParserException("No booleans domain to verify");
		final CommentBooleans domain = (CommentBooleans) domains.get("booleans");
		Utils.walkGraph(graph, new ControlFlowGraphVisitor() {
			@Override
			public boolean visitVertice(CFGVertice vertice) {
				// verify values
				NonRelationalDomain<Bool> analysisResult = result.get(vertice);
				domain.compareResult(vertice, analysisResult);
				return true;
			}

			@Override
			public boolean visitEdge(CFGSingleEdge edge) {
				return true;
			}
		});
	}

	public void matchBoxes(MethodControlFlowGraph graph, final Map<CFGVertice, IntegerBoxes> result) {
		if (!domains.containsKey("boxes"))
			throw new CommentParserException("No boxes domain to verify");
		
		final CommentBoxes domain = (CommentBoxes) domains.get("boxes");
		Utils.walkGraph(graph, new ControlFlowGraphVisitor() {
			@Override
			public boolean visitVertice(CFGVertice vertice) {
				// verify values
				IntegerBoxes analysisResult = result.get(vertice);
				domain.compareResult(vertice, analysisResult);
				return true;
			}

			@Override
			public boolean visitEdge(CFGSingleEdge edge) {
				return true;
			}
		});
	}
	
	public NonRelationalDomain<Interval> getInitialIntervals() {
		CommentIntv x = (CommentIntv) domains.get("intervals");
		if (x==null)
			return NonRelationalDomain.getInitialValue();
		return x.getInitialValue();
	}

	public NonRelationalDomain<Bool> getInitialBools() {
		CommentBooleans x = (CommentBooleans) domains.get("booleans");
		if (x==null)
			return NonRelationalDomain.getInitialValue();
		return x.getInitialValue();
	}

	public IntegerBoxes getInitialBoxes() {
		CommentBoxes x = (CommentBoxes) domains.get("boxes");
		if (x==null)
			return IntegerBoxes.getInitialValue();
		return x.getInitialValue();
	}
}
