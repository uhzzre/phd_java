package ai.test.comment.domains;

import ai.domain.DomainIntf;

public interface CommentDomainIntf<DI extends DomainIntf<DI>> {

	public void parseLine(String line);
	
	public void parseInitialValue(String value);
	
	public DI getInitialValue();
}
