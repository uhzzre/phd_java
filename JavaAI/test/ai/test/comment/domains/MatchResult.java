package ai.test.comment.domains;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import ai.cfg.CFGVertice;
import ai.cfg.edges.CFGMultiTargetEdge.CFGSingleEdge;
import ai.domain.Variable;

import com.google.common.collect.BiMap;

public class MatchResult {
	private final BiMap<CFGVertice, String> verticesMap;
	private final BiMap<String, Variable> variablesMap;
	
	public MatchResult(BiMap<CFGVertice, String> verticesMap, BiMap<String, Variable> variablesMap) {
		this.verticesMap = verticesMap;
		this.variablesMap = variablesMap;
	}
	
	public String getCommentVertice(CFGVertice vertice) {
		return verticesMap.get(vertice);
	}
	
	public int getVerticesCount(){
		return verticesMap.size();
	}

	public void compareLoops(Map<CFGVertice, Collection<CFGSingleEdge>> loopVertices, Map<String, Set<String>> loops) {
		BiMap<String, CFGVertice> interted = verticesMap.inverse();
		org.junit.Assert.assertEquals("Different number of loops", loops.size(), loopVertices.size());
		for (String commentLoopVertice : loops.keySet()) {
			CFGVertice cfgLoopVertice = interted.get(commentLoopVertice);
			org.junit.Assert.assertTrue("Not loop vertice: '" + commentLoopVertice + "'",
					loopVertices.containsKey(cfgLoopVertice));
			Set<String> commentLoopInputs = loops.get(commentLoopVertice);
			Collection<CFGSingleEdge> loopEdges = loopVertices.get(cfgLoopVertice);
			org.junit.Assert.assertEquals("Different number of input edges.", commentLoopInputs.size(),
					loopEdges.size());
			for (CFGSingleEdge edge : loopEdges) {
				String source = verticesMap.get(edge.source);
				org.junit.Assert.assertTrue("Not loop edge: '" + source + "'", commentLoopInputs.contains(source));
				commentLoopInputs.remove(source);
			}
		}
	}

	public boolean containsCommentVertice(String name) {
		return verticesMap.containsValue(name);
	}
	
	public Variable getVariable(String name) {
		return variablesMap.get(name);		
	}
	
	public String getVariable(Variable var) {
		return variablesMap.inverse().get(var);
	}
}

