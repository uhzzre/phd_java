package ai.test.comment.domains;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.common.Pair;
import ai.domain.interval.Interval;
import ai.domain.interval.IntervalValue;
import ai.test.comment.CommentParserException;
import ai.test.comment.Constants;

public class CommentIntv extends NonRelationalCommentDomain<Interval> {

	public CommentIntv(MatchResult result) {
		super(result);
	}
	private static final Pattern NAME_VALUE_PATTERN_PREFIX = Pattern.compile("\\s*(" + Constants.NODE + "):\\s*("
			+ DomainConstants.BOT + "|\\[[^,]*\\]),?(.*)$");


	@Override
	protected Pair<Pair<String, Interval>, String> getNextValue(String description) {
		Matcher matcher = NAME_VALUE_PATTERN_PREFIX.matcher(description);
		if (!matcher.matches())
			throw new CommentParserException("Invalid domain value '%s'", description);
		String variableName = matcher.group(1);
		String valueString = matcher.group(2);
		IntervalValue value = Utils.parseInterval(valueString);
		String descriptionSuffix = matcher.group(3).trim();
		return Pair.create(Pair.create(variableName, new Interval(value)), descriptionSuffix);
	}

//	public void compareResult(String commentVertice, NonRelationalDomain<Interval> analysisResult,
//			Map<String, Variable> variablesMap) {
//		Map<String, IntervalValue> variableMapping = mapping.get(commentVertice);
//		if (variableMapping == null) {
//			org.junit.Assert.assertEquals(NonRelationalDomain.getStaticBottom(), analysisResult);
//		} else if (analysisResult.isBottom()) {
//			throw new AssertionError(String.format("Analysis result is bottom while value for '%s' is not.",
//					commentVertice));
//		} else {
//			// verify variable set
//			for (Map.Entry<String, IntervalValue> entry : variableMapping.entrySet()) {
//				String varId = entry.getKey();
//				Variable var = variablesMap.get(varId);
//				IntervalValue value = entry.getValue();
//
//				org.junit.Assert.assertTrue(commentVertice + ": Missing integer variable '" + varId + "'/'" + var + "'",
//						analysisResult.containsValue(var));
//				Interval intv = analysisResult.getValueFor(var);
//				String message = String.format("Node: '%s', var: '%s'", commentVertice, varId);
//				if (value == null) {
//					org.junit.Assert.assertEquals(message, Interval.BOTTOM, intv);
//				} else {
//					org.junit.Assert.assertEquals(message, value, intv.getValue());
//				}
//			}
//			org.junit.Assert.assertEquals(variableMapping.size(), analysisResult.variablesCount());
//		}
//	}
}
