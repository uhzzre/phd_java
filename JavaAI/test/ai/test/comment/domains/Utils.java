package ai.test.comment.domains;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ai.domain.bool.Bool;
import ai.domain.interval.IntervalValue;
import ai.domain.interval.InvalidIntervalValueException;

public class Utils {
	public static final String INFTY = "∞";//\u221E";
	public static final String INFTY_NEG = "-"+INFTY;
	public static final String VALUE = "\\[([^:]+):([^:]+)\\]";
	
	private static Pattern NUMBER = Pattern.compile("^(\\-?[1-9]\\d*|0)(\\.\\d+)?$");
	private static Pattern VALUE_PATTERN = Pattern.compile("\\[([^:]+):([^:]+)\\]");
	
	private static InvalidIntervalValueException invalidFragment(boolean beginOrEnd, String value) {
		String messageTemplate = beginOrEnd ? "Invalid begin value '%s'" : "Invalid end value '%s'";
		return new InvalidIntervalValueException(String.format(messageTemplate, value));
	}
	
	private static InvalidIntervalValueException invalidInterval(String interval) {
		return new InvalidIntervalValueException(String.format("Invalid interval '%s'", interval));
	}
	
	private static Double parseNumber(String numberStr, boolean beginOrEnd) {
		Matcher matcher = NUMBER.matcher(numberStr);
		if (!matcher.matches())
			throw invalidFragment(beginOrEnd, numberStr);
		return Double.parseDouble(numberStr);
	}

	public static IntervalValue parseInterval(String interval) {
		if (interval.matches("\\s*"+DomainConstants.BOT+"\\s*"))
			return null;
		Matcher matcher = VALUE_PATTERN.matcher(interval);
		if (!matcher.matches())
			throw invalidInterval(interval);
		String beginStr = matcher.group(1).trim();		
		Double begin = beginStr.equals(INFTY_NEG) ? Double.NEGATIVE_INFINITY : parseNumber(beginStr, true);
		
		String endStr = matcher.group(2).trim();
		Double end = endStr.equals(INFTY) ? Double.POSITIVE_INFINITY : parseNumber(endStr, false);
		return new IntervalValue(begin, end);
	}
	
	public static Bool parseBool(String value) {
		value = value.trim();
		if (value.equals(DomainConstants.TOP))
			return Bool.TOP;
		if (value.equals(DomainConstants.BOT))
			return Bool.BOTTOM;
		if ("TRUE".equals(value.toUpperCase()))
			return Bool.TRUE;
		if ("FALSE".equals(value.toUpperCase()))
			return Bool.FALSE;
		return null;
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	private void verifyException(String toParse, InvalidIntervalValueException expected) {
		try {
			parseInterval(toParse);
			org.junit.Assert.fail("No exception");
		} catch (InvalidIntervalValueException exception){
			org.junit.Assert.assertEquals(expected.getMessage(), exception.getMessage());
		}
	}

	@Test
	public void testParsingInterval(){
		//bottom
		org.junit.Assert.assertEquals(parseInterval("\u22A5"), null);
		org.junit.Assert.assertEquals(parseInterval("\u22A5  "), null);
		org.junit.Assert.assertEquals(parseInterval("   \u22A5"), null);
		org.junit.Assert.assertEquals(parseInterval("   \u22A5  "), null);
		
		//correct intervals
		org.junit.Assert.assertEquals(new IntervalValue(1, 1), parseInterval("[1:1]"));
		org.junit.Assert.assertEquals(new IntervalValue(1, 1), parseInterval("[ 1 : 1 ]"));
		org.junit.Assert.assertEquals(new IntervalValue(1.2, 2.8), parseInterval("[1.2:2.8]"));
		org.junit.Assert.assertEquals(new IntervalValue(1.2, 2.8), parseInterval("[ 1.2: 2.8]"));
		org.junit.Assert.assertEquals(new IntervalValue(1.2, 2.8), parseInterval("[ 1.2 : 2.8 ]"));
		org.junit.Assert.assertEquals(new IntervalValue(Double.NEGATIVE_INFINITY, 2.8), parseInterval("[-∞:2.8]"));
		org.junit.Assert.assertEquals(new IntervalValue(3, Double.POSITIVE_INFINITY), parseInterval("[3:∞]"));
		org.junit.Assert.assertEquals(new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY), parseInterval("[-∞:∞]"));
		
		verifyException("[1.2:1]", InvalidIntervalValueException.invalidIntervalValue(1.2, 1.0));
		verifyException("[dsad:1]", invalidFragment(true, "dsad"));
		verifyException("[1:kupa]", invalidFragment(false, "kupa"));		
		verifyException("[1:-∞]", invalidFragment(false, "-∞"));
		verifyException("[∞:3]", invalidFragment(true, "∞"));
		verifyException("[∞:-∞]", invalidFragment(true, "∞"));
	}

}
