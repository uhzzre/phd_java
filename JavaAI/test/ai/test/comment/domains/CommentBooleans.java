package ai.test.comment.domains;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.common.Pair;
import ai.domain.bool.Bool;
import ai.test.comment.CommentParserException;
import ai.test.comment.Constants;

public class CommentBooleans extends  NonRelationalCommentDomain<Bool> {

	public CommentBooleans(MatchResult result) {
		super(result);
	}

	private static final Pattern NAME_VALUE_PATTERN_PREFIX = Pattern.compile("\\s*(" + Constants.NODE + "):\\s*("
			+ DomainConstants.BOT + "|true|false|" + DomainConstants.TOP + "),?(.*)$");

	@Override
	protected Pair<Pair<String, Bool>, String> getNextValue(String description) {
		Matcher matcher = NAME_VALUE_PATTERN_PREFIX.matcher(description);
		if (!matcher.matches())
			throw new CommentParserException("Invalid domain value '%s'", description);
		String variableName = matcher.group(1);
		String valueString = matcher.group(2);
		Bool value = Utils.parseBool(valueString);
		if (value == null)
			throw new CommentParserException("Invalid boolean value '%s'", valueString);
		description = matcher.group(3).trim();
		return Pair.create(Pair.create(variableName, value), description);
	}
}
