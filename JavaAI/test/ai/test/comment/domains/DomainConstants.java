package ai.test.comment.domains;

import java.util.regex.Pattern;

import ai.test.comment.Constants;

public class DomainConstants {
	public static final Pattern NAME_VALUE_PATTERN = Pattern.compile("\\s*(" + Constants.NODE + "):(.*)$");
	
	
	public static final String BOT = "⊥"; //"\u22A5";
	public static final String TOP = "⊤"; //"\u22A4";
}
