package ai.test.comment.domains;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import ai.cfg.CFGVertice;
import ai.domain.DomainIntf;
import ai.test.comment.CommentParserException;

public abstract class CommentDomainBase<DI extends DomainIntf<DI>, T> implements
		CommentDomainIntf<DI> {
	protected final MatchResult match;
	protected final Map<String, T> mapping;
	private T initialValue;
//	private DI initialValue;

	public CommentDomainBase(MatchResult result) {
		this.match = result;
		this.mapping = new HashMap<String, T>();
	}

	@Override
	public final void parseLine(String line) {
		Matcher matcher = DomainConstants.NAME_VALUE_PATTERN.matcher(line);
		if (!matcher.matches())
			throw new CommentParserException("Invalid intervals line '%s'", line);
		String cp = matcher.group(1);
		
		if (!match.containsCommentVertice(cp))
			throw new CommentParserException("Invalid control point name '%s'", cp);
		if (mapping.containsKey(cp))
			throw new CommentParserException("Duplicate control point description '%s'", cp);
		String itemDescription = matcher.group(2).trim();
		mapping.put(cp, parseItemDescription(itemDescription));
	}
	
	public abstract T parseItemDescription(String itemDescription);
	public abstract DI createInitialValue(T parsedDescription);

	@Override
	public final void parseInitialValue(String value) {
		if (initialValue != null)
			throw new CommentParserException("Invalid usage, duplicate initial value");
		initialValue = parseItemDescription(value);
	}

	public abstract void compareResult(CFGVertice vertice, DI analysisResult);

	public DI getInitialValue(){
		return initialValue == null ? null : createInitialValue(initialValue);
	}
}
