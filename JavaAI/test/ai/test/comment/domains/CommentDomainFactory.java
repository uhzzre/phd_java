package ai.test.comment.domains;



public class CommentDomainFactory {
	public static CommentDomainIntf<?> getDomainParser(String name, MatchResult result) {
		if ("intervals".equals(name))
			return new CommentIntv(result);
		if ("booleans".equals(name))
			return new CommentBooleans(result);
		if ("boxes".equals(name))
			return new CommentBoxes(result);
		return null;
	}
}
