package ai.test.comment.domains;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.cfg.CFGVertice;
import ai.common.Pair;
import ai.domain.DomainIntf;
import ai.domain.Variable;
import ai.domain.generic.NonRelationalDomain;
import ai.test.comment.CommentParserException;

public abstract class NonRelationalCommentDomain<DI extends DomainIntf<DI>> extends
		CommentDomainBase<NonRelationalDomain<DI>, Map<Variable, DI>> {

	public NonRelationalCommentDomain(MatchResult result) {
		super(result);
	}

	private static final Pattern DOMAIN_ITEM_PATTERN = Pattern.compile("\\{(.*)\\}$");

	public Map<Variable, DI> parseItemDescription(String itemDescription) {
		if (DomainConstants.BOT.equals(itemDescription))
			return null;
		Matcher matcher = DOMAIN_ITEM_PATTERN.matcher(itemDescription);
		if (!matcher.matches())
			throw new CommentParserException("Invalid domain value '%s'", itemDescription);
		itemDescription = matcher.group(1).trim();
		Map<Variable, DI> result = new HashMap<Variable, DI>();
		// Map<Variable, DI> result = new HashMap<Variable, DI>();
		while (itemDescription.length() > 0) {
			Pair<Pair<String, DI>, String> res = getNextValue(itemDescription);
			String variableName = res.left.left;
			Variable var = match.getVariable(variableName);
			if (var == null)
				throw new RuntimeException("Unknown variable '" + variableName + "'");
			if (result.containsKey(var))
				throw new CommentParserException("Duplicate variable '%s'", variableName);
			DI value = res.left.right;
			result.put(var, value);
			String newitemDescription = res.right;
			if (newitemDescription.length() > 0 && itemDescription.indexOf(newitemDescription) <= 0)
				throw new CommentParserException("An error!!");
			itemDescription = newitemDescription;
		}
		return result;
	}

	protected abstract Pair<Pair<String, DI>, String> getNextValue(String description);

	public final void compareResult(CFGVertice vertice, NonRelationalDomain<DI> analysisResult) {
		String commentVertice = match.getCommentVertice(vertice);
		Map<Variable, DI> variableMapping = mapping.get(commentVertice);
		if (variableMapping == null) {
			org.junit.Assert.assertEquals("Vertice: " + commentVertice, NonRelationalDomain.getStaticBottom(), analysisResult);
		} else if (analysisResult.isBottom()) {
			throw new AssertionError(String.format("Analysis result is bottom while value for '%s' is not.",
					commentVertice));
		} else {
			compare(variableMapping, analysisResult, commentVertice);
		}
	}
	
	public void compare(Map<Variable, DI> expected, NonRelationalDomain<DI> actual,
			String commentVertice) {
		// verify variable set
		for (Map.Entry<Variable, DI> entry : expected.entrySet()) {
			Variable var = entry.getKey();
			DI value = entry.getValue();
			org.junit.Assert.assertTrue(getMissingVariableMessage(commentVertice, var), actual.containsValue(var));
			DI analysisValue = actual.getValueFor(var);
			String message = String.format("Node: '%s', var: '%s'", commentVertice, var);
			org.junit.Assert.assertEquals(message, value, analysisValue);
		}
		org.junit.Assert.assertEquals("Vertice: '" + commentVertice + "':"+expected + ", vs:"+actual, expected.size(), actual.variablesCount());
	}

	private String getMissingVariableMessage(String commentVertice, Variable var) {
		return String.format("Vertice: '%s': Analysis result is missing variable '%s:%s'", commentVertice,
				match.getVariable(var), var);
	}


	@Override
	public NonRelationalDomain<DI> createInitialValue(Map<Variable, DI> parsedDescription) {
		NonRelationalDomain<DI> result = NonRelationalDomain.getInitialValue();
		for (Map.Entry<Variable, DI> entry : parsedDescription.entrySet())
			result = result.addNewVariable(entry.getKey(), entry.getValue(), false);
		return result;
	}
	
}
