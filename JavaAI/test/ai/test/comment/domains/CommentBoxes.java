package ai.test.comment.domains;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.cfg.CFGVertice;
import ai.domain.Variable;
import ai.domain.boxes.IntegerBoxes;
import ai.domain.boxes.IntegerBoxesHelper;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.interval.Interval;
import ai.test.comment.CommentParserException;

public class CommentBoxes extends CommentDomainBase<IntegerBoxes, List<Map<Variable, Interval>>> {

	private static final String EMPTY = "";
	private final CommentIntv intvParser;
	private static final Pattern INTV_BOX_PATTERN_PREFIX = Pattern.compile("\\s*(\\{[^\\}]*\\}),?(.*)$");

	public CommentBoxes(MatchResult result) {
		super(result);
		this.intvParser = new CommentIntv(result);
	}

	@Override
	public List<Map<Variable, Interval>> parseItemDescription(String description) {
		List<Map<Variable, Interval>> result = new LinkedList<Map<Variable, Interval>>();
		do {
			if (description.equals(EMPTY))
				break;
			Matcher matcher = INTV_BOX_PATTERN_PREFIX.matcher(description);
			if (!matcher.matches())
				throw new CommentParserException("Invalid domain value '%s'", description);
			String boxDescription = matcher.group(1).trim();
			//FIXME: add restrictions on box??
			Map<Variable, Interval> box = intvParser.parseItemDescription(boxDescription);
			result.add(box);
			description = matcher.group(2);
		} while (description.length() > 0);		
		return result;
	}

	@Override
	public IntegerBoxes createInitialValue(List<Map<Variable, Interval>> parsedDescription) {
		if (parsedDescription.size() == 0)
			return IntegerBoxes.getInitialValue();
		IntegerBoxes result = IntegerBoxes.BOTTOM;
		for(Map<Variable, Interval> item: parsedDescription) {
			NonRelationalDomain<Interval> parsedItem = intvParser.createInitialValue(item);
			result = result.join(IntegerBoxesHelper.fromIntervalBox(parsedItem));
		}
		return result;
	}

	@Override
	public void compareResult(CFGVertice vertice, IntegerBoxes analysisResult) {
		String commentVertice = match.getCommentVertice(vertice);
		List<Map<Variable, Interval>> boxes = mapping.get(commentVertice);
		if (boxes == null || boxes.size() == 0) {
			org.junit.Assert.assertEquals(IntegerBoxes.BOTTOM, analysisResult);
		} else if (analysisResult.isBottom()) {
			throw new AssertionError(String.format("Analysis result is bottom while value for '%s' is not.",
					commentVertice));
		} else {
			String message = String.format("Node: '%s'", commentVertice);
			List<NonRelationalDomain<Interval>> resultBoxes = IntegerBoxesHelper.split(analysisResult);
			org.junit.Assert.assertEquals(message + "\nexpected: " + boxes + "\nactual:" + resultBoxes, boxes.size(), resultBoxes.size());
			//iterate over both
			ListIterator<Map<Variable, Interval>> boxesIterator = boxes.listIterator();
			ListIterator<NonRelationalDomain<Interval>> resultBoxesIterator = resultBoxes.listIterator();
			while(boxesIterator.hasNext()) {
				Map<Variable, Interval> expectedBox = boxesIterator.next();
				NonRelationalDomain<Interval> actualBox = resultBoxesIterator.next();
				intvParser.compare(expectedBox, actualBox, commentVertice);
			}
		}
	}
}
