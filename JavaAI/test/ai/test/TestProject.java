package ai.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;

import ai.Utils;
import ai.cfg.InterestingCodeFragment;
import ai.cfg.MethodCFGBuilder;
import ai.cfg.MethodControlFlowGraph;
import ai.cfg.MethodFinder;

public class TestProject {
	public class ProjectFile {
		private final IPackageFragment pf;
		private final ICompilationUnit icu;
		private CompilationUnit cu;

		ProjectFile(String packageName, String filename) {
			this.pf = findPackage(packageName);
			org.junit.Assert.assertNotNull("Package '"+packageName+"' not found!!", this.pf);
			this.icu = pf.getCompilationUnit(filename);
			this.cu = Utils.parse(icu);
			org.junit.Assert.assertNotNull("Compilation unit '"+ filename + "' not found in package '"+ packageName +"'!", this.cu);
		}
		
		public MethodControlFlowGraph getCFG(String className, String methodName) {
			String qualifiedMethodName = pf.getElementName() + "." + className + ":" + methodName;
			for(InterestingCodeFragment codeFragment: MethodFinder.findMethods(cu)){
				if (codeFragment.getUniqueName().startsWith(qualifiedMethodName+"(")){
					return MethodCFGBuilder.buildCFG(codeFragment);
				}
			}
			throw new AssertionError("Method '" + qualifiedMethodName + "' not found!!");
		}

		public Collection<MethodControlFlowGraph> getAllMethodGraphs() {
			List<MethodControlFlowGraph> result = new LinkedList<MethodControlFlowGraph>();
			for(InterestingCodeFragment codeFragment: MethodFinder.findMethods(cu)){
				result.add(MethodCFGBuilder.buildCFG(codeFragment));
			}
			return result;
		}

		public ICompilationUnit getIcu() {
			return icu;
		}
	}
	
	private final IJavaProject project;
	private final ArrayList<IPackageFragment> sources;
	public TestProject(String name) throws CoreException {
		project = openProject(name);
		sources = getSourcePackageFragments();
	}
	
	private ArrayList<IPackageFragment> getSourcePackageFragments() throws JavaModelException {
		ArrayList<IPackageFragment> result = new ArrayList<IPackageFragment>();
		for(IPackageFragment pf: project.getPackageFragments()){
			if (pf.getKind() == IPackageFragmentRoot.K_BINARY)
				continue;
			result.add(pf);
			}
		if (result.isEmpty())
			throw new RuntimeException("No source packages found");
		return result;
	}

	private IJavaProject openProject(String name) throws CoreException {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		for(IProject project: workspace.getRoot().getProjects()){
			if (project.getDescription().getName().equals(name)){
				//prepare project for tests
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
				IJavaProject result = JavaCore.create(project);
				result.open(null);
				result.makeConsistent(null);
				assert result.isOpen();
				assert result.isConsistent();
				return result;
			}
		}
		org.junit.Assert.fail("Project '" + name + "' not found!!");
		return null;
	}
	
	private IPackageFragment findPackage(String packageName) {
		for(IPackageFragment source: sources){
			if (source.getElementName().equals(packageName))
				return source; 
		}
		return null;
	}
	
	public ProjectFile openFile(String packageName, String filename) {
		return new ProjectFile(packageName, filename);
	}

	public MethodControlFlowGraph getControlFlowGraph(String packageName, String filename, String qualifiedMathodName) {
		ProjectFile pf = new ProjectFile(packageName, filename);
		String[] names = qualifiedMathodName.split(":");
		org.junit.Assert.assertEquals(names.length, 2);
		return pf.getCFG(names[0], names[1]);
	}
	
//	public MethodControlFlowGraph getControlFlowGraph(String packageName, String filename, String qualifiedMethodName) {
//		CompilationUnit cu = getCompilationUnit(packageName, filename).right;
//		org.junit.Assert.assertNotNull("Compilation unit '"+ filename + "' not found in package '"+ packageName +"'!", cu);
//		for(MethodDeclaration method: MethodFinder.findMethods(cu)){
//			if (TestUtils.getUniqueMethodName(method).equals(qualifiedMethodName)){
//				return MethodCFGBuilder.buildCFG(method);
//			}
//		}
//		throw new AssertionError("Method '" + qualifiedMethodName + "' not found!!");
//	}
//	
}
