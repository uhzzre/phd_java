package ai.domain.bool.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.bool.Bool;

@RunWith(JUnit4.class)
public class DomainTest {
	@Test
	public void testBottom() {
		org.junit.Assert.assertTrue(Bool.BOTTOM.isBottom());
		org.junit.Assert.assertFalse(Bool.FALSE.isBottom());
		org.junit.Assert.assertFalse(Bool.TRUE.isBottom());
		org.junit.Assert.assertFalse(Bool.TOP.isBottom());
	}

	@Test
	public void testEquals(){
		org.junit.Assert.assertTrue(Bool.BOTTOM.equals(Bool.BOTTOM));
		org.junit.Assert.assertTrue(Bool.FALSE.equals(Bool.FALSE));
		org.junit.Assert.assertTrue(Bool.TRUE.equals(Bool.TRUE));
		org.junit.Assert.assertTrue(Bool.TOP.equals(Bool.TOP));
		
		org.junit.Assert.assertFalse(Bool.BOTTOM.equals(Bool.FALSE));
		org.junit.Assert.assertFalse(Bool.BOTTOM.equals(Bool.TRUE));
		org.junit.Assert.assertFalse(Bool.BOTTOM.equals(Bool.TOP));

		org.junit.Assert.assertFalse(Bool.FALSE.equals(Bool.BOTTOM));
		org.junit.Assert.assertFalse(Bool.FALSE.equals(Bool.TRUE));
		org.junit.Assert.assertFalse(Bool.FALSE.equals(Bool.TOP));

		org.junit.Assert.assertFalse(Bool.TRUE.equals(Bool.FALSE));
		org.junit.Assert.assertFalse(Bool.TRUE.equals(Bool.BOTTOM));
		org.junit.Assert.assertFalse(Bool.TRUE.equals(Bool.TOP));

		org.junit.Assert.assertFalse(Bool.TOP.equals(Bool.FALSE));
		org.junit.Assert.assertFalse(Bool.TOP.equals(Bool.TRUE));
		org.junit.Assert.assertFalse(Bool.TOP.equals(Bool.BOTTOM));
	}

	@Test
	public void testLeq(){
		org.junit.Assert.assertTrue(Bool.BOTTOM.leq(Bool.BOTTOM));
		org.junit.Assert.assertTrue(Bool.BOTTOM.leq(Bool.FALSE));
		org.junit.Assert.assertTrue(Bool.BOTTOM.leq(Bool.TRUE));
		org.junit.Assert.assertTrue(Bool.BOTTOM.leq(Bool.TOP));
		
		org.junit.Assert.assertFalse(Bool.FALSE.leq(Bool.BOTTOM));
		org.junit.Assert.assertTrue(Bool.FALSE.leq(Bool.FALSE));
		org.junit.Assert.assertFalse(Bool.FALSE.leq(Bool.TRUE));
		org.junit.Assert.assertTrue(Bool.FALSE.leq(Bool.TOP));

		org.junit.Assert.assertFalse(Bool.TRUE.leq(Bool.BOTTOM));
		org.junit.Assert.assertFalse(Bool.TRUE.leq(Bool.FALSE));
		org.junit.Assert.assertTrue(Bool.TRUE.leq(Bool.TRUE));
		org.junit.Assert.assertTrue(Bool.TRUE.leq(Bool.TOP));

		org.junit.Assert.assertFalse(Bool.TOP.leq(Bool.BOTTOM));
		org.junit.Assert.assertFalse(Bool.TOP.leq(Bool.FALSE));
		org.junit.Assert.assertFalse(Bool.TOP.leq(Bool.TRUE));
		org.junit.Assert.assertTrue(Bool.TOP.leq(Bool.TOP));
	}

	@Test
	public void testJoin(){
		org.junit.Assert.assertEquals(Bool.BOTTOM.join(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.join(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.BOTTOM.join(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.BOTTOM.join(Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(Bool.FALSE.join(Bool.BOTTOM), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.FALSE.join(Bool.TRUE), Bool.TOP);
		org.junit.Assert.assertEquals(Bool.FALSE.join(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.FALSE.join(Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(Bool.TRUE.join(Bool.BOTTOM), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TRUE.join(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TRUE.join(Bool.FALSE), Bool.TOP);
		org.junit.Assert.assertEquals(Bool.TRUE.join(Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(Bool.TOP.join(Bool.BOTTOM), Bool.TOP);
		org.junit.Assert.assertEquals(Bool.TOP.join(Bool.TRUE), Bool.TOP);
		org.junit.Assert.assertEquals(Bool.TOP.join(Bool.FALSE), Bool.TOP);
		org.junit.Assert.assertEquals(Bool.TOP.join(Bool.TOP), Bool.TOP);
	}

	@Test
	public void testMeet(){
		org.junit.Assert.assertEquals(Bool.BOTTOM.meet(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.meet(Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.meet(Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.meet(Bool.TOP), Bool.BOTTOM);

		org.junit.Assert.assertEquals(Bool.FALSE.meet(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.FALSE.meet(Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.FALSE.meet(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.FALSE.meet(Bool.TOP), Bool.FALSE);

		org.junit.Assert.assertEquals(Bool.TRUE.meet(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TRUE.meet(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TRUE.meet(Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TRUE.meet(Bool.TOP), Bool.TRUE);

		org.junit.Assert.assertEquals(Bool.TOP.meet(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TOP.meet(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TOP.meet(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.TOP.meet(Bool.TOP), Bool.TOP);
	}

	@Test
	public void testWiden(){
		org.junit.Assert.assertEquals(Bool.BOTTOM.widen(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.widen(Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.widen(Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.BOTTOM.widen(Bool.TOP), Bool.BOTTOM);

		org.junit.Assert.assertEquals(Bool.FALSE.widen(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.FALSE.widen(Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.FALSE.widen(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.FALSE.widen(Bool.TOP), Bool.FALSE);

		org.junit.Assert.assertEquals(Bool.TRUE.widen(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TRUE.widen(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TRUE.widen(Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TRUE.widen(Bool.TOP), Bool.TRUE);

		org.junit.Assert.assertEquals(Bool.TOP.widen(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(Bool.TOP.widen(Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(Bool.TOP.widen(Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(Bool.TOP.widen(Bool.TOP), Bool.TOP);
	}
}
