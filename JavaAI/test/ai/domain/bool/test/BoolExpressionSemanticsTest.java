package ai.domain.bool.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.bool.Bool;
import ai.domain.bool.BoolExpressionSemantics;

@RunWith(JUnit4.class)
public class BoolExpressionSemanticsTest {
	BoolExpressionSemantics sem = new BoolExpressionSemantics();
	@Test
	public void testNot() {		
		org.junit.Assert.assertEquals(sem.not(Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.not(Bool.FALSE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.not(Bool.TRUE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.not(Bool.TOP), Bool.TOP);
	}

	@Test
	public void testAnd(){
		org.junit.Assert.assertEquals(sem.and(Bool.BOTTOM, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.BOTTOM, Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.BOTTOM, Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.BOTTOM, Bool.TOP), Bool.BOTTOM);
		
		org.junit.Assert.assertEquals(sem.and(Bool.FALSE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.FALSE, Bool.TRUE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.and(Bool.FALSE, Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.and(Bool.FALSE, Bool.TOP), Bool.FALSE);

		org.junit.Assert.assertEquals(sem.and(Bool.TRUE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.TRUE, Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.and(Bool.TRUE, Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.and(Bool.TRUE, Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(sem.and(Bool.TOP, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.and(Bool.TOP, Bool.TRUE), Bool.TOP);
		org.junit.Assert.assertEquals(sem.and(Bool.TOP, Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.and(Bool.TOP, Bool.TOP), Bool.TOP);
	}

	@Test
	public void testOr(){
		org.junit.Assert.assertEquals(sem.or(Bool.BOTTOM, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.BOTTOM, Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.BOTTOM, Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.BOTTOM, Bool.TOP), Bool.BOTTOM);
		
		org.junit.Assert.assertEquals(sem.or(Bool.FALSE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.FALSE, Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.or(Bool.FALSE, Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.or(Bool.FALSE, Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(sem.or(Bool.TRUE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.TRUE, Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.or(Bool.TRUE, Bool.FALSE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.or(Bool.TRUE, Bool.TOP), Bool.TRUE);

		org.junit.Assert.assertEquals(sem.or(Bool.TOP, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.or(Bool.TOP, Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.or(Bool.TOP, Bool.FALSE), Bool.TOP);
		org.junit.Assert.assertEquals(sem.or(Bool.TOP, Bool.TOP), Bool.TOP);
	}

	@Test
	public void testXor(){
		org.junit.Assert.assertEquals(sem.xor(Bool.BOTTOM, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.BOTTOM, Bool.TRUE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.BOTTOM, Bool.FALSE), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.BOTTOM, Bool.TOP), Bool.BOTTOM);
		
		org.junit.Assert.assertEquals(sem.xor(Bool.FALSE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.FALSE, Bool.TRUE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.xor(Bool.FALSE, Bool.FALSE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.xor(Bool.FALSE, Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(sem.xor(Bool.TRUE, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.TRUE, Bool.TRUE), Bool.FALSE);
		org.junit.Assert.assertEquals(sem.xor(Bool.TRUE, Bool.FALSE), Bool.TRUE);
		org.junit.Assert.assertEquals(sem.xor(Bool.TRUE, Bool.TOP), Bool.TOP);

		org.junit.Assert.assertEquals(sem.xor(Bool.TOP, Bool.BOTTOM), Bool.BOTTOM);
		org.junit.Assert.assertEquals(sem.xor(Bool.TOP, Bool.TRUE), Bool.TOP);
		org.junit.Assert.assertEquals(sem.xor(Bool.TOP, Bool.FALSE), Bool.TOP);
		org.junit.Assert.assertEquals(sem.xor(Bool.TOP, Bool.TOP), Bool.TOP);
	}
}
