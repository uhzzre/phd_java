package ai.domain.bool.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;
import ai.test.TestProject.ProjectFile;

@RunWith(JUnit4.class)
public class AutoBoxingAndUnboxing extends TestsBase{

	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleans(cfg);
	}

	@Test
	public void testVariableDeclaration() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration01"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration02"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration03"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration04"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration05"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration06"));
	}
	
	@Test
	public void testUnboxingArrayAccess() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingArrayAccess01"));
	}

	@Test
	public void testUnboxingAssignment() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingAssignment01"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment02"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment03"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment04"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment05"));
	}

	@Test
	public void testUnboxingInfix() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingInfix01"));
		performTest(pf.getCFG("Boxing", "unboxingInfix02"));
		performTest(pf.getCFG("Boxing", "unboxingInfix03"));
	}

	@Test
	public void testUnboxingMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingMethodInvocation01"));
	}

	@Test
	public void testUnboxingSimpleName() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSimpleName01"));
	}

	@Test
	public void testUnboxingPrefixExpression() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingPrefix01"));
		performTest(pf.getCFG("Boxing", "unboxingPrefix02"));
	}

	@Test
	public void testUnboxingSuperFieldAccess() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSuperField01"));
		performTest(pf.getCFG("Boxing", "unboxingSuperField02"));
		performTest(pf.getCFG("Boxing", "unboxingSuperField03"));
	}

	@Test
	public void testUnboxingSuperMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSuperMethodInvocation01"));
	}
}
