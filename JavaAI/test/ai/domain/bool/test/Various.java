package ai.domain.bool.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;
import ai.test.TestProject.ProjectFile;

@RunWith(JUnit4.class)
public class Various extends TestsBase  {

	@Test
	public void testConstructor() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");		
		performTest(pf.getCFG("Various", "Various"));
	}
	
	@Test
	public void testArguments() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");		
		performTest(pf.getCFG("Various", "arguments01"));
		performTest(pf.getCFG("Various", "arguments02"));
		performTest(pf.getCFG("Various", "arguments03"));
		performTest(pf.getCFG("Various", "arguments04"));
	}

	@Test
	public void testBlockVariables() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");		
		performTest(pf.getCFG("Various", "blockVariables01"));
		performTest(pf.getCFG("Various", "blockVariables02"));
		performTest(pf.getCFG("Various", "blockVariables03"));
		performTest(pf.getCFG("Various", "blockVariables04"));
		performTest(pf.getCFG("Various", "blockVariables05"));
		performTest(pf.getCFG("Various", "blockVariables06"));
		performTest(pf.getCFG("Various", "blockVariables07"));
		performTest(pf.getCFG("Various", "blockVariables08"));
	}

	@Test
	public void testTryCatch() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");		
		performTest(pf.getCFG("Various", "tryCatchFinally01"));
		performTest(pf.getCFG("Various", "tryCatchFinally02"));
		performTest(pf.getCFG("Various", "tryCatchFinally03"));
		performTest(pf.getCFG("Various", "tryCatchFinally04"));
		performTest(pf.getCFG("Various", "tryCatchFinally05"));
		performTest(pf.getCFG("Various", "tryCatchFinally06"));
	}
	
	@Test
	public void testSwitch() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "BooleanSwitch.java");
		performTest(pf.getCFG("BooleanSwitch", "switchVariableDef01"));
		
		performTest(pf.getCFG("BooleanSwitch", "switchEnumExprExecuteOnce01"));
		performTest(pf.getCFG("BooleanSwitch", "switchEnumExprExecuteOnce02"));
		performTest(pf.getCFG("BooleanSwitch", "switchEnumExprExecuteOnce03"));
		performTest(pf.getCFG("BooleanSwitch", "switchEnumExprExecuteOnce04"));
		
		performTest(pf.getCFG("BooleanSwitch", "switchIntExprExecuteOnce01"));
		performTest(pf.getCFG("BooleanSwitch", "switchIntExprExecuteOnce02"));
		performTest(pf.getCFG("BooleanSwitch", "switchIntExprExecuteOnce03"));
		performTest(pf.getCFG("BooleanSwitch", "switchIntExprExecuteOnce04"));

		pf = project.openFile("domain.commons", "IntegerSwitch.java");
		performTest(pf.getCFG("IntegerSwitch", "switchEnumExprExecuteOnce01"));
		performTest(pf.getCFG("IntegerSwitch", "switchEnumExprExecuteOnce02"));
		performTest(pf.getCFG("IntegerSwitch", "switchEnumExprExecuteOnce03"));
		performTest(pf.getCFG("IntegerSwitch", "switchEnumExprExecuteOnce04"));
		
		performTest(pf.getCFG("IntegerSwitch", "switchIntExprExecuteOnce01"));
		performTest(pf.getCFG("IntegerSwitch", "switchIntExprExecuteOnce02"));
		performTest(pf.getCFG("IntegerSwitch", "switchIntExprExecuteOnce03"));
		performTest(pf.getCFG("IntegerSwitch", "switchIntExprExecuteOnce04"));
	}

	@Test
	public void testConstructorInvocation() throws CoreException {
		testAllMethods("domain.commons.constructor", "ConstructorInvocation01.java");
		testAllMethods("domain.commons.constructor", "ConstructorInvocation02.java");
		testAllMethods("domain.commons.constructor", "ConstructorInvocation03.java");
	}

	@Test
	public void testSuperConstructorInvocation() throws CoreException {
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation01.java");
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation02.java");
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation03.java");
	}

	@Test
	public void testAssignToField() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");
		performTest(pf.getCFG("Various", "assignToField01"));
		performTest(pf.getCFG("Various", "assignToField02"));
	}
	
	@Test
	public void testEnhancedFor() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "Various.java");
		performTest(pf.getCFG("Various", "enhancedFor01"));
		performTest(pf.getCFG("Various", "enhancedFor02"));
		performTest(pf.getCFG("Various", "enhancedFor03"));
	}	

	@Test
	public void testConditionalExpression() throws CoreException {
		ProjectFile pf = project.openFile("domain.commons", "ConditionalExpression.java");
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition01"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition02"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition03"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition04"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition05"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition06"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition07"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition08"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition09"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition10"));
		performTest(pf.getCFG("ConditionalExpression", "notInterestingResultAndCondition11"));
		
		performTest(pf.getCFG("ConditionalExpression", "booleanResult01"));
		performTest(pf.getCFG("ConditionalExpression", "booleanResult02"));
		
		pf = project.openFile("domain.commons", "ConditionalExpressionNoInervalComparison.java");
		performTest(pf.getCFG("ConditionalExpressionNoInervalComparison", "booleanResult03"));

		pf = project.openFile("domain.commons", "ConditionalExpression.java");
		performTest(pf.getCFG("ConditionalExpression", "integerResult01"));
		performTest(pf.getCFG("ConditionalExpression", "integerResult02"));
		performTest(pf.getCFG("ConditionalExpression", "integerResult03"));
	}	
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleans(cfg);
	}
}
