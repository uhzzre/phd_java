package ai.domain.bool.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class BasicExpressionsTests extends TestsBase{
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleans(cfg);
	}

	@Test
	public void testInfixAnd() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "and01"));
		performTest(pf.getCFG("InfixExpressions", "and02"));
		performTest(pf.getCFG("InfixExpressions", "and03"));
		performTest(pf.getCFG("InfixExpressions", "and04"));

		performTest(pf.getCFG("InfixExpressions", "lazyAnd01"));
		performTest(pf.getCFG("InfixExpressions", "lazyAnd02"));
		performTest(pf.getCFG("InfixExpressions", "lazyAnd03"));
	}

	@Test
	public void testInfixOr() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "or01"));
		performTest(pf.getCFG("InfixExpressions", "or02"));
		performTest(pf.getCFG("InfixExpressions", "or03"));
		performTest(pf.getCFG("InfixExpressions", "or04"));
		performTest(pf.getCFG("InfixExpressions", "or05"));

		performTest(pf.getCFG("InfixExpressions", "lazyOr01"));
		performTest(pf.getCFG("InfixExpressions", "lazyOr02"));
		performTest(pf.getCFG("InfixExpressions", "lazyOr03"));
	}

	@Test
	public void testInfixXor() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "xor01"));
		performTest(pf.getCFG("InfixExpressions", "xor02"));
		performTest(pf.getCFG("InfixExpressions", "xor03"));
		performTest(pf.getCFG("InfixExpressions", "xor04"));

		performTest(pf.getCFG("InfixExpressions", "notLazyXor01"));
		performTest(pf.getCFG("InfixExpressions", "notLazyXor02"));
	}

	@Test
	public void testMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "MethodInvocation.java");
		performTest(pf.getCFG("MethodInvocation", "methodInvocation01"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation02"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation03"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation04"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation05"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation06"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation07"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation08"));
		
		performTest(pf.getCFG("MethodInvocation", "methodInvocationWithResult01"));
		
		performTest(pf.getCFG("MethodInvocation", "methodInvocationSuper01"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocationSuper02"));
		
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation01"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation02"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation03"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation04"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation05"));

		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationWithResult01"));

		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper01"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper02"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper03"));		
	}

	@Test
	public void testOperatorPrecedence() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "OperatorPrecedence.java");
		performTest(pf.getCFG("OperatorPrecedence", "notBeforeAnd01"));
		performTest(pf.getCFG("OperatorPrecedence", "notBeforeOr01"));
	}

	@Test
	public void testPrefixNot() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixNot01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixNot02"));
		performTest(pf.getCFG("PrefixExpressions", "prefixNot03"));
	}

	@Test
	public void testAssignAnd() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignAnd01"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd02"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd03"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd04"));
	}

	@Test
	public void testAssignOr() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignOr01"));
		performTest(pf.getCFG("AssignWithModify", "assignOr02"));
		performTest(pf.getCFG("AssignWithModify", "assignOr03"));
		performTest(pf.getCFG("AssignWithModify", "assignOr04"));
	}

	@Test
	public void testAssignXor() throws CoreException {
		ProjectFile pf = project.openFile("domain.booleans.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignXor01"));
		performTest(pf.getCFG("AssignWithModify", "assignXor02"));
		performTest(pf.getCFG("AssignWithModify", "assignXor03"));
		performTest(pf.getCFG("AssignWithModify", "assignXor04"));
	}
}
