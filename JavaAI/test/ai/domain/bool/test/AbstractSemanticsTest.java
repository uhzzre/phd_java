package ai.domain.bool.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class AbstractSemanticsTest extends TestsBase{
	

	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleans(cfg);
	}

	@Test
	public void testNewVariables() throws CoreException{
		ProjectFile pf = project.openFile("domain.booleans", "Variables.java");
		performTest(pf.getCFG("Variables", "testInitials"));
		performTest(pf.getCFG("Variables", "booleanVariableWithSimpleAssignment01"));
		performTest(pf.getCFG("Variables", "booleanVariableWithSimpleAssignment02"));
	}
}
