package ai.domain.intervals.test;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class AbstractSemanticTest extends TestsBase{
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testIntervals(cfg);
	}

	@Test
	public void testNewVariables() throws CoreException{
		ProjectFile pf = project.openFile("domain.intervals", "Variables.java");
		performTest(pf.getCFG("Variables", "initialValues"));
		performTest(pf.getCFG("Variables", "byteVariableWithSimpleAssignment"));
		performTest(pf.getCFG("Variables", "shortVariableWithSimpleAssignment"));
		performTest(pf.getCFG("Variables", "intVariableWithSimpleAssignment"));
		performTest(pf.getCFG("Variables", "longVariableWithSimpleAssignment1"));
		performTest(pf.getCFG("Variables", "longVariableWithSimpleAssignment2"));
		performTest(pf.getCFG("Variables", "floatVariableWithSimpleAssignment1"));
		performTest(pf.getCFG("Variables", "floatVariableWithSimpleAssignment2"));
		performTest(pf.getCFG("Variables", "doubleVariableWithSimpleAssignment"));
	}

	@Test
	public void testNumericArgument() throws CoreException{
		Logger logger = Logger.getRootLogger();
		logger.setLevel(Level.DEBUG);
		
		MethodControlFlowGraph cfg = project.getControlFlowGraph("domain.intervals", "Basics.java", "Basics:numericArgument");
		performTest(cfg);
	}

}
