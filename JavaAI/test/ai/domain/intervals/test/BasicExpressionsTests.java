package ai.domain.intervals.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class BasicExpressionsTests extends TestsBase{
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testIntervals(cfg);
	}

	@Test
	public void testPostfixIncrease() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PostfixExpressions.java");
		
		performTest(pf.getCFG("PostfixExpressions", "postfixIncrease01"));
		performTest(pf.getCFG("PostfixExpressions", "postfixIncrease02"));
		performTest(pf.getCFG("PostfixExpressions", "postfixIncrease03"));
	}

	@Test
	public void testPostfixDecrease() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PostfixExpressions.java");
		performTest(pf.getCFG("PostfixExpressions", "postfixDecrease01"));
		performTest(pf.getCFG("PostfixExpressions", "postfixDecrease02"));
		performTest(pf.getCFG("PostfixExpressions", "postfixDecrease03"));
	}

	@Test
	public void testPrefixIncrease() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixIncrease01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixIncrease02"));
		performTest(pf.getCFG("PrefixExpressions", "prefixIncrease03"));
	}

	@Test
	public void testPrefixDecrease() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixDecrease01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixDecrease02"));
		performTest(pf.getCFG("PrefixExpressions", "prefixDecrease03"));
	}
	
	@Test
	public void testPrefixMinus() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixMinus01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixMinus02"));
	}

	@Test
	public void testPrefixPlus() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixPlus01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixPlus02"));
	}

	@Test
	public void testPrefixComplement() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixExpressions.java");
		performTest(pf.getCFG("PrefixExpressions", "prefixComplement01"));
		performTest(pf.getCFG("PrefixExpressions", "prefixComplement02"));
	}

	@Test
	public void testInfixSum() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "sum01"));
		performTest(pf.getCFG("InfixExpressions", "sum02"));
		performTest(pf.getCFG("InfixExpressions", "sum03"));
		performTest(pf.getCFG("InfixExpressions", "sum04"));
		performTest(pf.getCFG("InfixExpressions", "sum05"));
	}

	@Test
	public void testInfixMultiply() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "multiply01"));
		performTest(pf.getCFG("InfixExpressions", "multiply02"));
		performTest(pf.getCFG("InfixExpressions", "multiply03"));
		performTest(pf.getCFG("InfixExpressions", "multiply04"));
		performTest(pf.getCFG("InfixExpressions", "multiply05"));
	}

	@Test
	public void testInfixMinus() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "minus01"));
		performTest(pf.getCFG("InfixExpressions", "minus02"));
		performTest(pf.getCFG("InfixExpressions", "minus03"));
		performTest(pf.getCFG("InfixExpressions", "minus04"));
		performTest(pf.getCFG("InfixExpressions", "minus05"));
	}

	@Test
	public void testInfixDivide() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "divide01"));
		performTest(pf.getCFG("InfixExpressions", "divide02"));
		performTest(pf.getCFG("InfixExpressions", "divide03"));
		performTest(pf.getCFG("InfixExpressions", "divide04"));
		performTest(pf.getCFG("InfixExpressions", "divide05"));
	}

	@Test
	public void testInfixRemainder() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "remainder01"));
	}

	@Test
	public void testInfixLeftShift() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "lhs01"));
		performTest(pf.getCFG("InfixExpressions", "lhs02"));
	}

	@Test
	public void testInfixRightShift() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "rhs01"));
		performTest(pf.getCFG("InfixExpressions", "rhs02"));
	}

	@Test
	public void testInfixRightShiftUnsigned() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "InfixExpressions.java");
		performTest(pf.getCFG("InfixExpressions", "rhsu01"));
		performTest(pf.getCFG("InfixExpressions", "rhsu02"));
	}

	@Test
	public void testPrefixPostfixUpdates() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "PrefixPostfixUpdates.java");
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations01"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations02"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations03"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations04"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations05"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations06"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations07"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations08"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations09"));
		performTest(pf.getCFG("PrefixPostfixUpdates", "arithmeticOperations10"));
	}

	@Test
	public void testOperatorPrecedence() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "OperatorPrecedence.java");
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeSum01"));
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeSum02"));
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeSum03"));
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeMinus01"));
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeMinus02"));
		performTest(pf.getCFG("OperatorPrecedence", "mulBeforeMinus03"));
		performTest(pf.getCFG("OperatorPrecedence", "plusMinus01"));
		performTest(pf.getCFG("OperatorPrecedence", "plusMinus02"));
	}
	
	@Test
	public void testAssignments() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "Assignments.java");
		performTest(pf.getCFG("Assignments", "assignment01"));
		performTest(pf.getCFG("Assignments", "assignment02"));
		performTest(pf.getCFG("Assignments", "assignment03"));
		performTest(pf.getCFG("Assignments", "assignment04"));
		performTest(pf.getCFG("Assignments", "assignment05"));
		performTest(pf.getCFG("Assignments", "assignment06"));
	}

	@Test
	public void testMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "MethodInvocation.java");
		performTest(pf.getCFG("MethodInvocation", "methodInvocation01"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation02"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation03"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation04"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation05"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation06"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation07"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocation08"));
		
		performTest(pf.getCFG("MethodInvocation", "methodInvocationWithResult01"));
		
		performTest(pf.getCFG("MethodInvocation", "methodInvocationSuper01"));
		performTest(pf.getCFG("MethodInvocation", "methodInvocationSuper02"));
		
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation01"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation02"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation03"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation04"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocation05"));

		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationWithResult01"));

		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper01"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper02"));
		performTest(pf.getCFG("MethodInvocation", "booleanMethodInvocationSuper03"));		
	}

	@Test
	public void testFieldAccess() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "FieldAccess.java");
		performTest(pf.getCFG("FieldAccess", "numericFieldAccess01"));
		performTest(pf.getCFG("FieldAccess", "numericFieldAccess02"));
		performTest(pf.getCFG("FieldAccess", "numericFieldAccess03"));
		performTest(pf.getCFG("FieldAccess", "numericFieldAccess04"));
		performTest(pf.getCFG("FieldAccess", "numericFieldAccess05"));

		performTest(pf.getCFG("FieldAccess", "numericSuperFieldAccess01"));
		performTest(pf.getCFG("FieldAccess", "numericSuperFieldAccess02"));
		performTest(pf.getCFG("FieldAccess", "numericSuperFieldAccess03"));

		performTest(pf.getCFG("FieldAccess", "booleanFieldAccess01"));
		performTest(pf.getCFG("FieldAccess", "booleanFieldAccess02"));
		performTest(pf.getCFG("FieldAccess", "booleanFieldAccess03"));
		performTest(pf.getCFG("FieldAccess", "booleanFieldAccess04"));
		performTest(pf.getCFG("FieldAccess", "booleanFieldAccess05"));

		performTest(pf.getCFG("FieldAccess", "booleanSuperFieldAccess01"));
		performTest(pf.getCFG("FieldAccess", "booleanSuperFieldAccess02"));
		performTest(pf.getCFG("FieldAccess", "booleanSuperFieldAccess03"));
	}

	@Test
	public void testClassInstanceCreation() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "ClassInstanceCreation.java");
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance01"));
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance02"));
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance03"));
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance04"));
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance05"));
		performTest(pf.getCFG("ClassInstanceCreation", "classInstance06"));
	}

	@Test
	public void testArrays() throws CoreException {
//		Logger logger = Logger.getRootLogger();
//		logger.setLevel(Level.DEBUG);
		ProjectFile pf = project.openFile("domain.expressions", "Arrays.java");
		performTest(pf.getCFG("Arrays", "arrayCreation01"));
		performTest(pf.getCFG("Arrays", "arrayCreation02"));
		performTest(pf.getCFG("Arrays", "arrayCreation03"));
		performTest(pf.getCFG("Arrays", "arrayCreation04"));
		performTest(pf.getCFG("Arrays", "arrayCreation05"));
		performTest(pf.getCFG("Arrays", "arrayCreation06"));
		performTest(pf.getCFG("Arrays", "arrayAccess01"));
		performTest(pf.getCFG("Arrays", "arrayAccess02"));
		performTest(pf.getCFG("Arrays", "arrayAccess03"));
	}

	@Test
	public void testIgnoredExpressions() throws CoreException {
		ProjectFile pf = project.openFile("domain.expressions", "IgnoredExpressions.java");
		performTest(pf.getCFG("IgnoredExpressions", "ignoreThis01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreStringLiteral01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreBooleanLiteral01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreCharacterLiteral01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreFieldAccess01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreNull01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreTypeLiteral01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreInstanceOf01"));
		performTest(pf.getCFG("IgnoredExpressions", "ignoreCast01"));
	}
	
	@Test
	public void testAssignAndPlus() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignPlus01"));
		performTest(pf.getCFG("AssignWithModify", "assignPlus02"));
		performTest(pf.getCFG("AssignWithModify", "assignPlus03"));
		performTest(pf.getCFG("AssignWithModify", "assignPlus04"));
	}
	
	@Test
	public void testAssignAndMinus() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignMinus01"));
		performTest(pf.getCFG("AssignWithModify", "assignMinus02"));
		performTest(pf.getCFG("AssignWithModify", "assignMinus03"));
		performTest(pf.getCFG("AssignWithModify", "assignMinus04"));
	}

	@Test
	public void testAssignAndTimes() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignTimes01"));
		performTest(pf.getCFG("AssignWithModify", "assignTimes02"));
		performTest(pf.getCFG("AssignWithModify", "assignTimes03"));
		performTest(pf.getCFG("AssignWithModify", "assignTimes04"));
	}

	@Test
	public void testAssignAndDivide() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignDivide01"));
		performTest(pf.getCFG("AssignWithModify", "assignDivide02"));
		performTest(pf.getCFG("AssignWithModify", "assignDivide03"));
		performTest(pf.getCFG("AssignWithModify", "assignDivide04"));
	}

	@Test
	public void testAssignAndBitAnd() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignAnd01"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd02"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd03"));
		performTest(pf.getCFG("AssignWithModify", "assignAnd04"));
	}

	@Test
	public void testAssignAndBitOr() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignOr01"));
		performTest(pf.getCFG("AssignWithModify", "assignOr02"));
		performTest(pf.getCFG("AssignWithModify", "assignOr03"));
		performTest(pf.getCFG("AssignWithModify", "assignOr04"));
	}

	@Test
	public void testAssignAndBitXor() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignXor01"));
		performTest(pf.getCFG("AssignWithModify", "assignXor02"));
		performTest(pf.getCFG("AssignWithModify", "assignXor03"));
		performTest(pf.getCFG("AssignWithModify", "assignXor04"));
	}

	@Test
	public void testAssignAndRemainder() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignRemainder01"));
		performTest(pf.getCFG("AssignWithModify", "assignRemainder02"));
		performTest(pf.getCFG("AssignWithModify", "assignRemainder03"));
		performTest(pf.getCFG("AssignWithModify", "assignRemainder04"));
	}

	@Test
	public void testAssignAndLeftShift() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignLeftShift01"));
		performTest(pf.getCFG("AssignWithModify", "assignLeftShift02"));
		performTest(pf.getCFG("AssignWithModify", "assignLeftShift03"));
		performTest(pf.getCFG("AssignWithModify", "assignLeftShift04"));
	}

	@Test
	public void testAssignAndRightShiftSigned() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftSigned01"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftSigned02"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftSigned03"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftSigned04"));
	}

	@Test
	public void testAssignAndRightShiftUnsigned() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals.expressions", "AssignWithModify.java");
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftUnsigned01"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftUnsigned02"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftUnsigned03"));
		performTest(pf.getCFG("AssignWithModify", "assignRightShiftUnsigned04"));
	}
}
