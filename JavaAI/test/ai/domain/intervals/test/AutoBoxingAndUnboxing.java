package ai.domain.intervals.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;
import ai.test.TestProject.ProjectFile;

@RunWith(JUnit4.class)
public class AutoBoxingAndUnboxing extends TestsBase{

	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testIntervals(cfg);
	}

	@Test
	public void testVariableDeclaration() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration01"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration02"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration03"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration04"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration05"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration06"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration07"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration08"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration09"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration10"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration11"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration12"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration13"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration14"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration15"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration16"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration17"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration18"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration19"));
		performTest(pf.getCFG("Boxing", "unboxingVariableDeclaration20"));
	}
	
	@Test
	public void testUnboxingArrayAccess() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingArrayAccess01"));
	}

	@Test
	public void testUnboxingAssignment() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingAssignment01"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment02"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment03"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment04"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment05"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment06"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment07"));
		performTest(pf.getCFG("Boxing", "unboxingAssignment08"));
	}

	@Test
	public void testUnboxingInfix() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingInfix01"));
		performTest(pf.getCFG("Boxing", "unboxingInfix02"));
		performTest(pf.getCFG("Boxing", "unboxingInfix03"));
	}

	@Test
	public void testUnboxingMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingMethodInvocation01"));
	}

	@Test
	public void testUnboxingSimpleName() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSimpleName01"));
	}

	@Test
	public void testUnboxingPrefixExpression() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingPrefix01"));
	}

	@Test
	public void testUnboxingPostfixExpression() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingPostfix01"));
	}

	@Test
	public void testUnboxingSuperFieldAccess() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSuperField01"));
		performTest(pf.getCFG("Boxing", "unboxingSuperField02"));
	}

	@Test
	public void testUnboxingSuperMethodInvocation() throws CoreException {
		ProjectFile pf = project.openFile("domain.intervals", "Boxing.java");
		performTest(pf.getCFG("Boxing", "unboxingSuperMethodInvocation01"));
	}
}
