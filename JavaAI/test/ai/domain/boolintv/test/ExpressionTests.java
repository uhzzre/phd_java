package ai.domain.boolintv.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class ExpressionTests extends TestsBase  {

	@Test
	public void testComparisonSideefects() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions", "BooleanExpressions.java");
		
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect01"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect02"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect03"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect04"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect05"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect06"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect07"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonSideeffect08"));
	}

	@Test
	public void testComparisonNarrowsDown() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions", "BooleanExpressions.java");
		
		performTest(pf.getCFG("BooleanExpressions", "comparisonNarrowsDown01"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonNarrowsDown02"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonNarrowsDown03"));
		performTest(pf.getCFG("BooleanExpressions", "comparisonNarrowsDown04"));
	}

	@Test
	public void testNumericCompare() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions", "BooleanExpressions.java");
		
		performTest(pf.getCFG("BooleanExpressions", "numericCompare01"));
		performTest(pf.getCFG("BooleanExpressions", "numericCompare02"));
		performTest(pf.getCFG("BooleanExpressions", "numericCompare03"));
	}
	
//	@Test
//	public void testBooleanComparisions() throws CoreException {
//		ProjectFile pf = project.openFile("domain.boolintv.expressions", "BooleanExpressions.java");
//		
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans01"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans02"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans03"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans04"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans05"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans06"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans07"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans08"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans09"));
//		performTest(pf.getCFG("BooleanExpressions", "comparisonOfBooleans10"));
//	}

	@Test
	public void testBooleanTestNotNarrowsDown() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions", "BooleanExpressions.java");
		
		performTest(pf.getCFG("BooleanExpressions", "booleanTestNotNarrowsDown01"));
		performTest(pf.getCFG("BooleanExpressions", "booleanTestNotNarrowsDown02"));
		performTest(pf.getCFG("BooleanExpressions", "booleanTestNotNarrowsDown03"));
		performTest(pf.getCFG("BooleanExpressions", "booleanTestNotNarrowsDown04"));
	}
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleansAndIntervals(cfg);
	}
}
