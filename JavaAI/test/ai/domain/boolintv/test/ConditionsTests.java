package ai.domain.boolintv.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class ConditionsTests extends TestsBase  {

	@Test
	public void testSimpleLe() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");
		
		performTest(pf.getCFG("SimpleNumericCompare", "le01"));
		performTest(pf.getCFG("SimpleNumericCompare", "le02"));
		performTest(pf.getCFG("SimpleNumericCompare", "le03"));
		performTest(pf.getCFG("SimpleNumericCompare", "le04"));
	}

	@Test
	public void testSimpleLeq() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");		
		performTest(pf.getCFG("SimpleNumericCompare", "leq01"));
		performTest(pf.getCFG("SimpleNumericCompare", "leq02"));
		performTest(pf.getCFG("SimpleNumericCompare", "leq03"));
		performTest(pf.getCFG("SimpleNumericCompare", "leq04"));
	}

	@Test
	public void testSimpleGe() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");
		performTest(pf.getCFG("SimpleNumericCompare", "ge01"));
		performTest(pf.getCFG("SimpleNumericCompare", "ge02"));
		performTest(pf.getCFG("SimpleNumericCompare", "ge03"));
		performTest(pf.getCFG("SimpleNumericCompare", "ge04"));
	}

	@Test
	public void testSimpleGeq() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");		
		performTest(pf.getCFG("SimpleNumericCompare", "geq01"));
		performTest(pf.getCFG("SimpleNumericCompare", "geq02"));
		performTest(pf.getCFG("SimpleNumericCompare", "geq03"));
		performTest(pf.getCFG("SimpleNumericCompare", "geq04"));
	}

	@Test
	public void testSimpleEq() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");
		performTest(pf.getCFG("SimpleNumericCompare", "eq01"));
		performTest(pf.getCFG("SimpleNumericCompare", "eq02"));
		performTest(pf.getCFG("SimpleNumericCompare", "eq03"));
		performTest(pf.getCFG("SimpleNumericCompare", "eq04"));
	}

	@Test
	public void testSimpleNeq() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "SimpleNumericCompare.java");
		performTest(pf.getCFG("SimpleNumericCompare", "neq01"));
		performTest(pf.getCFG("SimpleNumericCompare", "neq02"));
		performTest(pf.getCFG("SimpleNumericCompare", "neq03"));
		performTest(pf.getCFG("SimpleNumericCompare", "neq04"));
	}
	
	@Test
	public void testLazyAnd() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "LazyOperators.java");
		performTest(pf.getCFG("LazyOperators", "lazyAnd01"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd02"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd03"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd04"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd05"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd06"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd07"));
		performTest(pf.getCFG("LazyOperators", "lazyAnd08"));
	}

	@Test
	public void testLazyOr() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.conditions", "LazyOperators.java");
		performTest(pf.getCFG("LazyOperators", "lazyOr01"));
		performTest(pf.getCFG("LazyOperators", "lazyOr02"));
		performTest(pf.getCFG("LazyOperators", "lazyOr03"));
		performTest(pf.getCFG("LazyOperators", "lazyOr04"));
		performTest(pf.getCFG("LazyOperators", "lazyOr05"));
		performTest(pf.getCFG("LazyOperators", "lazyOr06"));
		performTest(pf.getCFG("LazyOperators", "lazyOr07"));
		performTest(pf.getCFG("LazyOperators", "lazyOr08"));
	}
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleansAndIntervals(cfg);
	}
}
