package ai.domain.boolintv.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class InheritedTests extends TestsBase {
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleansAndIntervals(cfg);
	}

	@Test
	public void testNewVariables() throws CoreException {
		testAllMethods("domain.booleans", "Variables.java");
		testAllMethods("domain.intervals", "Variables.java");
	}

	@Test
	public void testArrays() throws CoreException {
		testAllMethods("domain.expressions", "Arrays.java");
	}

	@Test
	public void testAssignments() throws CoreException {
		testAllMethods("domain.expressions", "Assignments.java");
	}

	@Test
	public void testClassInstanceCreation() throws CoreException {
		testAllMethods("domain.expressions", "ClassInstanceCreation.java");
	}

	@Test
	public void testFieldAccess() throws CoreException {
		testAllMethods("domain.expressions", "FieldAccess.java");
	}

	@Test
	public void testIgnoredExpressions() throws CoreException {
		testAllMethods("domain.expressions", "IgnoredExpressions.java");
	}

	@Test
	public void testMethodInvocation() throws CoreException {
		testAllMethods("domain.expressions", "MethodInvocation.java");
	}

	@Test
	public void testInfixExpressions() throws CoreException {
		testAllMethods("domain.booleans.expressions", "InfixExpressions.java");
		testAllMethods("domain.intervals.expressions", "InfixExpressions.java");
	}

	@Test
	public void testPrefixExpressions() throws CoreException {
		testAllMethods("domain.booleans.expressions", "PrefixExpressions.java");
		testAllMethods("domain.intervals.expressions", "PrefixExpressions.java");
	}

	@Test
	public void testOperatorPrecedence() throws CoreException {
		testAllMethods("domain.booleans.expressions", "OperatorPrecedence.java");
		testAllMethods("domain.intervals.expressions", "OperatorPrecedence.java");
	}

	@Test
	public void testPostfixExpressions() throws CoreException {
		testAllMethods("domain.intervals.expressions", "PostfixExpressions.java");
	}

	@Test
	public void testPrefixPostfixUpdates() throws CoreException {
		testAllMethods("domain.intervals.expressions", "PrefixPostfixUpdates.java");
	}

	@Test
	public void testVarious() throws CoreException {
		testAllMethods("domain.commons", "Various.java");
	}

	@Test
	public void testSwitch() throws CoreException {
		testAllMethods("domain.commons", "BooleanSwitch.java");
		testAllMethods("domain.commons", "IntegerSwitch.java");
	}
	
	@Test
	public void testintervalBoxing() throws CoreException {
		testAllMethods("domain.booleans", "Boxing.java");
		testAllMethods("domain.intervals", "Boxing.java");
	}

	@Test
	public void testConstructorInvocation() throws CoreException {
		testAllMethods("domain.commons.constructor", "ConstructorInvocation01.java");
		testAllMethods("domain.commons.constructor", "ConstructorInvocation02.java");
		testAllMethods("domain.commons.constructor", "ConstructorInvocation03.java");
	}

	@Test
	public void testSuperConstructorInvocation() throws CoreException {
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation01.java");
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation02.java");
		testAllMethods("domain.commons.constructor", "SuperConstructorInvocation03.java");
	}

	@Test
	public void testConditionalExpression() throws CoreException {
		testAllMethods("domain.commons", "ConditionalExpression.java");
	}
}
