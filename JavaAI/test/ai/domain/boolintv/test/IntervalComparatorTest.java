package ai.domain.boolintv.test;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestProject.ProjectFile;
import ai.test.TestsBase;

@RunWith(JUnit4.class)
public class IntervalComparatorTest extends TestsBase {
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		log.info("Running test on: " + cfg.getCodeFragment().getUniqueName());
		testBooleansAndIntervals(cfg);
	}
	
//	protected Level getLogLevel(){
//		return Level.INFO;
//	}
//
	@Test
	public void testNumberAndNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "NumberAndNumber.java");

		performTest(pf.getCFG("NumberAndNumber", "numberGtNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberGtNumber02"));

		performTest(pf.getCFG("NumberAndNumber", "numberLtNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberLtNumber02"));

		performTest(pf.getCFG("NumberAndNumber", "numberGteNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberGteNumber02"));

		performTest(pf.getCFG("NumberAndNumber", "numberLteNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberLteNumber02"));

		performTest(pf.getCFG("NumberAndNumber", "numberEqNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberEqNumber02"));

		performTest(pf.getCFG("NumberAndNumber", "numberNeqNumber01"));
		performTest(pf.getCFG("NumberAndNumber", "numberNeqNumber02"));
	}
	
	@Test
	public void testVariableAndNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndNumber.java");

		performTest(pf.getCFG("VariableAndNumber", "variableGtNumber01"));
		performTest(pf.getCFG("VariableAndNumber", "variableLtNumber01"));
		performTest(pf.getCFG("VariableAndNumber", "variableGteNumber01"));
		performTest(pf.getCFG("VariableAndNumber", "variableLteNumber01"));
		performTest(pf.getCFG("VariableAndNumber", "variableEqNumber01"));
		performTest(pf.getCFG("VariableAndNumber", "variableNeqNumber01"));
	}

	@Test
	public void testVariableWithMinusOrPlusAndNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "UnaryOpVariableAndNumber.java");

		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableGtNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableGtNumber02"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableLtNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableLtNumber02"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableGteNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableGteNumber02"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableLteNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableLteNumber02"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableEqNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableEqNumber02"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableNeqNumber01"));
		performTest(pf.getCFG("UnaryOpVariableAndNumber", "variableNeqNumber02"));
	}
	
	@Test
	public void testTwoVariables() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariable.java");

		performTest(pf.getCFG("VariableAndVariable", "varGtVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varGtVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varGtVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varGtVar04"));

		performTest(pf.getCFG("VariableAndVariable", "varLtVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varLtVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varLtVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varLtVar04"));

		performTest(pf.getCFG("VariableAndVariable", "varGteVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varGteVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varGteVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varGteVar04"));

		performTest(pf.getCFG("VariableAndVariable", "varLteVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varLteVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varLteVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varLteVar04"));
		
		performTest(pf.getCFG("VariableAndVariable", "varEqVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varEqVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varEqVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varEqVar04"));

		performTest(pf.getCFG("VariableAndVariable", "varNeqVar01"));
		performTest(pf.getCFG("VariableAndVariable", "varNeqVar02"));
		performTest(pf.getCFG("VariableAndVariable", "varNeqVar03"));
		performTest(pf.getCFG("VariableAndVariable", "varNeqVar04"));
	}

	@Test
	public void testVariableAndVariablePlusNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariablePlusNumber.java");

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGtVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGtVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGtVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGtVarPlusNum04"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGtVarPlusNum05"));

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLtVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLtVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLtVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLtVarPlusNum04"));

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGteVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGteVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGteVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varGteVarPlusNum04"));

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLteVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLteVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLteVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varLteVarPlusNum04"));

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varEqVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varEqVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varEqVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varEqVarPlusNum04"));

		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varNeqVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varNeqVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varNeqVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusNumber", "varNeqVarPlusNum04"));
	}

	@Test
	public void testVariableAndVariablePlusMultipleNumbers() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariablePlusMultipleNumbers.java");

		performTest(pf.getCFG("VariableAndVariablePlusMultipleNumbers", "varGtVarPlusNum01"));
		performTest(pf.getCFG("VariableAndVariablePlusMultipleNumbers", "varGtVarPlusNum02"));
		performTest(pf.getCFG("VariableAndVariablePlusMultipleNumbers", "varGtVarPlusNum03"));
		performTest(pf.getCFG("VariableAndVariablePlusMultipleNumbers", "varGtVarPlusNum04"));
	}
	
	@Test
	public void testVariableAndVariableMinusNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariableMinusNumber.java");

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGtVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGtVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGtVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGtVarMinusNum04"));

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLtVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLtVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLtVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLtVarMinusNum04"));

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGteVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGteVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGteVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varGteVarMinusNum04"));

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLteVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLteVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLteVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varLteVarMinusNum04"));

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varEqVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varEqVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varEqVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varEqVarMinusNum04"));

		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varNeqVarMinusNum01"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varNeqVarMinusNum02"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varNeqVarMinusNum03"));
		performTest(pf.getCFG("VariableAndVariableMinusNumber", "varNeqVarMinusNum04"));
	}
	
	@Test
	public void testVariableAndVariableTimesNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariableTimesNumber.java");

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGtVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGtVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGtVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGtVarTimesNum04"));

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLtVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLtVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLtVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLtVarTimesNum04"));

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGteVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGteVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGteVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varGteVarTimesNum04"));

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLteVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLteVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLteVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varLteVarTimesNum04"));

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varEqVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varEqVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varEqVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varEqVarTimesNum04"));

		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varNeqVarTimesNum01"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varNeqVarTimesNum02"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varNeqVarTimesNum03"));
		performTest(pf.getCFG("VariableAndVariableTimesNumber", "varNeqVarTimesNum04"));
	}

	@Test
	public void testVariableAndVariableDivideNumber() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableAndVariableDivideNumber.java");

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGtVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGtVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGtVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGtVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGtVarDivideNum05"));

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLtVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLtVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLtVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLtVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLtVarDivideNum05"));

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGteVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGteVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGteVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGteVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varGteVarDivideNum05"));

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLteVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLteVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLteVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLteVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varLteVarDivideNum05"));

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varEqVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varEqVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varEqVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varEqVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varEqVarDivideNum05"));

		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varNeqVarDivideNum01"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varNeqVarDivideNum02"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varNeqVarDivideNum03"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varNeqVarDivideNum04"));
		performTest(pf.getCFG("VariableAndVariableDivideNumber", "varNeqVarDivideNum05"));
	}

	@Test
	public void testVariableAndVariableLinearInequalities() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "LinearInequalities.java");

		performTest(pf.getCFG("LinearInequalities", "plusWithMultipleVariables01"));
		performTest(pf.getCFG("LinearInequalities", "eqWithSameVariableOnBothSides01"));
		performTest(pf.getCFG("LinearInequalities", "eqWithSameVariableOnBothSides02"));
		performTest(pf.getCFG("LinearInequalities", "eqWithSameVariableOnBothSides03"));
		performTest(pf.getCFG("LinearInequalities", "eqWithSameVariableOnBothSides04"));
		performTest(pf.getCFG("LinearInequalities", "leWithSameVariableOnBothSides01"));
	}

	@Test
	public void testVariableModified() throws CoreException {
		ProjectFile pf = project.openFile("domain.boolintv.expressions.numericcomparison", "VariableModified.java");

		performTest(pf.getCFG("VariableModified", "postfixInc01"));
		performTest(pf.getCFG("VariableModified", "postfixInc02"));
		performTest(pf.getCFG("VariableModified", "postfixInc03"));
		performTest(pf.getCFG("VariableModified", "postfixInc04"));

		performTest(pf.getCFG("VariableModified", "prefixInc01"));
		performTest(pf.getCFG("VariableModified", "prefixInc02"));
		performTest(pf.getCFG("VariableModified", "prefixInc03"));
		performTest(pf.getCFG("VariableModified", "prefixInc04"));

		performTest(pf.getCFG("VariableModified", "assignment01"));
		performTest(pf.getCFG("VariableModified", "assignment02"));
		performTest(pf.getCFG("VariableModified", "assignment03"));
		performTest(pf.getCFG("VariableModified", "assignment04"));
		performTest(pf.getCFG("VariableModified", "assignment05"));
		performTest(pf.getCFG("VariableModified", "assignment06"));
		performTest(pf.getCFG("VariableModified", "assignment07"));
		performTest(pf.getCFG("VariableModified", "assignment08"));
		performTest(pf.getCFG("VariableModified", "assignment09"));
	}
}
