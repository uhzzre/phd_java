package ai.domain.interval.test;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.interval.Interval;
import ai.domain.interval.InvalidIntervalValueException;

@RunWith(JUnit4.class)
public class DomainTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testCreateInterval() {
		Interval a = new Interval(1,2);
		org.junit.Assert.assertEquals(a.getLeft(), 1, 0); 
		org.junit.Assert.assertEquals(a.getRight(), 2, 0); 

		Interval b = new Interval(1,1);
		org.junit.Assert.assertEquals(b.getLeft(), 1, 0); 
		org.junit.Assert.assertEquals(b.getRight(), 1, 0); 
		
		Interval leftIntf = new Interval(Double.NEGATIVE_INFINITY, 2);
		org.junit.Assert.assertEquals(leftIntf.getLeft(), Double.NEGATIVE_INFINITY, 0); 
		org.junit.Assert.assertEquals(leftIntf.getRight(), 2, 0); 

		Interval rightIntf = new Interval(2, Double.POSITIVE_INFINITY);
		org.junit.Assert.assertEquals(rightIntf.getLeft(), 2, 0); 
		org.junit.Assert.assertEquals(rightIntf.getRight(), Double.POSITIVE_INFINITY, 0); 
		
		Interval top = new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		org.junit.Assert.assertEquals(top.getLeft(), Double.NEGATIVE_INFINITY, 0); 
		org.junit.Assert.assertEquals(top.getRight(), Double.POSITIVE_INFINITY, 0); 
	}

	@Test
	public void testCreateInvalidInterval01(){
		thrown.expect(InvalidIntervalValueException.class);
		new Interval(2,1);
	}

	@Test
	public void testCreateInvalidInterval02(){
		thrown.expect(InvalidIntervalValueException.class);
		new Interval(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
	}

	@Test
	public void testCreateInvalidInterval03(){
		thrown.expect(InvalidIntervalValueException.class);
		new Interval(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
	}

	@Test
	public void testJoin() {
		Interval join = new Interval(1,2).join(new Interval(4,6));
		org.junit.Assert.assertEquals(join.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(join.getRight(), 6, 0);
		
		join = new Interval(4,6).join(new Interval(1,2));
		org.junit.Assert.assertEquals(join.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(join.getRight(), 6, 0);
		
		join = new Interval(1,2).join(Interval.BOTTOM);
		org.junit.Assert.assertEquals(join.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(join.getRight(), 2, 0);

		join = Interval.BOTTOM.join(new Interval(1,2));
		org.junit.Assert.assertEquals(join.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(join.getRight(), 2, 0);
	
		join = Interval.BOTTOM.join(Interval.BOTTOM);
		org.junit.Assert.assertEquals(join, Interval.BOTTOM);

		join = new Interval(1,2).join(new Interval(Double.NEGATIVE_INFINITY, 10));
		org.junit.Assert.assertEquals(join.getLeft(), Double.NEGATIVE_INFINITY, 0);
		org.junit.Assert.assertEquals(join.getRight(), 10, 0);

		join = new Interval(1,2).join(new Interval(10, Double.POSITIVE_INFINITY));
		org.junit.Assert.assertEquals(join.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(join.getRight(), Double.POSITIVE_INFINITY, 0);
	}

	@Test
	public void testMeet() {
		Interval meet = new Interval(1,2).meet(new Interval(2, 5));
		org.junit.Assert.assertEquals(meet.getLeft(), 2, 0);
		org.junit.Assert.assertEquals(meet.getRight(), 2, 0);

		meet = new Interval(1,2).meet(new Interval(Double.NEGATIVE_INFINITY, 5));
		org.junit.Assert.assertEquals(meet.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(meet.getRight(), 2, 0);

		meet = new Interval(1,2).meet(new Interval(3, 5));
		org.junit.Assert.assertEquals(meet, Interval.BOTTOM);
		
		//bottom
		meet = Interval.BOTTOM.meet(Interval.BOTTOM);
		org.junit.Assert.assertEquals(meet, Interval.BOTTOM);

		meet = Interval.BOTTOM.meet(new Interval(1,10));
		org.junit.Assert.assertEquals(meet, Interval.BOTTOM);

		meet = Interval.BOTTOM.meet(new Interval(Double.NEGATIVE_INFINITY,10));
		org.junit.Assert.assertEquals(meet, Interval.BOTTOM);

		meet = Interval.BOTTOM.meet(new Interval(Double.NEGATIVE_INFINITY,Double.POSITIVE_INFINITY));
		org.junit.Assert.assertEquals(meet, Interval.BOTTOM);
	}

	@Test
	public void testWiden() {
		Interval widen = new Interval(1,2).widen(new Interval(1,2));
		org.junit.Assert.assertEquals(widen.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(widen.getRight(), 2, 0);

		widen = new Interval(1,5).widen(new Interval(1,3));
		org.junit.Assert.assertEquals(widen.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(widen.getRight(), 5, 0);

		widen = new Interval(0,5).widen(new Interval(1,3));
		org.junit.Assert.assertEquals(widen.getLeft(), 0, 0);
		org.junit.Assert.assertEquals(widen.getRight(), 5, 0);

		widen = Interval.BOTTOM.widen(new Interval(1,3));
		org.junit.Assert.assertEquals(widen.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(widen.getRight(), 3, 0);

		widen = new Interval(1,3).widen(new Interval(1,5));
		org.junit.Assert.assertEquals(widen.getLeft(), 1, 0);
		org.junit.Assert.assertEquals(widen.getRight(), Double.POSITIVE_INFINITY, 0);

		widen = new Interval(1,3).widen(new Interval(0,2));
		org.junit.Assert.assertEquals(widen.getLeft(), Double.NEGATIVE_INFINITY, 0);
		org.junit.Assert.assertEquals(widen.getRight(), 3, 0);

		widen = new Interval(1,3).widen(new Interval(0,5));
		org.junit.Assert.assertEquals(widen.getLeft(), Double.NEGATIVE_INFINITY, 0);
		org.junit.Assert.assertEquals(widen.getRight(), Double.POSITIVE_INFINITY, 0);
	}

}
