package ai.domain.interval.test;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.interval.Interval;
import ai.domain.interval.IntervalExpressionSemantics;

@RunWith(JUnit4.class)
public class ExpressionSemanticsTest {
	Interval TOP = new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);

	@Test
	public void testUnaryMinus() {
		IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM));

		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY),
				sem.minus(new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 0),
				sem.minus(new Interval(0, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, -5),
				sem.minus(new Interval(5, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 3),
				sem.minus(new Interval(-3, Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(0, Double.POSITIVE_INFINITY),
				sem.minus(new Interval(Double.NEGATIVE_INFINITY, 0)));
		org.junit.Assert.assertEquals(new Interval(-5, Double.POSITIVE_INFINITY),
				sem.minus(new Interval(Double.NEGATIVE_INFINITY, 5)));
		org.junit.Assert.assertEquals(new Interval(3, Double.POSITIVE_INFINITY),
				sem.minus(new Interval(Double.NEGATIVE_INFINITY, -3)));

		org.junit.Assert.assertEquals(new Interval(0, 0), sem.minus(new Interval(0, 0)));
		org.junit.Assert.assertEquals(new Interval(-2, 0), sem.minus(new Interval(0, 2)));
		org.junit.Assert.assertEquals(new Interval(0, 2), sem.minus(new Interval(-2, 0)));
		org.junit.Assert.assertEquals(new Interval(1, 2), sem.minus(new Interval(-2, -1)));
		org.junit.Assert.assertEquals(new Interval(-2, -1), sem.minus(new Interval(1, 2)));
		org.junit.Assert.assertEquals(new Interval(-5, 3), sem.minus(new Interval(-3, 5)));
	}

	@Test
	public void testPlus(){
		IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
		//bottom
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(Interval.BOTTOM, Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(Interval.BOTTOM, new Interval(-1,2)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(Interval.BOTTOM, new Interval(1, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, 1)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(new Interval(-1,2), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(new Interval(1, Double.POSITIVE_INFINITY), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(new Interval(Double.NEGATIVE_INFINITY, 1), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.plus(new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY), Interval.BOTTOM));
		
		org.junit.Assert.assertEquals(TOP, sem.plus(TOP, TOP));
		org.junit.Assert.assertEquals(TOP, sem.plus(TOP, new Interval(1,2)));
		org.junit.Assert.assertEquals(TOP, sem.plus(TOP, new Interval(1,Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(TOP, sem.plus(TOP, new Interval(Double.NEGATIVE_INFINITY, 3)));
		org.junit.Assert.assertEquals(TOP, sem.plus(new Interval(Double.NEGATIVE_INFINITY, -1), new Interval(1,Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(TOP, sem.plus(new Interval(Double.NEGATIVE_INFINITY, 1), new Interval(-1,Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 4), sem.plus(new Interval(Double.NEGATIVE_INFINITY, -1), new Interval(3,5)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 4), sem.plus(new Interval(3,5), new Interval(Double.NEGATIVE_INFINITY, -1)));
		org.junit.Assert.assertEquals(new Interval(-97, Double.POSITIVE_INFINITY), sem.plus(new Interval(3,Double.POSITIVE_INFINITY), new Interval(-100, -1)));
		org.junit.Assert.assertEquals(new Interval(-97, Double.POSITIVE_INFINITY), sem.plus(new Interval(-100, -1), new Interval(3,Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(-97, 4), sem.plus(new Interval(-100, -1), new Interval(3,5)));
		org.junit.Assert.assertEquals(new Interval(-97, 4), sem.plus(new Interval(3,5), new Interval(-100, -1)));
	}

	@Test
	public void testMinus(){
		IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
		//bottom
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM, Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM, new Interval(-1,2)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM, new Interval(1, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, 1)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(new Interval(-1,2), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(new Interval(1, Double.POSITIVE_INFINITY), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(new Interval(Double.NEGATIVE_INFINITY, 1), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.minus(new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY), Interval.BOTTOM));
		
		Interval top = new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		
		org.junit.Assert.assertEquals(top, sem.minus(top, top));
		org.junit.Assert.assertEquals(top, sem.minus(top, new Interval(1,2)));
		org.junit.Assert.assertEquals(top, sem.minus(top, new Interval(1,Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(top, sem.minus(top, new Interval(Double.NEGATIVE_INFINITY, 3)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, -2), sem.minus(new Interval(Double.NEGATIVE_INFINITY, -1), new Interval(1,Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 2), sem.minus(new Interval(Double.NEGATIVE_INFINITY, 1), new Interval(-1,Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, -4), sem.minus(new Interval(Double.NEGATIVE_INFINITY, -1), new Interval(3,5)));
		org.junit.Assert.assertEquals(new Interval(4, Double.POSITIVE_INFINITY), sem.minus(new Interval(3,5), new Interval(Double.NEGATIVE_INFINITY, -1)));
		org.junit.Assert.assertEquals(new Interval(4, Double.POSITIVE_INFINITY), sem.minus(new Interval(3,Double.POSITIVE_INFINITY), new Interval(-100, -1)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY,-4), sem.minus(new Interval(-100, -1), new Interval(3,Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(-105, -4), sem.minus(new Interval(-100, -1), new Interval(3,5)));
		org.junit.Assert.assertEquals(new Interval(4, 105), sem.minus(new Interval(3,5), new Interval(-100, -1)));
	}

	@Test
	public void testMultiply(){
		IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
		//bottom
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, new Interval(-1,2)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, new Interval(1, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, 1)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, new Interval(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(new Interval(-1,2), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(new Interval(1, Double.POSITIVE_INFINITY), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(new Interval(Double.NEGATIVE_INFINITY, 1), Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(TOP, Interval.BOTTOM));
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.times(Interval.BOTTOM, TOP));
		
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, TOP));
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, new Interval(1,2)));
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, new Interval(1,Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, new Interval(Double.NEGATIVE_INFINITY, 3)));
		org.junit.Assert.assertEquals(TOP, sem.times(new Interval(3, Double.POSITIVE_INFINITY), new Interval(Double.NEGATIVE_INFINITY, 3)));
		org.junit.Assert.assertEquals(TOP, sem.times(new Interval(-3, Double.POSITIVE_INFINITY), new Interval(Double.NEGATIVE_INFINITY, -3)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, -9), sem.times(new Interval(3, Double.POSITIVE_INFINITY), new Interval(Double.NEGATIVE_INFINITY, -3)));
		org.junit.Assert.assertEquals(new Interval(0,0), sem.times(TOP, new Interval(0,0)));
		org.junit.Assert.assertEquals(new Interval(0,0), sem.times(new Interval(0,0), TOP));
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, new Interval(0,1)));
		org.junit.Assert.assertEquals(TOP, sem.times(TOP, new Interval(-1,0)));

		org.junit.Assert.assertEquals(new Interval(0, Double.POSITIVE_INFINITY), sem.times(new Interval(0, Double.POSITIVE_INFINITY), new Interval(0,1)));
		org.junit.Assert.assertEquals(new Interval(0, Double.POSITIVE_INFINITY), sem.times(new Interval(0,1), new Interval(0, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(new Interval(-5, Double.POSITIVE_INFINITY), sem.times(new Interval(0,1), new Interval(-5, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(new Interval(-35, Double.POSITIVE_INFINITY), sem.times(new Interval(0,7), new Interval(-5, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertEquals(TOP, sem.times(new Interval(-1,7), new Interval(-5, Double.POSITIVE_INFINITY)));

		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 0), sem.times(new Interval(Double.NEGATIVE_INFINITY, 0), new Interval(0,1)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 0), sem.times(new Interval(0,1), new Interval(Double.NEGATIVE_INFINITY, 0)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 5), sem.times(new Interval(0,1), new Interval(Double.NEGATIVE_INFINITY, 5)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, 35), sem.times(new Interval(0,7), new Interval(Double.NEGATIVE_INFINITY, 5)));
		org.junit.Assert.assertEquals(TOP, sem.times(new Interval(-1,7), new Interval(Double.NEGATIVE_INFINITY, 5)));

		org.junit.Assert.assertEquals(new Interval(-500, -3), sem.times(new Interval(-100, -1), new Interval(3,5)));
		org.junit.Assert.assertEquals(new Interval(-500, -3), sem.times(new Interval(3,5), new Interval(-100, -1)));
		org.junit.Assert.assertEquals(new Interval(-500, 5), sem.times(new Interval(3,5), new Interval(-100, 1)));
		org.junit.Assert.assertEquals(new Interval(-500, 5), sem.times(new Interval(-100, 1), new Interval(3,5)));
	}

	@Test
	public void testDivide(){
		IntervalExpressionSemantics sem = new IntervalExpressionSemantics();
		//bottom
		for(Interval val: Arrays.asList(Interval.BOTTOM, new Interval(1,2), new Interval(1,Double.POSITIVE_INFINITY),
				new Interval(Double.NEGATIVE_INFINITY, 3), TOP)){
			org.junit.Assert.assertEquals(Interval.BOTTOM, sem.divide(val, Interval.BOTTOM));
			org.junit.Assert.assertEquals(Interval.BOTTOM, sem.divide(Interval.BOTTOM, val));
		}
		//others
		org.junit.Assert.assertEquals(Interval.BOTTOM, sem.divide(new Interval(1,1), new Interval(0,0)));
		org.junit.Assert.assertEquals(new Interval(1, Double.POSITIVE_INFINITY), sem.divide(new Interval(1,1), new Interval(0,1)));
		org.junit.Assert.assertEquals(new Interval(Double.NEGATIVE_INFINITY, -1), sem.divide(new Interval(1,1), new Interval(-1, 0)));
		org.junit.Assert.assertEquals(new Interval(0, 0), sem.divide(new Interval(1,1), new Interval(4, 100)));
		org.junit.Assert.assertEquals(new Interval(0, 0), sem.divide(new Interval(1,1), new Interval(-100, -4)));
		org.junit.Assert.assertEquals(Interval.TOP, sem.divide(new Interval(1,1), new Interval(-100, 100)));
		org.junit.Assert.assertEquals(new Interval(0, Double.POSITIVE_INFINITY), sem.divide(new Interval(4,Double.POSITIVE_INFINITY), new Interval(2,6)));
		org.junit.Assert.assertEquals(new Interval(1, Double.POSITIVE_INFINITY), sem.divide(new Interval(4,Double.POSITIVE_INFINITY), new Interval(2,4)));
		org.junit.Assert.assertEquals(new Interval(2, Double.POSITIVE_INFINITY), sem.divide(new Interval(4,Double.POSITIVE_INFINITY), new Interval(2,2)));
		org.junit.Assert.assertEquals(new Interval(1, 10), sem.divide(new Interval(9,21), new Interval(2,5)));
		org.junit.Assert.assertEquals(new Interval(-10, -1), sem.divide(new Interval(9,21), new Interval(-5,-2)));
	}

}
