package ai.domain.boxes.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.Variable;
import ai.domain.boxes.BoxesWideningOperator;
import ai.domain.boxes.BoxesWideningOperator.WideningStepThresholds;
import ai.domain.boxes.IntegerBoxes;
import ai.domain.boxes.IntegerBoxesHelper;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.interval.Interval;
import ai.domain.interval.IntervalValue;
import ai.test.DummyVariable;

@RunWith(JUnit4.class)
public class BoxesDomainWideningTests {
	private NonRelationalDomain<Interval> prepareBox(IntervalValue... data) {
		int idx = 0;
		NonRelationalDomain<Interval> res = NonRelationalDomain.getInitialValue();
		for(int i=data.length-1; i>=0; i--) {
			IntervalValue val = data[i];
			res = res.addNewVariable(new DummyVariable("TestVariable " + idx, idx++), new Interval(val), false);
		}
		return res;
	}

	private IntegerBoxes getSingleBoxNoVerify(IntervalValue... data) {
		NonRelationalDomain<Interval> input = prepareBox(data);
		return IntegerBoxesHelper.fromIntervalBox(input);
	}
	
	private IntegerBoxes getSingleBoxAndVerify(IntervalValue... data) {
		NonRelationalDomain<Interval> input = prepareBox(data);
		IntegerBoxes i = IntegerBoxesHelper.fromIntervalBox(input);

		List<NonRelationalDomain<Interval>> output = IntegerBoxesHelper.split(i);
		org.junit.Assert.assertEquals(output.size(), 1);
		org.junit.Assert.assertEquals(output.get(0), input);
		return i;
	}
	
	private IntegerBoxes oneDimensionalBox(IntervalValue... data) {
		IntegerBoxes result = IntegerBoxes.BOTTOM;
		for(IntervalValue val: data)
			result = result.join(IntegerBoxesHelper.fromIntervalBox(prepareBox(val)));
		return result;
	}
	
	@Test
	public void testOneDimensionNoThresholds01() {
		IntegerBoxes left = oneDimensionalBox(new IntervalValue(1, 2), new IntervalValue(4, 5));
		IntegerBoxes right = oneDimensionalBox(new IntervalValue(4, 6));
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right);
		org.junit.Assert.assertEquals(oneDimensionalBox(new IntervalValue(1, 2), new IntervalValue(4, Double.POSITIVE_INFINITY)), wid);
	}

	@Test
	public void testOneDimensionNoThresholds02() {
		IntegerBoxes left = oneDimensionalBox(new IntervalValue(1, 2), new IntervalValue(4, 5));
		IntegerBoxes right = oneDimensionalBox(new IntervalValue(4, 6), new IntervalValue(-2, -1));
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right);
		org.junit.Assert.assertEquals(oneDimensionalBox(new IntervalValue(Double.NEGATIVE_INFINITY, 2), new IntervalValue(4, Double.POSITIVE_INFINITY)), wid);
	}

	@Test
	public void testOneDimensionWithThresholds01() {
		IntegerBoxes left = oneDimensionalBox(new IntervalValue(1, 2));
		IntegerBoxes right = oneDimensionalBox(new IntervalValue(4, 6));
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right, Arrays.asList(new Double(-2), new Double(8), new Double(120)));
		org.junit.Assert.assertEquals(oneDimensionalBox(new IntervalValue(1, 7)), wid);
	}

	@Test
	public void testTwoDimension01() {
		IntegerBoxes left = getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(0, 1)).join(
				getSingleBoxAndVerify(new IntervalValue(3, 4), new IntervalValue(3, 4)));
		IntegerBoxes right = getSingleBoxAndVerify(new IntervalValue(0, 2), new IntervalValue(0, 2));
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right);
		org.junit.Assert.assertEquals(
				getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(0, Double.POSITIVE_INFINITY)).join(
				getSingleBoxAndVerify(new IntervalValue(2,2), new IntervalValue(0,2))).join(
				getSingleBoxAndVerify(new IntervalValue(3,4), new IntervalValue(3,4))), wid);
	}

	@Test
	public void testTwoDimensionWithThresholds01() {
		IntegerBoxes left = getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(0, 1)).join(
				getSingleBoxAndVerify(new IntervalValue(3, 4), new IntervalValue(3, 4)));
		IntegerBoxes right = getSingleBoxAndVerify(new IntervalValue(0, 2), new IntervalValue(0, 2));
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right, Arrays.asList(new Double(7)));
		org.junit.Assert.assertEquals(
				getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(0, 6)).join(
				getSingleBoxAndVerify(new IntervalValue(2,2), new IntervalValue(0,2))).join(
				getSingleBoxAndVerify(new IntervalValue(3,4), new IntervalValue(3,4))), wid);
	}

	@Test
	public void testTwoDimensionWithThresholds02() {
		IntegerBoxes left = getSingleBoxAndVerify(new IntervalValue(0,10), new IntervalValue(0, 1));
		IntegerBoxes right = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 5)),
				getSingleBoxAndVerify(new IntervalValue(3,6), new IntervalValue(0, 1)),
				getSingleBoxAndVerify(new IntervalValue(7,8), new IntervalValue(0,2)),
				getSingleBoxAndVerify(new IntervalValue(9,10), new IntervalValue(0,1)));
		final ArrayList<Variable> variables = left.getVariables();
		IntegerBoxes wid = BoxesWideningOperator.computeWidening(left, right,
				new WideningStepThresholds() {
					@Override
					public Set<Double> getThresholds(Variable var) {
						List<Double> thresholds;
						if (var == variables.get(0))
							thresholds = Arrays.asList(2.0, 4.0, 6.0, 8.0, 10.0);
						else
							thresholds = Arrays.asList(5.0);
						return new HashSet<Double>(thresholds);
					}
				});
		IntegerBoxes result = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, Double.POSITIVE_INFINITY)),
				getSingleBoxAndVerify(new IntervalValue(3,3), new IntervalValue(0, 4)),
				getSingleBoxAndVerify(new IntervalValue(4,5), new IntervalValue(0,1)),
				getSingleBoxAndVerify(new IntervalValue(6,9), new IntervalValue(0,4)),
				getSingleBoxAndVerify(new IntervalValue(9,10), new IntervalValue(0,1))
				);
		org.junit.Assert.assertEquals(result, wid);
	}

	@Test
	public void testStrange0() {
		IntegerBoxes left = getSingleBoxAndVerify(
				new IntervalValue(1, Double.POSITIVE_INFINITY),
				new IntervalValue(2, Double.POSITIVE_INFINITY),
				new IntervalValue(2, Double.POSITIVE_INFINITY),
				new IntervalValue(2, 2),
				new IntervalValue(-1, -1)
				);
		IntegerBoxes right = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(2, 2),
						new IntervalValue(-1, -1),
						new IntervalValue(1, 1)
						),
				getSingleBoxAndVerify(
				new IntervalValue(1, Double.POSITIVE_INFINITY),
				new IntervalValue(2, Double.POSITIVE_INFINITY),
				new IntervalValue(2, Double.POSITIVE_INFINITY),
				new IntervalValue(2, 2),
				new IntervalValue(-1, -1)
				),
				getSingleBoxAndVerify(
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(2, 2),
						new IntervalValue(-0.0, -0.0)
						)
				);

		IntegerBoxes result = left.widen(right);
		org.junit.Assert.assertTrue(left.leq(result));
		org.junit.Assert.assertTrue(right.leq(result));
	}

	@Test
	public void testStrange1() {
		IntegerBoxes left = IntegerBoxesHelper.join(
				getSingleBoxNoVerify(
						new IntervalValue(0, 0),
						new IntervalValue(0, 0),
						new IntervalValue(Long.MAX_VALUE, Long.MAX_VALUE)),
				getSingleBoxNoVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(0, Long.MAX_VALUE),
						new IntervalValue(Double.NEGATIVE_INFINITY, Long.MAX_VALUE)),
				getSingleBoxNoVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(Long.MAX_VALUE, Double.POSITIVE_INFINITY),
						new IntervalValue(Long.MAX_VALUE, Long.MAX_VALUE))
				);
		IntegerBoxes right = IntegerBoxesHelper.join(
				getSingleBoxNoVerify(
						new IntervalValue(0, 0),
						new IntervalValue(0, 0),
						new IntervalValue(Long.MAX_VALUE, Long.MAX_VALUE)),
				getSingleBoxNoVerify(
						new IntervalValue(1, 1),
						new IntervalValue(0, Long.MAX_VALUE),
						new IntervalValue(Double.NEGATIVE_INFINITY, Long.MAX_VALUE)),
				getSingleBoxNoVerify(
						new IntervalValue(1, 1),
						new IntervalValue(Long.MAX_VALUE, Double.POSITIVE_INFINITY),
						new IntervalValue(Long.MAX_VALUE, Long.MAX_VALUE)
						),
				getSingleBoxNoVerify(
						new IntervalValue(2, 5),
						new IntervalValue(0, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, Long.MAX_VALUE)
						));
		left.widen(right);
	}
}
