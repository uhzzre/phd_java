package ai.domain.boxes.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.domain.boxes.IntegerBoxes;
import ai.domain.boxes.IntegerBoxesHelper;
import ai.domain.generic.NonRelationalDomain;
import ai.domain.interval.Interval;
import ai.domain.interval.IntervalValue;
import ai.test.DummyVariable;

@RunWith(JUnit4.class)
public class BoxesDomainTests {
	private NonRelationalDomain<Interval> prepareBox(IntervalValue... data) {
		int idx = 0;
		NonRelationalDomain<Interval> res = NonRelationalDomain.getInitialValue();
		for (IntervalValue val : data) {
			res = res.addNewVariable(new DummyVariable("TestVariable " + idx, idx++), new Interval(val), false);
		}
		return res;
	}

	private IntegerBoxes getSingleBoxAndVerify(IntervalValue... data) {
		NonRelationalDomain<Interval> input = prepareBox(data);
		IntegerBoxes i = IntegerBoxesHelper.fromIntervalBox(input);

		List<NonRelationalDomain<Interval>> output = IntegerBoxesHelper.split(i);
		org.junit.Assert.assertEquals(output.size(), 1);
		org.junit.Assert.assertEquals(output.get(0), input);
		return i;
	}

	@Test
	public void createSingleBoxFromIntv() {
		// 0 dimensions
		getSingleBoxAndVerify();
		// 1 dimension
		getSingleBoxAndVerify(new IntervalValue(1, 2));
		getSingleBoxAndVerify(new IntervalValue(1, 1));
		getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, 2));
		getSingleBoxAndVerify(new IntervalValue(2, Double.POSITIVE_INFINITY));
		getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY));
		// 2 dimensions
		getSingleBoxAndVerify(new IntervalValue(1, 2), new IntervalValue(3, 4));
		getSingleBoxAndVerify(new IntervalValue(0, 3), new IntervalValue(3, 4));
		getSingleBoxAndVerify(new IntervalValue(0, Double.POSITIVE_INFINITY), new IntervalValue(
				Double.NEGATIVE_INFINITY, 4));
		// 3 dimensions
		getSingleBoxAndVerify(new IntervalValue(1, 2), new IntervalValue(3, 4), new IntervalValue(4, 5));
		getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, 17), new IntervalValue(3, 4),
				new IntervalValue(4, Double.POSITIVE_INFINITY));
	}

	private void verifyResult(IntegerBoxes result, NonRelationalDomain<Interval>... expectedDisjuncts) {
		if (result.isBottom()) {
			org.junit.Assert.assertEquals(0, expectedDisjuncts.length);
			return;
		}
		List<NonRelationalDomain<Interval>> actualDisjuncts = IntegerBoxesHelper.split(result);
		org.junit.Assert.assertEquals(expectedDisjuncts.length, actualDisjuncts.size());
		int i = 0;
		for (NonRelationalDomain<Interval> actualDisjunct : actualDisjuncts) {
			NonRelationalDomain<Interval> expectedDisjunc = expectedDisjuncts[i++];
			org.junit.Assert.assertEquals(expectedDisjunc, actualDisjunct);
		}
	}

	@Test
	public void joinOneDimension() {
		// 1 dimension
		IntegerBoxes first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		IntegerBoxes second = getSingleBoxAndVerify(new IntervalValue(2, 4));
		verifyResult(first.join(second), prepareBox(new IntervalValue(1, 4)));
		verifyResult(second.join(first), prepareBox(new IntervalValue(1, 4)));

		first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		second = getSingleBoxAndVerify(new IntervalValue(3, 4));
		verifyResult(first.join(second), prepareBox(new IntervalValue(1, 4)));
		verifyResult(second.join(first), prepareBox(new IntervalValue(1, 4)));

		first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		second = getSingleBoxAndVerify(new IntervalValue(4, 5));
		verifyResult(first.join(second), prepareBox(new IntervalValue(1, 2)), prepareBox(new IntervalValue(4, 5)));
		verifyResult(second.join(first), prepareBox(new IntervalValue(1, 2)), prepareBox(new IntervalValue(4, 5)));

		first = first.join(second);
		second = getSingleBoxAndVerify(new IntervalValue(3, Double.POSITIVE_INFINITY));
		verifyResult(first.join(second), prepareBox(new IntervalValue(1, Double.POSITIVE_INFINITY)));
		verifyResult(second.join(first), prepareBox(new IntervalValue(1, Double.POSITIVE_INFINITY)));
	}

	@Test
	public void meetOneDimension() {
		// 1 dimension
		IntegerBoxes first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		IntegerBoxes second = getSingleBoxAndVerify(new IntervalValue(2, 4));
		verifyResult(first.meet(second), prepareBox(new IntervalValue(2, 2)));
		verifyResult(second.meet(first), prepareBox(new IntervalValue(2, 2)));

		first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		second = getSingleBoxAndVerify(new IntervalValue(3, 4));
		verifyResult(first.meet(second));
		verifyResult(second.meet(first));

		first = getSingleBoxAndVerify(new IntervalValue(1, 7));
		second = getSingleBoxAndVerify(new IntervalValue(4, 5));
		verifyResult(first.meet(second), prepareBox(new IntervalValue(4, 5)));
		verifyResult(second.meet(first), prepareBox(new IntervalValue(4, 5)));
	}

	@Test
	public void leqOneDimension() {
		// 1 dimension
		IntegerBoxes first = getSingleBoxAndVerify(new IntervalValue(1, 2));
		IntegerBoxes second = getSingleBoxAndVerify(new IntervalValue(2, 4));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertFalse(second.leq(first));

		first = getSingleBoxAndVerify(new IntervalValue(1, 7));
		second = getSingleBoxAndVerify(new IntervalValue(3, 4));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertTrue(second.leq(first));

		first = getSingleBoxAndVerify(new IntervalValue(1, 7));
		second = getSingleBoxAndVerify(new IntervalValue(4, 5));
		verifyResult(first.meet(second), prepareBox(new IntervalValue(4, 5)));
		verifyResult(second.meet(first), prepareBox(new IntervalValue(4, 5)));
	}

	@Test
	public void leqTwoDimensionsWithVarRemoval() {
		// 1 dimension
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 2)),
				getSingleBoxAndVerify(new IntervalValue(5,6), new IntervalValue(5, 6)));
		IntegerBoxes second = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0, 1)),
				getSingleBoxAndVerify(new IntervalValue(6, 6)));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertTrue(second.leq(first));
	}

	@Test
	public void leqTwoDimensionsWithoutVarRemoval01() {
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 2)),
				getSingleBoxAndVerify(new IntervalValue(5,6), new IntervalValue(5, 6)));
		IntegerBoxes second = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(1,2)),
				getSingleBoxAndVerify(new IntervalValue(6, 6), new IntervalValue(5,5)));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertTrue(second.leq(first));
	}

	@Test
	public void leqTwoDimensionsWithoutVarRemoval02() {
		// 1 dimension
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 2)),
				getSingleBoxAndVerify(new IntervalValue(5,6), new IntervalValue(5, 6)));
		IntegerBoxes second = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(1,3)),
				getSingleBoxAndVerify(new IntervalValue(6, 6), new IntervalValue(5,5)));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertFalse(second.leq(first));
	}

	@Test
	public void leqThreeDimensionsWithVarRemoval01() {
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 2), new IntervalValue(0,0)),
				getSingleBoxAndVerify(new IntervalValue(5,6), new IntervalValue(5, 6), new IntervalValue(3, 3)));
		IntegerBoxes second = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0, 1), new IntervalValue(1,2)),
				getSingleBoxAndVerify(new IntervalValue(6, 6), new IntervalValue(5,5)));
		org.junit.Assert.assertFalse(first.leq(second));
		org.junit.Assert.assertTrue(second.leq(first));
	}

	@Test
	public void testIsTop() {
		IntegerBoxes b = IntegerBoxes.BOTTOM;
		org.junit.Assert.assertFalse(b.isTop());
		b = getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY));
		org.junit.Assert.assertTrue(b.isTop());
		b = getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, 3));
		org.junit.Assert.assertFalse(b.isTop());
		
		b = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(0,2), new IntervalValue(0, 2), new IntervalValue(0,0)),
				getSingleBoxAndVerify(new IntervalValue(5,6), new IntervalValue(5, 6), new IntervalValue(3, 3)));
		org.junit.Assert.assertFalse(b.isTop());
		b = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)));
		org.junit.Assert.assertTrue(b.isTop());
	}
	
	@Test
	public void leqTest01() {
		// 1 dimension
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(0, 0), new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(1, Double.POSITIVE_INFINITY), 
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(1,1),
						new IntervalValue(1, Double.POSITIVE_INFINITY)));
		IntegerBoxes second = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(0, 0), new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(1, Double.POSITIVE_INFINITY), 
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(2, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(1,1),
						new IntervalValue(1, Double.POSITIVE_INFINITY)));
		System.out.println(first.leq(second));
		System.out.println(second.leq(first));
	}
	
	@Test
	public void leqTest02() {
		// 1 dimension
		IntegerBoxes first = IntegerBoxesHelper.join(
				getSingleBoxAndVerify(
						new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(0, 0), 
						new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY),
						new IntervalValue(1, Double.POSITIVE_INFINITY), 
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(0, 0)),
				getSingleBoxAndVerify(
						new IntervalValue(1, Double.POSITIVE_INFINITY),
						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
						new IntervalValue(1,1),
						new IntervalValue(1, Double.POSITIVE_INFINITY)));
		System.out.println(first);
//		IntegerBoxes second = IntegerBoxesHelper.join(
//				getSingleBoxAndVerify(
//						new IntervalValue(1, Double.POSITIVE_INFINITY),
//						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
//						new IntervalValue(0, 0), new IntervalValue(0, 0)),
//				getSingleBoxAndVerify(
//						new IntervalValue(1, Double.POSITIVE_INFINITY),
//						new IntervalValue(1, Double.POSITIVE_INFINITY), 
//						new IntervalValue(1, Double.POSITIVE_INFINITY),
//						new IntervalValue(0, 0)),
//				getSingleBoxAndVerify(
//						new IntervalValue(2, Double.POSITIVE_INFINITY),
//						new IntervalValue(Double.NEGATIVE_INFINITY, 0),
//						new IntervalValue(1,1),
//						new IntervalValue(1, Double.POSITIVE_INFINITY)));
//		System.out.println(first.leq(second));
//		System.out.println(second.leq(first));
	}
}
