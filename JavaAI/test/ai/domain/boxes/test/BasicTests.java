package ai.domain.boxes.test;

import org.apache.log4j.Level;
import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;
import ai.test.TestProject.ProjectFile;

@RunWith(JUnit4.class)
public class BasicTests extends TestsBase {

	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleansAndBoxes(cfg);
	}
	
	protected Level getLogLevel(){
		return Level.DEBUG;
	}
	

	@Test
	public void testBasic() throws CoreException {
		ProjectFile pf = project.openFile("domain.boxes", "Basics.java");
		performTest(pf.getCFG("Basics", "test01"));
	}
}
