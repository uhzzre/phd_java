package ai.interpreter.test;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.CoreException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import ai.cfg.MethodControlFlowGraph;
import ai.test.TestsBase;
import ai.test.TestProject.ProjectFile;

@RunWith(JUnit4.class)
public class InterpreterTests extends TestsBase {

	@Test
	public void testNewVariables() throws CoreException{
		BasicConfigurator.configure();
		Logger logger = Logger.getRootLogger();
		logger.setLevel(Level.DEBUG);
		ProjectFile pf = project.openFile("interpreter", "InterpreterTests.java");
		performTest(pf.getCFG("InterpreterTests", "test01")); // we evaluate join when all inputs are calculated
		performTest(pf.getCFG("InterpreterTests", "test02")); // switch
		performTest(pf.getCFG("InterpreterTests", "test03")); // switch
	}
	
	@Override
	protected void performTest(MethodControlFlowGraph cfg) {
		testBooleansAndIntervals(cfg);
	}

}
