package ai.cfg.test;

import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;
import org.junit.Before;
import org.junit.Test;

import ai.cfg.CFGVertice;
import ai.cfg.MethodControlFlowGraph;
import ai.cfg.Utils;
import ai.common.Pair;
import ai.test.TestProject;
import ai.test.TestProject.ProjectFile;
import ai.test.TestUtils;
import ai.test.comment.TestCommentData;

import com.google.common.collect.Multimap;

public class CFGTest {
	private TestProject project;

	@Test
	public void testGettingCompilationUnit() throws CoreException{
		//get file
		ProjectFile pf = project.openFile("", "CFGTests.java");
		IJavaElement icu = pf.getIcu();
		org.junit.Assert.assertEquals(icu.getElementName(), "CFGTests.java");
		org.junit.Assert.assertEquals(icu.getAncestor(ICompilationUnit.PACKAGE_FRAGMENT).getElementName(), "");
		
		pf = project.openFile("cfg", "MethodNames.java");
		icu = pf.getIcu();
		org.junit.Assert.assertEquals(icu.getElementName(), "MethodNames.java");
		org.junit.Assert.assertEquals(icu.getAncestor(ICompilationUnit.PACKAGE_FRAGMENT).getElementName(), "cfg");
	}

	@Test
	public void testEmptyMethod() throws CoreException{
		ProjectFile pf = project.openFile("cfg", "MethodNames.java");
		//top level methods
		MethodControlFlowGraph cfg = pf.getCFG("MethodNames", "topLevelMethod");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames:topLevelMethod()");

		cfg = pf.getCFG("MethodNames", "anotherTopLevelMethod");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames:anotherTopLevelMethod()");

		//inner class methods
		cfg = pf.getCFG("MethodNames$InnerClass", "innerClassMethod");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames$InnerClass:innerClassMethod()");
		
		cfg = pf.getCFG("MethodNames$InnerClass", "anotherInnerClassMethod");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames$InnerClass:anotherInnerClassMethod()");

		//top level anonymous methods
		cfg = pf.getCFG("MethodNames$1", "compareTo");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames$1:compareTo(String)");
		
		//inner class anonymous
		cfg = pf.getCFG("MethodNames$InnerClass$1", "compareTo");
		org.junit.Assert.assertEquals(cfg.getCodeFragment().getUniqueName(), "cfg.MethodNames$InnerClass$1:compareTo(String)");
	}
	
	public void testMethodInvocation() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:statementMethodInvocation"));
	}
	
	// ---------- BLOCK VARIABLES HANDLING ----------------
	@Test
	public void testBlockVariablesHandling() {
		ProjectFile pf = project.openFile("cfg", "VariableBlock.java");
		TestCommentData.parseComments(pf.getCFG("VariableBlock", "methodBody"));
		TestCommentData.parseComments(pf.getCFG("VariableBlock", "statementIf"));
	}
	// ----------------------------------------------------
	
	// ------------------------ IF ----------------------------------
	
	@Test
	public void testSimpleIf() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:statementIf"));
	}

	@Test
	public void testSimpleIfOnlyTrue() {
		MethodControlFlowGraph cfg = project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:statementIfOnlyTrue");
		TestCommentData.parseComments(cfg);
		org.junit.Assert.assertEquals("Loop vertice found!!", Utils.findLoopVertices(cfg).size(), 0);
	}

	@Test
	public void testSimpleIfOnlyFalse() {
		MethodControlFlowGraph cfg = project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:statementIfOnlyFalse");
		TestCommentData.parseComments(cfg);
		org.junit.Assert.assertEquals("Loop vertice found!!", Utils.findLoopVertices(cfg).size(), 0);
	}
	// ------------------------ END IF ----------------------------------
	
	// --------------------------- WHILE ------------------------------

	@Test
	public void testWhile() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementWhile"));
	}

	@Test
	public void testWhileBreak() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementWhileBreak"));
	}

	@Test
	public void testWhileBreakWithLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementWhileBreakWithLabel"));
	}

	@Test
	public void testWhileContinueWithLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementWhileContinueWithLabel"));
	}

	@Test
	public void testWhileContinue() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementWhileContinue"));
	}

	// ------------------------ END WHILE ------------------------------
	
	private void printMappings(MethodControlFlowGraph graph) {
		Pair<Pair<Map<ASTNode, CFGVertice>, Multimap<CFGVertice, ASTNode>>, Pair<Map<ASTNode, CFGVertice>, Multimap<CFGVertice, ASTNode>>> mappings = graph.getMappings();
		Map<ASTNode, CFGVertice> beforeNode = mappings.left.left;
		for(Map.Entry<ASTNode, CFGVertice> entry: beforeNode.entrySet()) {
			System.out.println(entry.getKey().toString().split("\n")[0] + ":" + entry.getValue());
		}
		Multimap<CFGVertice, ASTNode> beforeVertice = mappings.left.right;

		Map<ASTNode, CFGVertice> afterNode = mappings.right.left;
		for(Map.Entry<ASTNode, CFGVertice> entry: afterNode.entrySet()) {
			System.out.println(entry.getKey().toString().split("\n")[0] + ":" + entry.getValue());
		}
		Multimap<CFGVertice, ASTNode> afterVertice = mappings.right.right;
	}

	// --------------------------- DO-WHILE ------------------------------

	@Test
	public void testDoWhile() {
		MethodControlFlowGraph graph = project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementDoWhile");
		TestCommentData.parseComments(graph);
	}

	@Test
	public void testDoWhileBreak() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementDoWhileBreak"));
	}

	@Test
	public void testDoWhileBreakWithLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementDoWhileBreakWithLabel"));
	}

	@Test
	public void testDoWhileContinueWithLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementDoWhileContinueWithLabel"));
	}

	@Test
	public void testDoWhileContinue() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementDoWhileContinue"));
	}

	// ------------------------ END DO-WHILE ------------------------------

	@Test
	public void testEmptyBlock() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:emptyBlock"));
	}

	@Test
	public void testEmptyStatement() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:emptyStatement"));
	}

	@Test
	public void testEmptyStatements() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:emptyStatements"));
	}
	
	@Test
	public void testSimpleAssignment() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:simpleAssignment"));
	}

	@Test
	public void variableDeclarationWithoutAssignment() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:simpleVariadleDeclarationWithoutAssignment"));
	}

	@Test
	public void variableDeclarationWithAssignment() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:simpleVariadleDeclarationWithAssignment"));
	}

	// ------------------------------- FOR ---------------------------------------
	@Test
	public void testForBasic() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementFor"));
	}

	@Test
	public void testForMultipleUpdaters() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForMultipleUpdates"));
	}

	@Test
	public void testForMultipleInitialisersUpdaters() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForMultipleInitialisersAndUpdaters"));
	}

	@Test
	public void testForWithBreak() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForWithBreak"));
	}

	@Test
	public void testForWithBreakLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForWithBreakLabel"));
	}

	@Test
	public void testForWithContinue() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForWithContinue"));
	}

	@Test
	public void testForWithContinueLabel() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Loops.java", "Loops:statementForWithContinueLabel"));
	}
	// ------------------------------- END FOR ---------------------------------------
	
	// ------------------------------- RETURN ---------------------------------------
	@Test
	public void testBasicReturn() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Return.java", "Return:simpleReturn"));
	}

	@Test
	public void testReturnIf() {
		ProjectFile pf = project.openFile("cfg", "Return.java");
		TestCommentData.parseComments(pf.getCFG("Return", "returnFromIf01"));
		TestCommentData.parseComments(pf.getCFG("Return", "returnFromIf02"));
	}

	@Test
	public void testReturnFor() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Return.java", "Return:returnFromFor"));
	}

	@Test
	public void testReturnWhile() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "Return.java", "Return:returnFromWhile"));
	}
	// ------------------------------- END RETURN ---------------------------------------

	// ------------------------------- TRY CATCH ---------------------------------------
	@Test
	public void testTryCatchFinallyOnly(){
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryFinallyOnly"));
	}
	
	@Test
	public void testTryCatchFinallyOnlyWithReturn(){
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryFinallyAndReturn01"));
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryFinallyAndReturn02"));
	}

	@Test
	public void testTryCatchOnly(){
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryCatchOnly01"));
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryCatchOnly02"));
	}

	@Test
	public void testTryCatchAndFinally(){
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryCatchAndFinally01"));
	}

	@Test
	public void testTryCatchAndFinallyAndReturn(){
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "TryCatch.java", "TryCatch:tryCatchAndFinallyReturn01"));
	}
	// ---------------------------------------------------------------------------------
	
	
	// ------------------------------- ENHANCED FOR ------------------------------------
	
	@Test
	public void testEnhancedFor() {
		ProjectFile pf = project.openFile("cfg", "EnhancedFor.java");
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForArray01"));
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForArray02"));
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForCollection01"));
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForCollection02"));
	}

	@Test
	public void testEnhancedForWithBreak() {
		ProjectFile pf = project.openFile("cfg", "EnhancedFor.java");
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForArrayWithBreak01"));
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForCollectionWithBreak01"));
	}

	@Test
	public void testEnhancedForWithContinue() {
		ProjectFile pf = project.openFile("cfg", "EnhancedFor.java");
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForArrayWithContinue01"));
		TestCommentData.parseComments(pf.getCFG("EnhancedFor", "enhancedForCollectionWithContinue01"));
	}
	
	// ---------------------------------------------------------------------------------

	// ------------------------- Constructor invocation --------------------------------
	
	@Test
	public void testConstructorInvocation() {
		ProjectFile pf = project.openFile("cfg", "ConstructorInvocation.java");
		for(MethodControlFlowGraph cfg: pf.getAllMethodGraphs()){
			TestCommentData.parseComments(cfg);
		}
	}
	// ---------------------------------------------------------------------------------

	// ------------------------- Super Constructor invocation --------------------------------
	
	@Test
	public void testSuperConstructorInvocation() {
		ProjectFile pf = project.openFile("cfg", "SuperConstructorInvocation.java");
		for(MethodControlFlowGraph cfg: pf.getAllMethodGraphs()){
			TestCommentData.parseComments(cfg);
		}
	}
	// ---------------------------------------------------------------------------------
	
	// ------------------------------ SWITCH -------------------------------------------
	
	@Test
	public void testSwitch() {
		ProjectFile pf = project.openFile("cfg", "Switch.java");
		TestCommentData.parseComments(pf.getCFG("Switch", "switch01"));
		TestCommentData.parseComments(pf.getCFG("Switch", "switch02"));
		TestCommentData.parseComments(pf.getCFG("Switch", "switch03"));
		TestCommentData.parseComments(pf.getCFG("Switch", "switch04"));
		TestCommentData.parseComments(pf.getCFG("Switch", "switch05"));
		TestCommentData.parseComments(pf.getCFG("Switch", "switch06"));
	}
	// ---------------------------------------------------------------------------------

	// ------------------------------ THROW -------------------------------------------
	@Test
	public void testThrow() {
		ProjectFile pf = project.openFile("cfg", "Throw.java");
		TestCommentData.parseComments(pf.getCFG("Throw", "throwCought01"));
		TestCommentData.parseComments(pf.getCFG("Throw", "throwCought02"));
		TestCommentData.parseComments(pf.getCFG("Throw", "throwCought03"));
		TestCommentData.parseComments(pf.getCFG("Throw", "throwUncought01"));
		TestCommentData.parseComments(pf.getCFG("Throw", "throwUncought02"));
	}
	// ---------------------------------------------------------------------------------

	// ------------------------------ SYNCHRONIZES -------------------------------------
	@Test
	public void testSynchronized() {
		ProjectFile pf = project.openFile("cfg", "Synchronized.java");
		TestCommentData.parseComments(pf.getCFG("Synchronized", "stmtSynchronized01"));
		TestCommentData.parseComments(pf.getCFG("Synchronized", "stmtSynchronized02"));
	}
	// ---------------------------------------------------------------------------------	
	
	// ------------------------------ Break -------------------------------------
	@Test
	public void testBreakWithoutLoop() {
		ProjectFile pf = project.openFile("cfg", "WeirdConstructions.java");
		TestCommentData.parseComments(pf.getCFG("WeirdConstructions", "breakBlock01"));
	}
	// ---------------------------------------------------------------------------------	

	@Test
	public void testAssertIgnored() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:simpleAssert"));
	}
	
	@Test
	public void testClassDeclarationInside() {
		TestCommentData.parseComments(project.getControlFlowGraph("cfg", "SimpleInstructions.java", "SimpleInstructions:simpleClassDeclaration"));
	}
	
		
	@Before
	public void setUp() throws Exception {
		// Initialize the test fixture for each test
		// that is run.
		project = TestUtils.initialiseAndGetProject("CFG Tests");
//		waitForJobs();
//		project = Utils.openProject("AI Tests");
//		delay(3000);
//		waitForJobs();

		// testView = (FavoritesView)
		// PlatformUI
		// .getWorkbench()
		// .getActiveWorkbenchWindow()
		// .getActivePage()
		// .showView(VIEW_ID);
		//
		// // Delay for 3 seconds so that
		// // the Favorites view can be seen.
		// waitForJobs();
		// delay(3000);
		// // Add additional setup code here.
	}
}
