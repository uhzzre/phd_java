package ast;

import org.eclipse.jdt.core.dom.ASTNode;

public interface NodeMapping {
	public ASTNode getNode(String index);
}
