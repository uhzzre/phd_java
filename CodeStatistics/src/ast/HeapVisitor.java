package ast;

import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;

public class HeapVisitor extends ASTVisitor {
	
	private boolean result = false;
	private Set<IBinding> namesFromHeap;
	
	public boolean check(ASTNode node, Set<IBinding> namesFromHeap){
		this.namesFromHeap = namesFromHeap;
		result = false;
		node.accept(this);
		return result;
	}
	
	@Override
	public boolean visit(ArrayAccess node) {
		result = true;
		return false;
		
	}

	private void updateExpression(Expression expr) {
		expr.accept(this);
		if (!result) {
			if (expr.getNodeType() == ASTNode.SIMPLE_NAME || expr.getNodeType() == ASTNode.QUALIFIED_NAME) {
				if (namesFromHeap.contains(((Name) expr).resolveBinding())) {
					result = true;
				}
			} else if (expr.getNodeType() == ASTNode.FIELD_ACCESS) {
				if (namesFromHeap.contains(((FieldAccess)expr).resolveFieldBinding())) {
					result = true;
				}
			}
		}
	}
	
	@Override
	public boolean visit(MethodInvocation node) {
		if (node.getExpression() != null) {
			updateExpression(node.getExpression());
		}
		return false;
	}
	
	@Override
	public boolean visit(FieldAccess node) {
		updateExpression(node.getExpression());
		return super.visit(node);
	}
	
}
