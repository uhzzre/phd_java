package ast;

import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;

public class Utils {
	static private SimpleName getPrefix(Name name) {
		while (name.isQualifiedName())
			name = ((QualifiedName) name).getQualifier();
		assert name.isSimpleName();
		return (SimpleName) name;
	}

	static public String getNamePrefix(Name name) {
		return getPrefix(name).getIdentifier();
	}
	
	public static boolean isFinal(Name node) {
		//we deal with qualified name
		while (true) {
			IBinding binding = node.resolveBinding();
			if (binding == null)
				return false;
			if (binding.getKind() != IBinding.VARIABLE)
				return false;
			IVariableBinding variableBinding = (IVariableBinding) binding;
			if (variableBinding.isField()) {
				if ((variableBinding.getModifiers() & org.eclipse.jdt.core.dom.Modifier.FINAL) == 0)
					return false;
			} else {
				assert node.isSimpleName();
			}
			if (node.isQualifiedName()) {
				node = ((QualifiedName) node).getQualifier();
			} else {
				break;
			}
		}
		return true;
	}

	public static boolean isFinalSuffix(Name node) {
		//we deal with qualified name
		if (node.isSimpleName())
			return false;
		QualifiedName qn = (QualifiedName) node;
		while (true) {
			IBinding binding = qn.resolveBinding();
			if (binding == null)
				return false;
			if (binding.getKind() != IBinding.VARIABLE)
				return false;
			IVariableBinding variableBinding = (IVariableBinding) binding;
			assert variableBinding.isField();
			if ((variableBinding.getModifiers() & org.eclipse.jdt.core.dom.Modifier.FINAL) == 0)
				return false;
			node = qn.getQualifier();
			if (node.isSimpleName()){
				break;
			} else {
				qn = (QualifiedName) node;
			}
		}
		return true;
	}

	public static boolean isFinalFieldAccess(SimpleName node) {
		IBinding binding = node.resolveBinding();
		if (binding == null)
			return false;
		if (binding.getKind() != IBinding.VARIABLE)
			return false;
		return (((IVariableBinding) binding).getModifiers() & org.eclipse.jdt.core.dom.Modifier.FINAL) != 0;
	}

	public static IBinding getBinding(Name node) {
		SimpleName name = getPrefix(node);
		return name.resolveBinding();
	}

	public static ITypeBinding getTypeBinding(Name node) {
		SimpleName name = getPrefix(node);
		return name.resolveTypeBinding();
	}
	
	public static boolean contains(String[] arr, String name) {
		for(int i=0; i< arr.length; i++){
			if (arr[i].equals(name))
				return true;
		}
		return false;
	}
	
	public static IMethodBinding findMethod(ITypeBinding binding, String methodName){
		for(IMethodBinding mb: binding.getDeclaredMethods()){
			if (mb.getName().equals(methodName))
				return mb;
		}
		return null;
	}
}
