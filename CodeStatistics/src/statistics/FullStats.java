package statistics;

import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.jdt.core.dom.ASTNode;

public class FullStats implements StatisticsDetails {
	private SortedMap<String, SortedMap<Integer, String>> stats = new TreeMap<String, SortedMap<Integer, String>>();
	
	@Override
	public void add(String filename, ASTNode node, int lineNo, String insertBefore) {
		SortedMap<Integer, String> res = new TreeMap<Integer, String>();
		if (stats.containsKey(filename)){
			res = stats.get(filename);
		}
		String result = node.toString();
		if (insertBefore !=null)
			result = insertBefore + "\n" + result;
		res.put(lineNo, result);
		stats.put(filename,  res);
	}
	
	@Override
	public String toString(){
		int totalCount = 0;
		for(SortedMap<Integer, String> items: stats.values()){
			totalCount += items.size();
		}
		String res = "" + totalCount;
		for (String file : stats.keySet()){
			res +="\n=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|=|\n";
			res += "FILE : " + file + ":" +  stats.get(file).size() + "\n";
			SortedMap<Integer, String> appearancesMap = stats.get(file);
			for (int lineNo: appearancesMap.keySet()){
				res += "[" + lineNo+"]\n" + appearancesMap.get(lineNo) + "\n--------------------------------------------\n ";
			}
		}
		return res;
	}
}
