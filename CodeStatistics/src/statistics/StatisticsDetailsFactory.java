package statistics;

public class StatisticsDetailsFactory {
	public static final int FULL_STATS = 0;
	public static final int COUNT_FOR_FILE = 1;
	public static final int TOTAL_COUNT = 2;
	public static StatisticsDetails get(int level) {
		if (level == FULL_STATS)
			return new FullStats();
		if (level == COUNT_FOR_FILE)
			return new CountForFile();
		return new TotalCountStatistics();
	}
}
