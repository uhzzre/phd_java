package statistics;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;

public class TotalCountStatistics implements StatisticsDetails {
	private int count = 0;
	private Set<Integer> usedNodes = new HashSet<Integer>(); //heuristics??
	@Override
	public void add(String filename, ASTNode node, int lineNo, String insertBefore) {
		if (usedNodes.contains(System.identityHashCode(node)))
			return;
		usedNodes.add(System.identityHashCode(node));
		count += 1;

	}

	@Override
	public String toString(){
		return "" + count;
	}
}
