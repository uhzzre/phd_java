package statistics;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;

public class CountForFile implements StatisticsDetails {
	private Map<String, Integer> stats = new HashMap<String, Integer>();
	@Override
	public void add(String filename, ASTNode node, int lineNo, String insertBefore) {
		Integer res = new Integer(0);
		if (stats.containsKey(filename)){
			res = stats.get(filename);
		}
		res = new Integer(res.intValue() + 1);
		stats.put(filename, res);
	}

	@Override
	public String toString(){
		String res = "{\n";
		for (String file : stats.keySet()){
			res += file + ": " + stats.get(file) +"\n";
		}
		return res + "}\n";
	}
}
