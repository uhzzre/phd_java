package statistics;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;

/**
 * This class is responsible for gathering statistics about recognized loops. It
 * can collect, for each type of loop, total number of matching nodes, number of
 * matching nodes in each file as well as the content of these nodes.
 * 
 * @author Jedrek
 * 
 */
public class StatisticsCollector{	
	private Map<String, Map<String, StatisticsDetails> > statistics;
	private int level;

	public StatisticsCollector(int level) {
		this.statistics = new HashMap<String, Map<String, StatisticsDetails> >();
		this.level = level;
	}
	
	private Map<String, StatisticsDetails> getProjectStats(IJavaProject project) {
		String projectName = project.getElementName();
		if (!statistics.containsKey(projectName)) {
			Map<String, StatisticsDetails> map = new HashMap<String, StatisticsDetails>();
			statistics.put(projectName, map);
			return map;
		} else
			return statistics.get(projectName);
	}
	
	public void add(IJavaProject project, CompilationUnit cu, String loopType, ASTNode node, String insertBefore) {		
		String fileName = cu.getJavaElement().getPath().toString();
		int lineNo = cu.getLineNumber(node.getStartPosition());
		
		Map<String, StatisticsDetails> projectStats = getProjectStats(project);
		if (!projectStats.containsKey(loopType)) {
			projectStats.put(loopType, StatisticsDetailsFactory.get(level));
		}
		StatisticsDetails statsForType = projectStats.get(loopType);
		statsForType.add(fileName, node, lineNo, insertBefore);
		projectStats.put(loopType, statsForType);
	}

	public String toString() {
		String res = "";
		for (String projectName : statistics.keySet()){
			Map<String, StatisticsDetails> statsForProject = statistics.get(projectName);
			res += "\n**********************************************\n";
			res += "PROJECT: " + projectName +"\n";
			for (String loopType : statsForProject.keySet()){
				res+= "\n&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n";
				res +="LOOP TYPE: " + loopType + "\n" + statsForProject.get(loopType).toString();
			}

		}
		return res;
	}
}
