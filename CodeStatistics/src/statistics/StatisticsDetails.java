package statistics;

import org.eclipse.jdt.core.dom.ASTNode;

public interface StatisticsDetails {
	public void add(String filename, ASTNode node, int lineNo, String insertBefore);
}
