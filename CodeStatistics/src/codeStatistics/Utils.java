package codeStatistics;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.ui.IWorkbenchWindow;
import org.w3c.dom.Document;

import ast.NodeMapping;

public class Utils {

	public static List<IPackageFragment> getPackageFragments(
			ITreeSelection selection) throws JavaModelException {
		List<IPackageFragment> result = new LinkedList<IPackageFragment>();
		Set<Object> roots = new HashSet<Object>();
		for (TreePath path : selection.getPaths()) {
			roots.add(path.getFirstSegment());
		}
		for (Object root : roots) {
			IJavaProject project = (IJavaProject) root;
			for (IPackageFragmentRoot packageFragment : project
					.getAllPackageFragmentRoots()) {
				if (packageFragment.getKind() != IPackageFragmentRoot.K_SOURCE)
					continue;
				for (IJavaElement element : packageFragment.getChildren()) {
					if (element.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
						result.add((IPackageFragment) element);
					} else
						throw new RuntimeException(
								"AAAAAAAA: Nie package fragment!!!");
				}
			}
		}
		return result;
	}

	public static List<ICompilationUnit> getSelectedIcu(ITreeSelection selection)
			throws JavaModelException {
		List<ICompilationUnit> result = new LinkedList<ICompilationUnit>();
		Iterator iter = selection.iterator();
		while (iter.hasNext()) {
			Object obj = iter.next();
			if (obj instanceof ICompilationUnit) {
				result.add((ICompilationUnit) obj);
			} else if (obj instanceof IPackageFragment) {
				IPackageFragment pf = (IPackageFragment) obj;
				for (ICompilationUnit icu : pf.getCompilationUnits())
					result.add(icu);
			} else if (obj instanceof IJavaProject) {
				IJavaProject jp = (IJavaProject) obj;
				for (IPackageFragmentRoot packageFragment : jp
						.getAllPackageFragmentRoots()) {
					if (packageFragment.getKind() != IPackageFragmentRoot.K_SOURCE)
						continue;
					for (IJavaElement element : packageFragment.getChildren()) {
						if (element.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
							for (ICompilationUnit icu : ((IPackageFragment) element)
									.getCompilationUnits())
								result.add(icu);
						} else
							throw new RuntimeException(
									"AAAAAAAA: Nie package fragment!!!");
					}
				}

			}
		}
		return result;
	}

	public static String print(Document doc) throws TransformerException {

		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		trans.transform(source, result);
		return sw.toString();
	}

	public static CompilationUnit parse(ICompilationUnit unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit); // set source
		parser.setResolveBindings(true); // we need bindings later on
		return (CompilationUnit) parser.createAST(null /* IProgressMonitor */); // parse
	}

	public static void displayError(IWorkbenchWindow window, Throwable exc) {
		IStatus status = new Status(IStatus.ERROR,
				CodeStatisticsPlugin.PLUGIN_ID, 1, "Exception found.", exc);
		ExceptionDetailsErrorDialog.openError(window.getShell(),
				"Error while loading file", null, status);
	}

	public static ASTNode getMappedAstNode(NodeMapping visitor, XdmItem item) {
		String id = ((XdmNode) item).getAttributeValue(new QName("id"));
		if (id == null)
			return null;
		return visitor.getNode(id);
	}
}
