package codeStatistics;

import java.util.ArrayList;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import codeStatistics.extension.IAnalysisPlugin;

public class CodeStatisticsPlugin extends AbstractUIPlugin {

	private static CodeStatisticsPlugin plugin;
	public static final String PLUGIN_ID = "CodeStatistics";
	final ArrayList<IAnalysisPlugin> analysisExtensions;	

	public CodeStatisticsPlugin() {
		this.analysisExtensions = AnalysisPluginsManager.loadPlugins();
		System.out.println("Loaded:" + analysisExtensions);
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		//load extensions
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static CodeStatisticsPlugin getDefault() {
		return plugin;
	}

	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
