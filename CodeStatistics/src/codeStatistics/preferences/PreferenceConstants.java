package codeStatistics.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_LOG_FILE = "logFile";

	public static final String P_XPATH_FILE = "xpathFile";

	public static final String P_LOG_LEVEL = "logLevel";

	public static final String[][] P_LEVEL_CHOICES = {{"FULL", "FULL"}, {"COUNT_FOR_FILE","COUNT_FOR_FILE"}, {"TOTAL_COUNT", "TOTAL_COUNT"}};
}
