package codeStatistics.xpathfunctions;

import net.sf.saxon.s9api.ExtensionFunction;
import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.ItemTypeFactory;
import net.sf.saxon.s9api.OccurrenceIndicator;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.value.ObjectValue;

import org.eclipse.jdt.core.dom.ASTNode;

public class AstTextExtensionFunction implements ExtensionFunction{
	
	private final ItemTypeFactory typeFactory;

	public AstTextExtensionFunction(ItemTypeFactory typeFactory){
		this.typeFactory = typeFactory;
	}

	@Override
	public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
    	XdmItem item = arguments[0].itemAt(0);
    	ObjectValue value = (ObjectValue) item.getUnderlyingValue();
		ASTNode node = (ASTNode) value.getObject();
		return new XdmAtomicValue(node.toString());
	}

	@Override
	public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.makeSequenceType(typeFactory.getExternalObjectType(ASTNode.class), OccurrenceIndicator.ONE)};
	}

	@Override
	public QName getName() {
		return FunctionUtils.getName("text");
	}

	@Override
	public SequenceType getResultType() {
		return SequenceType.makeSequenceType(ItemType.STRING, OccurrenceIndicator.ONE);
	}
}
