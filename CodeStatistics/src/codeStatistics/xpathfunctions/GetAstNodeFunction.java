package codeStatistics.xpathfunctions;

import net.sf.saxon.s9api.ExtensionFunction;
import net.sf.saxon.s9api.ItemType;
import net.sf.saxon.s9api.ItemTypeFactory;
import net.sf.saxon.s9api.OccurrenceIndicator;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XdmEmptySequence;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmValue;

import org.eclipse.jdt.core.dom.ASTNode;

import ast.NodeMapping;
import codeStatistics.Utils;

public class GetAstNodeFunction implements ExtensionFunction {

	private NodeMapping mapping;
	private final ItemTypeFactory typeFactory;
	
	public GetAstNodeFunction(ItemTypeFactory typeFactory){
		this.typeFactory = typeFactory;
	}

	@Override
	public XdmValue call(XdmValue[] arguments) throws SaxonApiException {
    	XdmItem item = arguments[0].itemAt(0);
    	ASTNode nodeOrNull = Utils.getMappedAstNode(mapping, item);
    	return (nodeOrNull == null) ? XdmEmptySequence.getInstance() : new ObjValue(nodeOrNull);
	}

	@Override
	public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.makeSequenceType(ItemType.ANY_ITEM, OccurrenceIndicator.ONE)};
	}

	@Override
	public QName getName() {
		return FunctionUtils.getName("ast");
	}

	@Override
	public SequenceType getResultType() {
		return FunctionUtils.createZeroOrOneResultType(typeFactory, ASTNode.class);
	}
	
	public void setNodeMapping(NodeMapping mapping) {
		this.mapping = mapping;
	}
}

