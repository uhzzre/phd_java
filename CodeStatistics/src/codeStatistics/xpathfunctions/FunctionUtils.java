package codeStatistics.xpathfunctions;

import net.sf.saxon.s9api.ItemTypeFactory;
import net.sf.saxon.s9api.OccurrenceIndicator;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SequenceType;
import net.sf.saxon.s9api.XPathCompiler;

public class FunctionUtils {
	public static final String NAMESPACE = "http://www.mimuw.edu.pl/~kjk/CodeStatistics";
	
	public static SequenceType createZeroOrOneResultType(ItemTypeFactory typeFactory, Class<?> clazz) {
		return SequenceType.makeSequenceType(typeFactory.getExternalObjectType(clazz), 
				OccurrenceIndicator.ZERO_OR_ONE);
	}

	public static QName getName(String name){
        return new QName(NAMESPACE, name);
	}
	
	public static XPathCompiler createCompiler(Processor proc){
		XPathCompiler result = proc.newXPathCompiler();
		result.declareNamespace("cs", FunctionUtils.NAMESPACE);
		return result;
	}
	
}
