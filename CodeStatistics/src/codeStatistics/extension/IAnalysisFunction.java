package codeStatistics.extension;

import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;

public interface IAnalysisFunction {
	public String getName();
	public Object execute(Object[] args, Map<ASTNode, Object> context);
	public Class<?>[] getArgumentType();
	public Class<?> getResultType();
}
