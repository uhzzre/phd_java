package codeStatistics.extension;

import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;

public interface IAnalysisPlugin {
	public String getName();	
	public void initialiseAnalysis();
	public List<IAnalysisFunction> getExtensionFunctions();
	public Map<ASTNode, Object> analyse(CompilationUnit cu);
	public String getSummary();
}
