package codeStatistics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;

import codeStatistics.extension.IAnalysisFunction;
import codeStatistics.extension.IAnalysisPlugin;

public class AnalysisPluginsManager {
	private static final String IANALYSIS_PLUGIN_ID = "codeStatistics.analysisPlugin";

	static ArrayList<IAnalysisPlugin> loadPlugins() {
		ArrayList<IAnalysisPlugin> result = new ArrayList<IAnalysisPlugin>();
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(
				IANALYSIS_PLUGIN_ID);
		try {
			for(IConfigurationElement e: config) {
				System.out.println("Evaluating extension");
				final Object o = e.createExecutableExtension("class");
				if (o instanceof IAnalysisPlugin)
					result.add((IAnalysisPlugin) o);
			}
		} catch (CoreException e) {
			System.out.println(e.getMessage());
		}
		return result;
	}
//	
//	public static List<List<IBooleanAnalysisFunction>> getBooleanFunctionDefinitions() {
//		List<List<IBooleanAnalysisFunction>> result = new LinkedList<List<IBooleanAnalysisFunction>>();
//		for(IAnalysisPlugin ext: CodeStatisticsPlugin.getDefault().analysisExtensions){
//			result.add(ext.getBooleanExtensionFunctions());
//		}
//		return result;
//	}

	public static List<List<IAnalysisFunction>> getFunctionDefinitions() {
		List<List<IAnalysisFunction>> result = new LinkedList<List<IAnalysisFunction>>();
		for(IAnalysisPlugin ext: CodeStatisticsPlugin.getDefault().analysisExtensions){
			result.add(ext.getExtensionFunctions());
		}
		return result;
	}
	
	public static void initialiseExtensions() {
		for(IAnalysisPlugin ext: CodeStatisticsPlugin.getDefault().analysisExtensions){
			ext.initialiseAnalysis();
		}
	}
	
	public static ArrayList<Map<ASTNode, Object>> analyse(CompilationUnit cu) {
		ArrayList<IAnalysisPlugin> exts = CodeStatisticsPlugin.getDefault().analysisExtensions;
		ArrayList<Map<ASTNode, Object>> result = new ArrayList<Map<ASTNode,Object>>(exts.size());
		for(IAnalysisPlugin plug: exts){
			Map<ASTNode, Object> r = null;
			try {
				r = plug.analyse(cu);
			} catch (RuntimeException e) {
				e.printStackTrace(System.err);
			}
			result.add(r);
		}
		return result;
	}
	
	public static class Summary {
		public final String title;
		public final String message;
		public Summary(String title, String message) {
			this.title = title;
			this.message = message;
		}
	}
	
	public static List<Summary> getSummaries() {
		List<Summary> result = new LinkedList<Summary>();
		for(IAnalysisPlugin ext: CodeStatisticsPlugin.getDefault().analysisExtensions)
			result.add(new Summary(ext.getName(), ext.getSummary()));
		return result;
	}
}
