package codeStatistics;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class InfoMessageDialog extends MessageDialog{

	private final String longMessage;

	private InfoMessageDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage,
			int dialogImageType, String[] dialogButtonLabels, int defaultIndex, String longMessage) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);
		this.longMessage = longMessage;
	}
	
    public static void openInformation(Shell parent, String title, String message, String extraMessage) {
    	String[] btnLabels = new String[] { IDialogConstants.OK_LABEL };
		InfoMessageDialog dialog = new InfoMessageDialog(parent, title, null, message,
				INFORMATION, btnLabels, 0, extraMessage);
		dialog.setShellStyle(dialog.getShellStyle() | SWT.SHEET);
		dialog.open();    	
    }	
	
	@Override
	public Control createCustomArea(Composite parent) {
        Text text = new Text(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
        text.setText(longMessage);
        GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL
                | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
                | GridData.GRAB_VERTICAL);        
        text.setLayoutData(data);
		return text;
	}
}
