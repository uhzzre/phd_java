package input;

public interface SourceFileRepository extends Iterable<SourceFile>{
	public SourceFile getSourceFile(String name);
}
