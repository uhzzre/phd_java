package input;

import java.util.List;

public final class Configuration{
	public final Pattern allCasesDescriptor;
	public final List<Pattern> categorizedCases;
	public Configuration(Pattern allCases, List<Pattern> categorizedCases){
		this.allCasesDescriptor = allCases;
		this.categorizedCases = categorizedCases;
	}
}