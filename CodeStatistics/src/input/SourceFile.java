package input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SourceFile {
	private String location;
	public SourceFile(String location){
		this.location = location;		
	}
	
	public String getLocation() {
		return location;
	}

	public String getContent() throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader (new FileReader(location));
		char[] chars = new char[1024];
		while( (br.read(chars)) > -1){
			sb.append(String.valueOf(chars));
		}
		return sb.toString();
	}
}
