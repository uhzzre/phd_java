package input;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ConfigurationReader {
	public static Configuration loadConfiguration(String configurationFile) throws ParserConfigurationException, SAXException, IOException {
		List<Pattern> categorized = new LinkedList<Pattern>();
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(new File(configurationFile));
		NodeList all = doc.getElementsByTagName("all");
		assert (all.getLength() == 1);
		Pattern allDesc = extract(all.item(0));
		NodeList nodelist = doc.getElementsByTagName("description");
		for (int i = 0; i < nodelist.getLength(); i++){
			Node n = nodelist.item(i);
			categorized.add(extract(n));
		}
		return new Configuration(allDesc, categorized);
	}
	
	private static Pattern extract(Node n){
		NamedNodeMap attributes = n.getAttributes();
		
		Attr name = (Attr)attributes.getNamedItem("name");
		Attr xpath = (Attr)attributes.getNamedItem("xpath");
		Attr outputBefore = (Attr)attributes.getNamedItem("outputBefore");
		return new Pattern(name.getValue(), xpath.getValue(), (outputBefore != null) ? outputBefore.getValue() : null);
	
	}
}
