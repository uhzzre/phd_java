package input;


public final class Pattern {
	public final String name;
	public final String xpath;
	public final String outputBefore;
	public Pattern(String name, String xpath, String outputBefore){
		this.name = name;
		this.xpath = xpath;
		this.outputBefore = outputBefore;
	}
}	


